﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


namespace Fixel
{

        public class DadaBase
        {
            public SqlConnection Conn;
            public String Connstr;
            public DadaBase()
            {
                Connstr = ConfigurationManager.ConnectionStrings["MainConn"].ToString();
                Conn = new SqlConnection(Connstr);
                Open();
            }
            public void Open()
            {
                Conn.Open();
            }
            public void Close()
            {
                Conn.Close();
            }
            public SqlDataReader executeReader(string sql)
            {
                SqlCommand Cmd = new SqlCommand(sql, Conn);
                return Cmd.ExecuteReader();
            }
            public DataTable execute(string sql)
            {
                SqlCommand Cmd = new SqlCommand(sql, Conn);
                DataTable Dt = new DataTable();
                SqlDataAdapter Da = new SqlDataAdapter(Cmd);
                Da.Fill(Dt);
                return Dt;
            }

        }

  
}