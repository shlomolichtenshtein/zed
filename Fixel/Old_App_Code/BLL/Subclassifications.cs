﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Fixel.DAL;
using System.Data;

namespace Fixel
{
    namespace BLL
    {
        public class Subclassifications
        {
            public int SubclassificationsID { get; set; }
            public string SubclassificationsName { get; set; }
            public Subclassifications(int SubclassificationsID, string SubclassificationsName)
            {
                this.SubclassificationsID = SubclassificationsID;
                this.SubclassificationsName = SubclassificationsName;
            }
            public Subclassifications()
            {

            }
            public int Subcla(int data1, string data2)
            {


                return SubclassificationsDal.Subcla(data1, data2);
            }
            public static List<Subclassifications> GetAll()
            {

                DataTable Dt = SubclassificationsDal.GetAll();
                List<Subclassifications> Lst = new List<Subclassifications>();
                Subclassifications Temp;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {
                    Temp = new Subclassifications();
                    Temp.SubclassificationsID = (int)Dt.Rows[i]["SubclassificationsID"];
                    Temp.SubclassificationsName = (string)Dt.Rows[i]["SubclassificationsName"];
                    Lst.Add(Temp);
                }
                return Lst;


            }
        }
    }

}