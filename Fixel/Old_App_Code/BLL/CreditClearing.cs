﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Fixel.DAL;
using System.Data;

namespace Fixel
{
    namespace BLL
    {
        public class CreditClearing//A class of credit card types
        {
            public int CreditClearingid { get; set; }
            public string CreditClearingName { get; set; }
            public int Namber { get; set; }
            public double CommissionS { get; set; }
            public int CommissionK { get; set; }
            public int BanksID { get; set; }
            public int customerID { get; set; }
            public CreditClearing(int CreditClearingid, string CreditClearingName, int Namber, double CommissionS, int CommissionK, int BanksID, int customerID)
            {
                this.CreditClearingid = CreditClearingid;
                this.CreditClearingName = CreditClearingName;
                this.Namber = Namber;
                this.CommissionS = CommissionS;
                this.CommissionK = CommissionK;
                this.BanksID = BanksID;
                this.customerID = customerID;
            }
            public CreditClearing()
            {

            }
            public static List<CreditClearing> GetAll(int CId)
            {
                DataTable Dte = CreditClearingDal.GetAll(CId);
                List<CreditClearing> Exp = new List<CreditClearing>();
                CreditClearing Tamp;
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new CreditClearing();
                    Tamp.CreditClearingid = (int)Dte.Rows[i]["CreditClearingid"];
                    Tamp.CreditClearingName = (string)Dte.Rows[i]["CreditClearingName"].ToString();
                    Tamp.Namber = (int)Dte.Rows[i]["Namber"];
                    Tamp.CommissionS = (double)Dte.Rows[i]["CommissionS"];
                    Tamp.CommissionK = (int)Dte.Rows[i]["CommissionK"];
                    Tamp.BanksID = (int)Dte.Rows[i]["BanksID"];
                    Tamp.customerID = (int)Dte.Rows[i]["customerID"];
                    Exp.Add(Tamp);
                }
                return Exp;
            }
        }
    }

}
