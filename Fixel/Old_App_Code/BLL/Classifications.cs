﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Fixel.DAL;
using System.Data;

namespace Fixel
{
    namespace BLL
    {
        public class Classifications//Class of Class
        {
            public int ClassificationsID { get; set; }
            public string ClassificationsName { get; set; }
            public Classifications(int ClassificationsID, string ClassificationsName)
            {
                this.ClassificationsID = ClassificationsID;
                this.ClassificationsName = ClassificationsName;
            }
            public Classifications()
            {

            }
            public int Classif(int data1, string data2)
            {
             

                return ClassificationsDal.Classif(data1, data2);
            }
            public static List<Classifications> GetAll()
            {
           
                DataTable Dt = ClassificationsDal.GetAll();
                List<Classifications> Lst = new List<Classifications>();
                Classifications Temp;
                for (int i = 0; i < Dt.Rows.Count; i++)
                {
                    Temp = new Classifications();
                    Temp.ClassificationsID = (int)Dt.Rows[i]["ClassificationsID"];
                    Temp.ClassificationsName = (string)Dt.Rows[i]["ClassificationsName"];
                    Lst.Add(Temp);
                }
                return Lst;


            }

        }
    }

}