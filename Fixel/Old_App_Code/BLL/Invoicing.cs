﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Fixel.DAL;

namespace Fixel
{
    namespace BLL
    {
        public class Invoicing
        {
            public int InvoicingId { get; set; }
            public int VendorsID { get; set; }
            public string date { get; set; }
            public int Amount { get; set; }
            public int InvoicingNamber { get; set; }
            public string InvoicingBill { get; set; }

            public string VendorsName { get; set; }
            public int CustomerID { get; set; }
            public Invoicing(int InvoicingId, int VendorsID, string date, int Amount, int InvoicingNamber, string InvoicingBill)
            {
                this.InvoicingId = InvoicingId;
                this.VendorsID = VendorsID;
                this.date = date;
                this.Amount = Amount;
                this.InvoicingNamber = InvoicingNamber;
                this.InvoicingBill = InvoicingBill;

            }
            public Invoicing()
            {

            }
            public static List<Invoicing> GetAllBiI(int us, string name)
            {
                DataTable Dte = InvoicingDal.GetAllBiI(us, name);
                List<Invoicing> Exp = new List<Invoicing>();
                Invoicing Tamp;
 
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new Invoicing();
                    Tamp.InvoicingId = (int)Dte.Rows[i]["InvoicingId"];
                    Tamp.VendorsID = (int)Dte.Rows[i]["VendorsID"];
                    Tamp.VendorsName = (string)Dte.Rows[i]["VendorsName"];
                    Tamp.date = (string)Dte.Rows[i]["date"].ToString();
                    Tamp.Amount = (int)Dte.Rows[i]["Amount"];
                    Tamp.InvoicingNamber = (int)Dte.Rows[i]["InvoicingNamber"];
                    Tamp.InvoicingBill = (string)(Dte.Rows[i]["InvoicingBill"] + "");
                    Tamp.CustomerID = (int)Dte.Rows[i]["CustomerID"];
                    
                    Exp.Add(Tamp);
                }
                return Exp;
            }
            public static List<Invoicing> GetBid(int us, int id)
            {
                DataTable Dte = InvoicingDal.GetBid(us, id);
                List<Invoicing> Exp = new List<Invoicing>();
                Invoicing Tamp;
                string datag = Utils.GlobFuncs.DateGet(Dte.Rows[0]["date"].ToString());
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new Invoicing();
                    Tamp.InvoicingId = (int)Dte.Rows[i]["InvoicingId"];
                    Tamp.VendorsID = (int)Dte.Rows[i]["VendorsID"];
                    Tamp.VendorsName = (string)Dte.Rows[i]["VendorsName"];
                    Tamp.date = datag;
                    Tamp.Amount = (int)Dte.Rows[i]["Amount"];
                    Tamp.InvoicingNamber = (int)Dte.Rows[i]["InvoicingNamber"];
                    Tamp.InvoicingBill = (string)(Dte.Rows[i]["InvoicingBill"] + "");
                    Exp.Add(Tamp);
                }
                return Exp;
            }
            public static List<Invoicing> GetTreatment(int us)
            {
                DataTable Dte = InvoicingDal.GetTreatment(us);
                List<Invoicing> Exp = new List<Invoicing>();
                Invoicing Tamp;

                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new Invoicing();
                    Tamp.InvoicingId = (int)Dte.Rows[i]["InvoicingId"];
                    Tamp.VendorsID = (int)Dte.Rows[i]["VendorsID"];
                    Tamp.VendorsName = (string)Dte.Rows[i]["VendorsName"];
                    Tamp.date = (string)Dte.Rows[i]["date"].ToString();
                    Tamp.Amount = (int)Dte.Rows[i]["Amount"];
                    Tamp.InvoicingNamber = (int)Dte.Rows[i]["InvoicingNamber"];
                    Tamp.InvoicingBill = (string)(Dte.Rows[i]["InvoicingBill"] + "");
                    Tamp.CustomerID = (int)Dte.Rows[i]["CustomerID"];

                    Exp.Add(Tamp);
                }
                return Exp;
            }
            public static bool Delete(int id)
            {
                InvoicingDal.Delete(id);
                return true;
            }
            public static bool Used(string VendorsName, int InvoicingNamber, int cid)
            {
                return InvoicingDal.Used(VendorsName, InvoicingNamber, cid);
            }
            public void pot(int id)
            {
                Vendors ven;
                ven = new Vendors();
                int v = ven.Vend((string)this.VendorsName, (int)this.CustomerID);
                string dat2 = Utils.GlobFuncs.dateadd(this.date);
                Invoicing ht;
                ht = new Invoicing();
                ht.InvoicingId = id;
                ht.date = (string)dat2.ToString();
                ht.InvoicingNamber = (int)this.InvoicingNamber;
                ht.Amount = (int)this.Amount;
                ht.CustomerID = (int)this.CustomerID;
                ht.InvoicingBill = (string)this.InvoicingBill;

                ht.VendorsID = v;
                InvoicingDal.pot(ht);

            }
            public void AddInvoice()
            {
                Vendors ven;
                ven = new Vendors();
                int v = ven.Vend((string)this.VendorsName, (int)this.CustomerID);
                string dat2 = Utils.GlobFuncs.dateadd(this.date);
                Invoicing ht;
                ht = new Invoicing();
                ht.InvoicingId = (int)this.InvoicingId;
                ht.date = (string)dat2.ToString();
                ht.InvoicingNamber = (int)this.InvoicingNamber;
                ht.Amount = (int)this.Amount;
                ht.CustomerID = (int)this.CustomerID;
                ht.InvoicingBill = (string)this.InvoicingBill;

                ht.VendorsID = v;
                InvoicingDal.AddInvoice(ht);
            }
        }


    }

}
