﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Fixel.DAL;

namespace Fixel
{
    namespace BLL
    {
        public class Banks //Department of Banks
        {
            public int BanksID { get; set; }
            public string BanksName { get; set; }
            public Banks(int BanksID, string BanksName)
            {
                this.BanksID = BanksID;
                this.BanksName = BanksName;
            }
            public Banks() { }
            public int Bank(int data1, string data2)
            {
            return BanksDal.Bank(data1, data2);
            }
            public static List<Banks> GetAll()
            {
                DataTable Dte = BanksDal.GetAll();
                List<Banks> Exp = new List<Banks>();
                Banks Tamp;
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new Banks();
                    Tamp.BanksID = (int)Dte.Rows[i]["BanksID"];
                    Tamp.BanksName = (string)Dte.Rows[i]["BanksName"] + "";
                    Exp.Add(Tamp);
                }
                return Exp;
            }
        }
    }

}