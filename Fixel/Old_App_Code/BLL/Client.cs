﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Fixel.BLL;

namespace Fixel
{
    namespace BLL
    {
        public class Client : user//The company's department inherits from a user
        {
            public int SocietyID { get; set; }
            public string SocietyNaem { get; set; }
            public string Email { get; set; }
            public string Password { get; set; }
            public Client(int SocietyID, string SocietyNaem, string Email, string Password) : base(SocietyID, SocietyNaem, Email, Password)
            {
                this.SocietyID = SocietyID;
                this.SocietyNaem = SocietyNaem;
                this.Email = Email;
                this.Password = Password;
            }


        }
    } 

}
