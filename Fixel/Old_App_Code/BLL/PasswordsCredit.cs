﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Web.Http;
using Fixel.DAL;
using System.Configuration;

namespace Fixel
{
    namespace BLL
    {
        public class PasswordsCredit
        {
            public int PasswordsCreditID { get; set; }
            public int CreditID { get; set; }
            public int CustomerID { get; set; }
            public string TicketNumber { get; set; }
            public string Identify { get; set; }
            public string Password { get; set; }
            public int Day { get; set; }
            public PasswordsCredit()
            {

            }
            public static List<PasswordsCredit> PasswUser(int Bid, int Cid)//Brings your expenses by date
            {
                DataTable Dte = PasswordsCreditDAL.PasswUser(Bid, Cid);
                List<PasswordsCredit> Exp = new List<PasswordsCredit>();
                PasswordsCredit Tamp;
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new PasswordsCredit();
                    Tamp.Identify = (string)Dte.Rows[i]["Identify"];
                    Tamp.Password = (string)Dte.Rows[i]["Password"];
                    Tamp.TicketNumber = (string)(Dte.Rows[i]["TicketNumber"] + "");
                    Tamp.Day = (int)Dte.Rows[i]["Day"];

                    Exp.Add(Tamp);
                }
                return Exp;
            }
            public static void pot(string pass, int cid, string bid)
            {
                int b = CreditDal.Cred(cid, bid);
                string pa = Utils.StringCipher.Encrypt(pass, ConfigurationManager.AppSettings["calculator"]);
                PasswordsCreditDAL.pot(pa, cid, b);

            }
            public void Postc(List<PasswordsCredit> paa)
            {
                string InsertSql = "";
                for (int i = 0; i < paa.Count; i++)
                {
                    string pa = Utils.StringCipher.Encrypt(paa[i].Password, ConfigurationManager.AppSettings["calculator"]);
                    InsertSql += "Insert into PasswordsCredit(CreditID,CustomerID,Identify,[Password],TicketNumber,Day) values(";
                    InsertSql += "" + paa[i].CreditID + ",";
                    InsertSql += "" + paa[i].CustomerID + ",";
                    InsertSql += "'" + paa[i].Identify + "',";
                    InsertSql += "'" + pa + "',";
                    InsertSql += "'" + paa[i].TicketNumber + "',";
                    InsertSql += "" + paa[i].Day + ");";
                }
                PasswordsCreditDAL.pot(InsertSql);
                
            }
        }
    }

}
