﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Fixel.DAL;
using System.Configuration;
using System.Data;

namespace Fixel
{
    namespace BLL
    {
        public class PasswordsBA
        {
            public int PasswordsBAID { get; set; }
            public int BanksID { get; set; }
            public int CustomerID { get; set; }
            public string Identify { get; set; }
            public string Password { get; set; }
            public string AccountNumber { get; set; }

            public string BanksName { get; set; }
            public int Balance { get; set; }
            public int Attraction { get; set; }
            public int Frame { get; set; }   
            public PasswordsBA(int PasswordsBAID, int BanksID, int CustomerID, string Identify, string Password, string AccountNumber)
            {
                this.PasswordsBAID = PasswordsBAID;
                this.BanksID = BanksID;
                this.CustomerID = CustomerID;
                this.Identify = Identify;
                this.Password = Password;
                this.AccountNumber = AccountNumber;
            }
            public PasswordsBA(int passwordsBAID, int BanksID)
            {
                this.PasswordsBAID = PasswordsBAID;
                this.BanksID = BanksID;
            }
            public PasswordsBA()
            {

            }
            public static int Pas( string data2, int Cid)
            {


                return PasswordsBADal.Pas( data2, Cid);
            }
            public int PasswId(int Bid, int Cid)
            {
                return PasswordsBADal.PasswId(Bid, Cid);
            }
            public static string name(int paid)
            {
                return PasswordsBADal.name(paid);
            }
            public static void pot(string pass, int cid, string bid)
            {
                int b = BanksDal.Bank(cid, bid);
                string pa = Utils.StringCipher.Encrypt(pass, ConfigurationManager.AppSettings["calculator"]);
                PasswordsBADal.pot(pa,cid,b);

            }
            public static List<PasswordsBA> Getby(int us)
            {
                DataTable Dte = PasswordsBADal.Getby(us);
                List<PasswordsBA> Exp = new List<PasswordsBA>();
                PasswordsBA Tamp;
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new PasswordsBA();
                    Tamp.AccountNumber = (string)Dte.Rows[i]["AccountNumber"];
                    Tamp.Balance = (int)Dte.Rows[i]["Balance"];
                    Tamp.Attraction = (int)Dte.Rows[i]["Attraction"];
                    Tamp.Frame = (int)Dte.Rows[i]["Frame"];
                    Tamp.BanksName = (string)Dte.Rows[i]["BanksName"] + "";
                    Exp.Add(Tamp);
                }
                return Exp;
            }

            public void Postc(List<PasswordsBA> paa)
            { 
                string InsertSql = "";
                for(int i =0; i<paa.Count;i++)
                {
                    string pa = Utils.StringCipher.Encrypt(paa[i].Password, ConfigurationManager.AppSettings["calculator"]);
                    InsertSql += "Insert into PasswordsBA(BanksID,CustomerID,Identify,[Password],AccountNumber,Balance,Frame,Attraction) values(";
                    InsertSql += "" + paa[i].BanksID + ",";
                    InsertSql += "" + paa[i].CustomerID + ",";
                    InsertSql += "'" + paa[i].Identify + "',";
                    InsertSql += "'" + pa + "',";
                    InsertSql += "'" + paa[i].AccountNumber + "',";
                    InsertSql += "" + paa[i].Balance + ",";
                    InsertSql += "" + paa[i].Frame + ",";
                    InsertSql += "" + paa[i].Attraction + ");";
                }
                PasswordsBADal.pot(InsertSql);
            }
        }
    }

}
