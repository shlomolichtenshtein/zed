﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Fixel.DAL;

namespace Fixel
{
    namespace BLL
    {
        public class Credit
        {
            public int CreditID { get; set; }
            public string CreditName { get; set; }
            public Credit()
            {

            }
            public static List<Credit> GetAll()
            {
                DataTable Dte = CreditDal.GetAll();
                List<Credit> Exp = new List<Credit>();
                Credit Tamp;
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new Credit();
                    Tamp.CreditID = (int)Dte.Rows[i]["CreditID"];
                    Tamp.CreditName = (string)Dte.Rows[i]["CreditName"] + "";
                    Exp.Add(Tamp);
                }
                return Exp;
            }
        }
    }

}