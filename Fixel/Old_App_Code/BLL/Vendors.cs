﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Fixel.DAL;
using System.Data;


namespace Fixel
{
    namespace BLL
    {
        public class Vendors
        {
            public int VendorsID { get; set; }
            public string VendorsName { get; set; }
            public string Email { get; set; }
            public int CustomerID { get; set; }

            public int id { get; set; }
            public string Date { get; set; }
            public int payment { get; set; }
            public int Amount { get; set; }
            public int InvoicingNamber { get; set; }
            public string source { get; set; }
            public string DueDate { get; set; }

            public int Subtotal { get; set; }
            public int balance { get; set; }
            public int RequiredSummary { get; set; }
            public int CreditSummary { get; set; }
            public int InvoiciMM { get; set; }

            public Vendors(int VendorsID, string VendorsName, string Email, int CustomerID)
            {
                this.VendorsID = VendorsID;
                this.VendorsName = VendorsName;
                this.Email = Email;
                this.CustomerID = CustomerID;
            }
            public Vendors(int VendorsID, string VendorsName,  int CustomerID)
            {
                this.VendorsID = VendorsID;
                this.VendorsName = VendorsName;
              
                this.CustomerID = CustomerID;
            }
            public Vendors()
            {

            }
            public Vendors(string VendorsName,int CustomerID)
            {
                this.VendorsName = VendorsName;

                this.CustomerID = CustomerID;
            }
            public int Vend( string data2,int Cid)
            {


                return VendorsDal.Vend( data2, Cid);
            }
            public static List<Vendors> GetAll(int id)
            {
                DataTable Dte = VendorsDal.GetAll(id);
                List<Vendors> Exp = new List<Vendors>();
                Vendors Tamp;
                for (int i = 0; i < Dte.Rows.Count; i++)
                {                  
                    Tamp = new Vendors();
                    Tamp.VendorsID = (int)Dte.Rows[i]["VendorsID"];
                    Tamp.VendorsName = (string)Dte.Rows[i]["VendorsName"];
                    Tamp.Email = (string)(Dte.Rows[i]["Email"] + "");
                    Tamp.CustomerID = (int)Dte.Rows[i]["CustomerID"];
                    Exp.Add(Tamp);
                }
                return Exp;
            }
            public static List<Vendors> GetAllBiV(int id,string name)
            {
                DataTable Dte = VendorsDal.GetAllBiV(id,name);
                List<Vendors> Exp = new List<Vendors>();
                Vendors Tamp;
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new Vendors();
                    Tamp.id = (int)Dte.Rows[i]["id"];
                    Tamp.VendorsName = (string)Dte.Rows[i]["VendorsName"];
                    Tamp.Date = (string)(Dte.Rows[i]["Date"]).ToString();
                    Tamp.payment = (int)Dte.Rows[i]["payment"];
                    Tamp.Amount = (int)Dte.Rows[i]["Amount"];
                    Tamp.InvoicingNamber = (int)Dte.Rows[i]["InvoicingNamber"];
                    Tamp.source =  (string)(Dte.Rows[i]["source"]+"") ;
                    Tamp.DueDate = (string)(Dte.Rows[i]["DueDate"]).ToString();
                    Exp.Add(Tamp);
                }
                return Exp;
            }
            public static List<Vendors> GetAllBalance(int id)
            {
                DataTable Dte = VendorsDal.GetAllBalance(id);
                List<Vendors> Exp = new List<Vendors>();
                Vendors Tamp;
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new Vendors();
                    Tamp.VendorsName = (string)Dte.Rows[i]["VendorsName"];
                    Tamp.RequiredSummary = (int)Dte.Rows[i]["RequiredSummary"];
                    Tamp.CreditSummary = (int)Dte.Rows[i]["CreditSummary"];
                    Tamp.InvoiciMM = (int)Dte.Rows[i]["InvoiciMM"];
                    Tamp.balance = (int)(Dte.Rows[i]["balance"]);
                    Exp.Add(Tamp);
                }
                return Exp;
            }
            public int AddVendors()
            {
               return VendorsDal.AddVendors(this);
            }
        }
    }

}
