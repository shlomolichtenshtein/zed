﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Fixel.BLL;
using System.Data;

namespace Fixel
{
    namespace DAL
    {
        public class IncomeDal
        {
            public static DataTable GetAll(int CId)//מביא את כל הכנסות לפי לקוח
            {
                DadaBase Db = new DadaBase();
                string sql = "select IncomeID,t1.customerID, [Date],IncomeName,Amount,DatePR, ClassificationsName,SubclassificationsName from Income t1 full join Classifications t2 on t1.ClassificationsID = t2.ClassificationsID full join Subclassifications t3 on t1.SubclassificationsID = t3.SubclassificationsID where IncomeName is not null and t1.customerID =" + CId;
                return Db.execute(sql);
            }
            public static DataTable GetAl(int CId,int da)//מביא את כל הכנסות לפי לקוח
            {
                DadaBase Db = new DadaBase();
                string sql = "select IncomeID,t1.customerID, [Date],IncomeName,Amount,DatePR, ClassificationsName,SubclassificationsName from Income t1 full join Classifications t2 on t1.ClassificationsID = t2.ClassificationsID full join Subclassifications t3 on t1.SubclassificationsID = t3.SubclassificationsID where IncomeName is not null and t1.customerID =" + CId + " and [Date]>GETDATE()-" + da + "order by [Date] asc";
                return Db.execute(sql);
            }
            public static DataTable GetAlD(int CId, string da1, string da2)//מביא את כל הכנסות לפי לקוח
            {
                DadaBase Db = new DadaBase();
                string sql = "select IncomeID,t1.customerID, [Date],IncomeName,Amount,DatePR, ClassificationsName,SubclassificationsName from Income t1 full join Classifications t2 on t1.ClassificationsID = t2.ClassificationsID full join Subclassifications t3 on t1.SubclassificationsID = t3.SubclassificationsID where IncomeName is not null and t1.customerID =" + CId + "and [Date]>'" + da1 + "' and [Date]<'" + da2 +"' order by [Date] asc";
                return Db.execute(sql);
            }
            public static bool AddIncome(Income In)

            {

                DadaBase dbc = new DadaBase();
                string sqle = "INSERT into Income (IncomeName, ClassificationsID, SubclassificationsID, CustomerID, Amount, [Date],  DatePR,  PasswordsBAID) values('" + In.IncomeName + "'," + In.ClassificationsID + "," + In.SubclassificationsID + ","  + In.CustomerID + "," + In.Amount + ",'" + In.Date + "','" + In.DatePR + "'," + In.PasswordsBAID +   ")";
                DataTable da = dbc.execute(sqle);
                return true;
            }
            public static DataTable GetBId(int CId, int EId)//מביא הוצאה לפי ידי
            {
                DadaBase Db = new DadaBase();
                string sql = "select IncomeID, [Date],IncomeName,BanksName, Amount,DatePR, ClassificationsName,SubclassificationsName from Income t1 full join Classifications t2 on t1.ClassificationsID = t2.ClassificationsID full join Subclassifications t3 on t1.SubclassificationsID = t3.SubclassificationsID  full join PasswordsBA t4 on t1.PasswordsBAID=t4.PasswordsBAID full join Banks t5 on t4.BanksID=t5.BanksID where IncomeName is not null and t1.customerID =" + EId + "AND IncomeID=" + CId;
                return Db.execute(sql);
            }
            public static bool Delete(int id)
            {
                DadaBase Db = new DadaBase();
                string sql = " DELETE from Income where IncomeID ='" + id + "'";
                DataTable da = Db.execute(sql);
                return true;
            }
            public static bool pot(Income th)

            {

                DadaBase dbc = new DadaBase();
                string sqle = "update Income set ClassificationsID = '" + th.ClassificationsID + "', SubclassificationsID = '" + th.SubclassificationsID + "',[Date] = '" + th.Date + "', Amount = '" + th.Amount + "', DatePR = '" + th.DatePR + "', IncomeName = '" + th.IncomeName + "',  PasswordsBAID ='" + th.PasswordsBAID + "'  , CustomerID= '" + th.CustomerID + "' where IncomeID='" + th.IncomeID + "'";
                DataTable da = dbc.execute(sqle);
                return true;
            }
        }
    }

}