﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CustomerMain/Mana.Master" AutoEventWireup="true" CodeBehind="Classification.aspx.cs" Inherits="Fixel.CustomerMain.Classification" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title>Lorax - Bootstrap 4 Admin Dashboard Template</title>
    <!-- Favicon-->
    <link rel="icon" href="/assets/images/favicon.ico" type="image/x-icon">
    <!-- Plugins Core Css -->
    <link href="/assets/css/app.min.css" rel="stylesheet">
    <link href="/assets/js/bundles/materialize-rtl/materialize-rtl.min.css" rel="stylesheet">
    <!-- Custom Css -->
    <link href="/assets/css/style.css" rel="stylesheet" />
    <!-- You can choose a theme from css/styles instead of get all themes -->
    <link href="/assets/css/styles/all-themes.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="foter" runat="server">
    <section class="content">
        <div class="container-fluid">
        <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>On</strong> Hold</h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <a href="#" onClick="return false;" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                            aria-haspopup="true" aria-expanded="false">
                                            <i class="material-icons">more_vert</i>
                                        </a>
                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <a href="#" onClick="return false;">Action</a>
                                            </li>
                                            <li>
                                                <a href="#" onClick="return false;">Another action</a>
                                            </li>
                                            <li>
                                                <a href="#" onClick="return false;">Something else here</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="board">
                                    <div class="cards" id="b1">
                                        <div class="drop-card"><span class="table-img msg-user">
                                                <img src="/assets/images/user/user5.jpg" alt="">
                                            </span><span class="cardtitle noselect"> User
                                                card #1</span></div>
                                        <div class="drop-card"><span class="table-img msg-user">
                                                <img src="/assets/images/user/user6.jpg" alt="">
                                            </span><span class="cardtitle noselect">User
                                                card #2</span></div>
                                        <div class="drop-card"><span class="table-img msg-user">
                                                <img src="/assets/images/user/user7.jpg" alt="">
                                            </span><span class="cardtitle noselect">User
                                                card #3</span></div>
                                        <div class="drop-card"><span class="table-img msg-user">
                                                <img src="/assets/images/user/user8.jpg" alt="">
                                            </span><span class="cardtitle noselect">User
                                                card #4</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>In</strong> Progress</h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <a href="#" onClick="return false;" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                            aria-haspopup="true" aria-expanded="false">
                                            <i class="material-icons">more_vert</i>
                                        </a>
                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <a href="#" onClick="return false;">Action</a>
                                            </li>
                                            <li>
                                                <a href="#" onClick="return false;">Another action</a>
                                            </li>
                                            <li>
                                                <a href="#" onClick="return false;">Something else here</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="board">
                                    <div class="cards" id="b3">
                                        <div class="drop-card"><span class="table-img msg-user">
                                                <img src="/assets/images/user/user3.jpg" alt="">
                                            </span><span class="cardtitle noselect">User
                                                card #5</span></div>
                                        <div class="drop-card"><span class="table-img msg-user">
                                                <img src="/assets/images/user/user4.jpg" alt="">
                                            </span><span class="cardtitle noselect">User
                                                card #6</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>Done</strong> </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <a href="#" onClick="return false;" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                            aria-haspopup="true" aria-expanded="false">
                                            <i class="material-icons">more_vert</i>
                                        </a>
                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <a href="#" onClick="return false;">Action</a>
                                            </li>
                                            <li>
                                                <a href="#" onClick="return false;">Another action</a>
                                            </li>
                                            <li>
                                                <a href="#" onClick="return false;">Something else here</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="board">
                                    <div class="cards" id="b2">
                                        <div class="drop-card"><span class="table-img msg-user">
                                                <img src="/assets/images/user/user1.jpg" alt="">
                                            </span><span class="cardtitle noselect">User
                                                card #7</span></div>
                                        <div class="drop-card"><span class="table-img msg-user">
                                                <img src="/assets/images/user/user2.jpg" alt="">
                                            </span><span class="cardtitle noselect">User
                                                card #8</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="foter2" runat="server">
        <script src="/assets/js/app.min.js"></script>
    <!-- Custom Js -->
    <script src="/assets/js/admin.js"></script>
    <script src="/assets/js/pages/apps/dragdrop.js"></script>
</asp:Content>
