﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Fixel.BLL;

namespace Fixel.CustomerMain
{
    public partial class ProfitAndLoss : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["User"] == null)
                {
                    Response.Redirect("login.aspx");
                }
                user u = (user)Session["User"];
                int us = (int)u.UId;//id של הלקוח
                ltluesr.Text = "<script >var us=" + us + "; </script>";
                List<chartatem> ExpenseG = chartatem.ProfitLossAll(us);

                int i;

                string html = "";
                for (i = 0; i < ExpenseG.Count; i++)
                {
                    html += "<h1>" + ExpenseG[i].Months + "</h1>";
                }
                RptProd.DataSource = ExpenseG;
                RptProd.DataBind();
            }
        }
    }
   
}