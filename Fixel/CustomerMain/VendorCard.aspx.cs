﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Fixel.BLL;

namespace Fixel.CustomerMain
{
    public partial class VendorCard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["User"] == null)
                {
                    Response.Redirect("login.aspx");
                }
                user u = (user)Session["User"];
                int us = (int)u.UId;//id של הלקוח
                ltluesr.Text = "<script >var us=" + us + "; </script>";
                //Selection list where all Vendors
                List<Vendors> Ven = Vendors.GetAll(us);

                Vendor.DataSource = Ven;
                Vendor.DataBind();
                Vendor.DataTextField = "VendorsName";
                Vendor.DataValueField = "VendorsID";
                Vendor.DataBind();
                Vendor.Items.Insert(0, new ListItem("שם ספק", " 0"));
                Vendor.SelectedIndex = 0;
            }
        }
        protected void period(object sender, EventArgs e)//After choosing a doubt, he will bring his card
        {
            string g = Vendor.SelectedItem.Text;
            user u = (user)Session["User"];
            int us = (int)u.UId;//id של הלקוח
            List<Vendors> ExpenseG = Vendors.GetAllBiV(us,g);
            Vendors Tamp;
            Tamp = new Vendors();
            ExpenseG[0].Subtotal = ExpenseG[0].Amount - ExpenseG[0].payment;
            Tamp.CreditSummary = ExpenseG[0].payment;
            Tamp.RequiredSummary = ExpenseG[0].Amount;
            for (int j =1;j<ExpenseG.Count;j++)
            {
                ExpenseG[j].Subtotal = ExpenseG[j-1].Subtotal;
                ExpenseG[j].Subtotal +=( ExpenseG[j].Amount - ExpenseG[j].payment);
                Tamp.CreditSummary += ExpenseG[j].payment;
                Tamp.RequiredSummary+= ExpenseG[j].Amount;
            }

            Tamp.id = 0;
            Tamp.VendorsName = ExpenseG[0].VendorsName + " " + "יתרה";
            Tamp.Date ="";
            Tamp.payment = Tamp.CreditSummary;
            Tamp.Amount = Tamp.RequiredSummary;
            Tamp.InvoicingNamber =0;
            Tamp.source = "";
            Tamp.DueDate = "";
            Tamp.Subtotal = Tamp.RequiredSummary - Tamp.CreditSummary;

            ExpenseG.Add(Tamp);
            RptProd.DataSource = ExpenseG;
            RptProd.DataBind();
        }
    }
}