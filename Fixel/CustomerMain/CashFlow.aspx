﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CustomerMain/Mana.Master" AutoEventWireup="true" CodeBehind="CashFlow.aspx.cs" Inherits="Fixel.CustomerMain.CashFlow" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
                <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title>Lorax - Bootstrap 4 Admin Dashboard Template</title>
    <!-- Favicon-->
    <link rel="icon" href="/assets/images/favicon.ico" type="image/x-icon"/>
    <!-- Plugins Core Css -->
    <link href="/assets/css/app.min.css" rel="stylesheet"/>
    <link href="/assets/js/bundles/materialize-rtl/materialize-rtl.min.css" rel="stylesheet"/>
    <!-- Custom Css -->
     <script   src="https://code.jquery.com/jquery-3.4.1.js"   integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="  crossorigin="anonymous"></script>

    <link href="/assets/css/style.css" rel="stylesheet"/>
    <!-- You can choose a theme from css/styles instead of get all themes -->
    <link href="/assets/css/styles/all-themes.css" rel="stylesheet" />


    <link href="/assets/js/bundles/materialize-rtl/materialize-rtl.min.css" rel="stylesheet"/>
    <link href="/assets/css/form.min.css" rel="stylesheet"/>
    <!-- Custom Css -->
    <link href="/assets/css/style.css" rel="stylesheet"/>
    <!-- You can choose a theme from css/styles instead of get all themes -->
    <link href="/assets/css/styles/all-themes.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="foter" runat="server">
     <body class="light rtl">
        <asp:Literal ID="ltluesr" runat="server" />
            <section class="content">
        <div class="container-fluid">
                        <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <strong>תזרים מזומנים</strong> </h2>
                            <div class="row">
                            <div class="col-md-4">
                            <button class="dt-button buttons-print" tabindex="0" aria-controls="tableExport" data-toggle="modal" data-target="#gridModal" type="button"><span>שינוי סיסמא של הבנק/אשראי</span></button>
                                </div>

                            
                                </div>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="#" onClick="return false;" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                        aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="#" onClick="return false;">Action</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">Another action</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">Something else here</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
  <%--                              <table id="tableExport" class="display table table-hover table-checkable order-column m-t-20 width-per-100">
                                    <thead>
                                        <tr>
                                            <th>שם ההוצאה</th>
                                            <th>סכום</th>
                                            <th>תאריך פרעון</th>
                                            <th>מקור ההוצאה</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <asp:Repeater ID="RptProd" runat="server">
                                       <ItemTemplate>
                                        <tr>
                                            <td><%#Eval("ExpenseName") %></td>
                                            <td><%#Eval("Amount") %></td>
                                            <td><%#Eval("DueDate") %></td>
                                            <td><%#Eval("BanksName") %></td>
                                        </tr>
                                                                                 </ItemTemplate>
                                     </asp:Repeater>
                                                                            </tbody>
                                    <tfoot>

                                    </tfoot>
                                </table>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
                                    <div class="modal fade" id="gridModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalGrid"
                                        aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalGrid">הוצאות</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="container-fluid">
                    <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="card">
                                        <div class="header">
                                            <h2>
                                                <strong>סיסמא</strong> שינוי </h2>
                                            <ul class="header-dropdown m-r--5">
                                                <li class="dropdown">
                                                    <a href="#" onClick="return false;" class="dropdown-toggle" data-toggle="dropdown"
                                                        role="button" aria-haspopup="true" aria-expanded="false">
                                                        <i class="material-icons">more_vert</i>
                                                    </a>
                                                    <ul class="dropdown-menu pull-right">
                                                        <li>
                                                            <a href="#" onClick="return false;">Action</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" onClick="return false;">Another action</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" onClick="return false;">Something else here</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="body">
                                            <div class="demo-masked-input">
                                                <div class="row clearfix">
                                                   
                                                    <input type="hidden" name="ExpenseID" id="ExpenseID" />
                                                    <div class="col-md-6">
                                                         <b>סיסמא חדשה</b>
                                                        <div class="input-group">
                                                          <span class="input-group-addo">
                                                              </span>
                                                            <div class="form-line">
                                                    		<input id="password"  class="form-control" type="text" name="password" placeholder="נא הזן סיסמא" />
                                                            
                                                           </div>
                                                       </div>
                                                  </div>
                                                    <div class="col-md-6">
                                                        <b>שם הבנק</b>
                                                        <div class="input-group">
                                                               <span class="input-group-addo">
                                                            </span>
                                                            <div class="form-line">
                                                            	<input id="BankName"  class="form-control" type="text" name="BankName" placeholder="שם בנק/אשראי" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                        </div>
                                                  </div>
                          </div>
                                                </div>

                                         <button type="button"  class="btn-hover color-8" id="shlch" onclick="AddCheck()" >עדכון סיסמא</button>


                                                                      


                                            </div>
                                        </div>
                                    </div>
    </div></div>
        </div>   
                                               
            <script>

                //When you click Update or Create New
                function AddCheck() {
                    var pa = $("#password").val();
                    var ba = $("#BankName").val();
                    if (ba != "כאל" && ba != "ישראכרט" && ba != "לאומי קארד") {
                        $.ajax({
                            type: "Put",
                            url: "/api/PasswordsBA/" + pa + "/" + us + "/" + ba,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {

                                Checkbooks = JSON.stringify(data);
                                $("#gridModal").modal('toggle');
                            }
                        })
                    }
                    else {
                        $.ajax({
                            type: "Put",
                            url: "/api/PasswordsCredit/" + pa + "/" + us + "/" + ba,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {

                                Checkbooks = JSON.stringify(data);
                                $("#gridModal").modal('toggle');
                            }
                        })
                    }
                   
                }

            </script>
        </div>
    </section>

        </body>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="foter2" runat="server">
                <script src="/assets/js/app.min.js"></script>
    <script src="/assets/js/table.min.js"></script>
    <!-- Custom Js -->
    <script src="/assets/js/admin.js"></script>
    <script src="/assets/js/bundles/export-tables/dataTables.buttons.min.js"></script>
    <script src="/assets/js/bundles/export-tables/buttons.flash.min.js"></script>
    <script src="/assets/js/bundles/export-tables/jszip.min.js"></script>
    <script src="/assets/js/bundles/export-tables/pdfmake.min.js"></script>
    <script src="/assets/js/bundles/export-tables/vfs_fonts.js"></script>
    <script src="/assets/js/bundles/export-tables/buttons.html5.min.js"></script>
    <script src="/assets/js/bundles/export-tables/buttons.print.min.js"></script>
    <script src="/assets/js/pages/tables/jquery-datatable.js"></script>       
    <script src="/assets/js/form.min.js"></script>
    <script src="/assets/js/bundles//multiselect/js/jquery.multi-select.js"></script>
    <script src="/assets/js/bundles/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js"></script>
    <!-- Custom Js -->
    <script src="/assets/js/pages/forms/advanced-form-elements.js"></script>
        <script src="/assets/js/pages/forms/basic-form-elements.js"></script>
</asp:Content>
