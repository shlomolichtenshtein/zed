﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CustomerMain/Mana.Master" AutoEventWireup="true" CodeBehind="VendorCard.aspx.cs" Inherits="Fixel.CustomerMain.VendorCard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <meta charset="UTF-8"/>
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title>Lorax - Bootstrap 4 Admin Dashboard Template</title>
    <!-- Favicon-->
    <link rel="icon" href="/assets/images/favicon.ico" type="image/x-icon"/>
    <!-- Plugins Core Css -->
    <link href="/assets/css/app.min.css" rel="stylesheet"/>
    <link href="/assets/js/bundles/materialize-rtl/materialize-rtl.min.css" rel="stylesheet"/>
    <!-- Custom Css -->
    <link href="/assets/css/style.css" rel="stylesheet"/>
    <!-- You can choose a theme from css/styles instead of get all themes -->
    <link href="/assets/css/styles/all-themes.css" rel="stylesheet" />
        <link href="/assets/css/form.min.css" rel="stylesheet"/>


    <link href="/assets/js/bundles/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css" rel="stylesheet" />
    <!-- Multi Select Css -->
    <link href="/assets/js/bundles/multiselect/css/multi-select.css" rel="stylesheet">



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="foter" runat="server">
      <ItemTemplate>
    <body class="light rtl">
                <asp:Literal ID="ltluesr" runat="server" />
                        <section class="content">


        <div class="container-fluid">
                        <div class="block-header">
                                                     <div class="demo-masked-input">
                        <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="row">
                            <div class="col-md-4">
                            <h2>
                                <strong>כרטסת ספקים</strong> </h2>
                                </div>                                           
                            <div class="col-md-4">
                                                         <asp:DropDownList ID="Vendor" runat="server" DataTextField="VendorsName" DataValueField="VendorsID" CssClass="form-group default-select" >
                                                             <asp:ListItem></asp:ListItem>
                                                          </asp:DropDownList>
                              </div>                                          
                                <asp:Button ID="performance" class="dt-button buttons-print" OnClick="period" runat="server" Text="הצג" />
                                </div>
                            
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="#" onClick="return false;" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                        aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="#" onClick="return false;">Action</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">Another action</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">Something else here</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table id="tableExport" class="display table table-hover table-checkable order-column m-t-20 width-per-100">
                                    <thead>
                                        <tr>
                                            <th>שם ספק</th>
                                            <th>תאריך</th>                                           
                                            <th>מס' חשבונית</th>
                                            <th>אסמכתא</th>
                                            <th>תאריך פרעון שיק</th>
                                            <th>חיוב</th>
                                            <th>זיכוי</th>
                                            <th>יתרה</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <asp:Repeater ID="RptProd" runat="server">
                                       <ItemTemplate>
                                        <tr>
                                             <td><%#Eval("VendorsName") %></td>
                                            <td><%#Eval("Date") %></td>
                                            <td><%#Eval("InvoicingNamber") %></td>
                                            <td><%#Eval("source") %></td>
                                            <td><%#Eval("DueDate") %></td>
                                            <td><p class="font-bold col-pink"><%#Eval("Amount") %></p></td>
                                            <td><p class="font-bold col-teal"><%#Eval("payment") %></p></td>
                                            <td><p class="font-bold col-orange"><%#Eval("Subtotal") %></p></td>
                                        </tr>
                                                                                 </ItemTemplate>
                                     </asp:Repeater>
                                                                            </tbody>
                                    <tfoot>

                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
            </div>
                            </section>
        </body>
      </ItemTemplate>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="foter2" runat="server">
        <script src="/assets/js/app.min.js"></script>
    <script src="/assets/js/table.min.js"></script>
    <!-- Custom Js -->
    <script src="/assets/js/admin.js"></script>
    <script src="/assets/js/bundles/export-tables/dataTables.buttons.min.js"></script>
    <script src="/assets/js/bundles/export-tables/buttons.flash.min.js"></script>
    <script src="/assets/js/bundles/export-tables/jszip.min.js"></script>
    <script src="/assets/js/bundles/export-tables/pdfmake.min.js"></script>
    <script src="/assets/js/bundles/export-tables/vfs_fonts.js"></script>
    <script src="/assets/js/bundles/export-tables/buttons.html5.min.js"></script>
    <script src="/assets/js/bundles/export-tables/buttons.print.min.js"></script>
    <script src="/assets/js/pages/tables/jquery-datatable.js"></script>
        <script src="/assets/js/pages/forms/advanced-form-elements.js"></script>
        <script src="/assets/js/pages/forms/basic-form-elements.js"></script>
        <script src="/assets/js/form.min.js"></script>


    <script src="/assets/js/bundles//multiselect/js/jquery.multi-select.js"></script>
    <script src="/assets/js/bundles/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js"></script>
    <!-- Custom Js -->
    <script src="/assets/js/admin.js"></script>
    <script src="/assets/js/pages/forms/advanced-form-elements.js"></script>
    <!-- Demo Js -->


</asp:Content>
