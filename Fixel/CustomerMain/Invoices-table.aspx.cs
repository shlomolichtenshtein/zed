﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Fixel.BLL;

namespace Fixel.CustomerMain
{
    public partial class Invoices_table : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["User"] == null)
                {
                    Response.Redirect("login.aspx");
                }
                user u = (user)Session["User"];
                int us = (int)u.UId;//id של הלקוח
                ltluesr.Text = "<script >var us=" + us + "; </script>";
                //Selection list where all Vendors
                List<Vendors> Ven = Vendors.GetAll(us);

                Vendor.DataSource = Ven;
                Vendor.DataBind();
                Vendor.DataTextField = "VendorsName";
                Vendor.DataValueField = "VendorsID";
                Vendor.DataBind();
                Vendor.Items.Insert(0, new ListItem("שם ספק", " 0"));
                Vendor.SelectedIndex = 0;
            }
        }
        protected void period(object sender, EventArgs e)//After choosing a provider, he will bring his invoices
        {
            string g = Vendor.SelectedItem.Text;
            user u = (user)Session["User"];
            int us = (int)u.UId;//id של הלקוח
            List<Invoicing> ExpenseG = Invoicing.GetAllBiI(us, g);
            int i;
            string html = "";
            for (i = 0; i < ExpenseG.Count; i++)
            {
                html += "<h1>" + ExpenseG[i].date + "</h1>";
            }

            RptProd.DataSource = ExpenseG;
            RptProd.DataBind();
        }

        protected void Treatme_Click(object sender, EventArgs e)
        {
            user u = (user)Session["User"];
            int us = (int)u.UId;//id של הלקוח
            List<Invoicing> ExpenseG = Invoicing.GetTreatment(us);
            int i;
            string html = "";
            for (i = 0; i < ExpenseG.Count; i++)
            {
                html += "<h1>" + ExpenseG[i].date + "</h1>";
            }

            RptProd.DataSource = ExpenseG;
            RptProd.DataBind();
        }
    }
}