﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Fixel.BLL;

namespace Fixel.CustomerMain
{
    public partial class Income_table : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["User"] == null)
                {
                    Response.Redirect("login.aspx");
                }
                user u = (user)Session["User"];
                int us = (int)u.UId;//id של הלקוח
                ltluesr.Text = "<script >var us=" + us + "; </script>";
                List<Classifications> cla = Classifications.GetAll();
                classific.DataSource = cla;
                classific.DataBind();
                classific.DataTextField = "ClassificationsName";
                classific.DataValueField = "ClassificationsID";
                classific.DataBind();
                classific.Items.Insert(0, new ListItem("עסק", " 2"));
                classific.SelectedIndex = 0;

                List<Subclassifications> sub = Subclassifications.GetAll();
                Subcla.DataSource = sub;
                Subcla.DataBind();
                Subcla.DataTextField = "SubclassificationsName";
                Subcla.DataValueField = "SubclassificationsID";
                Subcla.DataBind();
                Subcla.Items.Insert(0, new ListItem("מכירות", " 12"));
                Subcla.SelectedIndex = 0;

                List<Income> ExpenseG = Income.GetAl(us,30);

                int i;

                string html = "";
                for (i = 0; i < ExpenseG.Count; i++)
                {
                    html += "<h1>" + ExpenseG[i].Date + "</h1>";
                }
                RptProd.DataSource = ExpenseG;
                RptProd.DataBind();

            }

        }
        protected void period(object sender, EventArgs e)
        {
            string g = DropDownList1.SelectedValue;
            int j = int.Parse(g);
            user u = (user)Session["User"];
            int us = (int)u.UId;//id של הלקוח
            if (DropDownList1.SelectedValue == "0")
            {
                string dat1 = Utils.GlobFuncs.dateadd(dateA.Text);
                string dat2 = Utils.GlobFuncs.dateadd(dateB.Text);
                List<Income> ExpenseG = Income.GetAlD(us, dat1, dat2);
                int i;
                string html = "";
                for (i = 0; i < ExpenseG.Count; i++)
                {
                    html += "<h1>" + ExpenseG[i].Date + "</h1>";
                }
                RptProd.DataSource = ExpenseG;
                RptProd.DataBind();

            }
            else
            {
                List<Income> ExpenseG = Income.GetAl(us, j);
                int i;
                string html = "";
                for (i = 0; i < ExpenseG.Count; i++)
                {
                    html += "<h1>" + ExpenseG[i].Date + "</h1>";
                }
                RptProd.DataSource = ExpenseG;
                RptProd.DataBind();
            }


        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            adae1.Visible = adae2.Visible = DropDownList1.SelectedValue == "0";
        }
    }
}
