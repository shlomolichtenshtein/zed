﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CustomerMain/Mana.Master" AutoEventWireup="true" CodeBehind="BusinessExpensesM.aspx.cs" Inherits="Fixel.CustomerMain.BusinessExpensesM" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title>Lorax - Bootstrap 4 Admin Dashboard Template</title>
    <!-- Favicon-->
    <link rel="icon" href="/assets/images/favicon.ico" type="image/x-icon">
    <!-- Plugins Core Css -->
    <link href="/assets/css/app.min.css" rel="stylesheet">
    <link href="/assets/js/bundles/materialize-rtl/materialize-rtl.min.css" rel="stylesheet">
    <!-- Custom Css -->
    <link href="/assets/css/style.css" rel="stylesheet" />
    <!-- You can choose a theme from css/styles instead of get all themes -->
    <link href="/assets/css/styles/all-themes.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="foter" runat="server">
        <ItemTemplate>
    <body class="light rtl">
                <asp:Literal ID="ltluesr" runat="server" />
                        <section class="content">


        <div class="container-fluid">
                        <div class="block-header">
                                                     <div class="demo-masked-input">
                        <div class="row clearfix">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">

                            <h2>
                                <strong>יתרות ספקים</strong> </h2>
                          
                                        

                            

                            
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="#" onClick="return false;" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                        aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="#" onClick="return false;">Action</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">Another action</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">Something else here</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table id="tableExport" class="display table table-hover table-checkable order-column m-t-20 width-per-100">
                                    <thead>
                                        <tr>
                                            <th>חודש</th>
                                            <th>שנה</th>                                           
                                            <th>מיסים</th>
                                            <th>משכורת</th>
                                            <th>שכירות</th>
                                            <th>פירסום</th>
                                            <th>רכב</th>
                                            <th>דלק</th>
                                            <th>ביטוחים</th>
                                            <th>אחזקת משרד</th>
                                            <th>מימון</th>
                                            <th>שונות</th>
                                            <th>מכירות</th>
                                            <th>תקשורת</th>
                                            <th>עמלות</th>
                                            <th>הלוואות ומשכנתא</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <asp:Repeater ID="RptProd" runat="server">
                                       <ItemTemplate>
                                        <tr>
                                             <td><%#Eval("Months") %></td>
                                            <td><%#Eval("Years") %></p></td>
                                            <td><p class="font-bold col-teal"><%#Eval("SumF") %></p></td>
                                        </tr>
                                                                                 </ItemTemplate>
                                     </asp:Repeater>
                                                                            </tbody>
                                    <tfoot>

                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
            </div>
                            </section>
        </body>
      </ItemTemplate>


    <body class="light rtl">
        <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
           <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="header">
                            <h2>Pie Chart</h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="#" onClick="return false;" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                        aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="#" onClick="return false;">Action</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">Another action</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">Something else here</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div id="echart_pie" class="chartsh chart-shadow2"></div>
                        </div>
                    </div>
                </div>
               </div>
            </div>
            </section>
        </body>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="foter2" runat="server">
        <script src="/assets/js/app.min.js"></script>
    <script src="/assets/js/chart.min.js"></script>
    <!-- Echart Js -->
    <script src="/assets/js/bundles/echart/echarts.js"></script>
<%--    <script src="/assets/js/pages/charts/echarts.js"></script>--%>
    <!-- Custom Js -->
    <script src="/assets/js/admin.js"></script>
    <script src="../js/BusinessExpensesMJS.js"></script>
</asp:Content>
