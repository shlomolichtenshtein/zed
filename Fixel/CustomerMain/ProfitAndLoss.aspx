﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CustomerMain/Mana.Master" AutoEventWireup="true" CodeBehind="ProfitAndLoss.aspx.cs" Inherits="Fixel.CustomerMain.ProfitAndLoss" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
                  <meta charset="UTF-8"/>
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title>Lorax - Bootstrap 4 Admin Dashboard Template</title>
    <!-- Favicon-->
    <link rel="icon" href="/assets/images/favicon.ico" type="image/x-icon"/>
    <!-- Plugins Core Css -->
    <link href="/assets/css/app.min.css" rel="stylesheet"/>
    <link href="/assets/js/bundles/materialize-rtl/materialize-rtl.min.css" rel="stylesheet"/>
    <!-- Custom Css -->
    <link href="/assets/css/style.css" rel="stylesheet"/>
    <!-- You can choose a theme from css/styles instead of get all themes -->
    <link href="/assets/css/styles/all-themes.css" rel="stylesheet" />
        <link href="/assets/css/form.min.css" rel="stylesheet"/>


    <link href="/assets/js/bundles/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css" rel="stylesheet" />
    <!-- Multi Select Css -->
    <link href="/assets/js/bundles/multiselect/css/multi-select.css" rel="stylesheet"/>

    <link href="/css/Table.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="foter" runat="server">
          <ItemTemplate>
    <body class="light rtl">
                <asp:Literal ID="ltluesr" runat="server" />
                        <section class="content">


        <div class="container-fluid">
                        <div class="block-header">
                                                     <div class="demo-masked-input">
                        <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">

                            <h2>
                                <strong>דווח רווח והפסד</strong> </h2>
                          
                                        

                            

                            
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="#" onClick="return false;" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                        aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="#" onClick="return false;">Action</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">Another action</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">Something else here</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table id="tableExport" class="display table table-hover table-checkable order-column m-t-20 width-per-100">
                                    <thead>
                                        <tr>
                                            <th>שנה</th>
                                            <th>חודש</th>                                           
                                            <th>רווח גולמי</th>
                                            <th>עובדים</th>
                                            <th>שאר הוצאות העסק</th>
                                             <th>סך הוצאות</th>
                                            <th>רווח נקי</th>                                           
                                            <th>הלוואות ומשכתא</th>
                                            <th>רביות על הלוואות</th>
                                            <th>סך החדרי הלוואות</th>
                                            <th>משכורת מנכל</th>
                                            <th>רווח סופי נקי</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <asp:Repeater ID="RptProd" runat="server">
                                       <ItemTemplate>
                                        <tr>
                                             <td><%#Eval("Years") %></td>
                                            <td><%#Eval("Months") %></td>
                                            <td><p class="font-bold col-teal"><%#Eval("GrossProfit") %></p></td>
                                            <td>
                                                <button type="button" id="salary" class="btn tblActnBtn" data-toggle="modal" data-target="#gridModal" onclick="modalA(<%#Eval("Years") %>, <%#Eval("Months")%>,1)" >
                                                    <p class="font-bold col-pink"><%#Eval("SalaryEmployees") %></p>
                                                </button>
                                            </td>
                                            <td>
                                                <button type="button" id="Other" class="btn tblActnBtn" data-toggle="modal" data-target="#gridModal" onclick="modalA(<%#Eval("Years") %>, <%#Eval("Months")%>,2)" >
                                                    <p class="font-bold col-pink"><%#Eval("OtherBusinessExpenses") %></p>
                                                </button>
                                            </td>
                                            <td><p class="font-bold col-pink"><%#Eval("Expend") %></p></td>
                                            <td><p class="font-bold col-orange"><%#Eval("Income") %></p></td>
                                            <td>
                                                <button type="button" id="Mortgag" class="btn tblActnBtn" data-toggle="modal" data-target="#gridModal" onclick="modalA(<%#Eval("Years") %>, <%#Eval("Months")%>,3)" >
                                                    <p class="font-bold col-pink"><%#Eval("Mortgage") %></p>
                                                </button>
                                            </td>
                                            <td>
                                                <button type="button" id="interes" class="btn tblActnBtn" data-toggle="modal" data-target="#gridModal" onclick="modalA(<%#Eval("Years") %>, <%#Eval("Months")%>,4)" >
                                                    <p class="font-bold col-pink"><%#Eval("interest") %></p>
                                                </button>
                                            </td>
                                            <td><p class="font-bold col-pink"><%#Eval("Sales") %></p></td>
                                            <td>
                                                <button type="button" id="CEOSalar" class="btn tblActnBtn" data-toggle="modal" data-target="#gridModal" onclick="modalA(<%#Eval("Years") %>, <%#Eval("Months")%>,5)" >
                                                    <p class="font-bold col-pink"><%#Eval("CEOSalary") %></p>
                                                </button>
                                            </td>
                                            <td><p class="font-bold col-orange"><%#Eval("SumF") %></p></td>
                                        </tr>
                                                                                 </ItemTemplate>
                                     </asp:Repeater>
                                                                            </tbody>
                                    <tfoot>

                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
            </div>
                             <div class="modal fade" id="gridModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalGrid"
                                        aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalGrid">פירוט</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="container-fluid">
                    <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="card">
                                        <div class="header">
                                            <h2>
                                                <strong>חדש</strong> עדכון</h2>
                                            <ul class="header-dropdown m-r--5">
                                                <li class="dropdown">
                                                    <a href="#" onClick="return false;" class="dropdown-toggle" data-toggle="dropdown"
                                                        role="button" aria-haspopup="true" aria-expanded="false">
                                                        <i class="material-icons">more_vert</i>
                                                    </a>
                                                    <ul class="dropdown-menu pull-right">
                                                        <li>
                                                            <a href="#" onClick="return false;">Action</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" onClick="return false;">Another action</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" onClick="return false;">Something else here</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="body">
                                            <div class="demo-masked-input">
                                                <div class="row clearfix" id="mpa">
                                                    <fieldset id="Profit">
                                                     </fieldset>
      
                                                </div>
                                                        </div>
                                         </div>
                                            <div class="body">
                                                <div class="recent-report__chart" id="c">
                                                     <canvas id="singel-bar-chart"></canvas>
                                                </div>
                                             </div>
                          </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
    </div></div>
        </div>  
                            <script>
                                function modalA(id, id2, id3) {
                                    $('#Profit').remove();
                                    $('#mpa').append('  <fieldset id="Profit"></fieldset >');
                                    var cy = [];
                                    var cm = [];
                                    var cs = [];
                                    $.ajax({
                                        type: "GET",
                                        url: "/api/Chartatem/" + us + "/" + id + "/" + id2 +"/" + id3,

                                        contentType: "application/json; charset=utf-8",
                                        dataType: "json",
                                        success: function (data) {

                                            Chartatem = JSON.parse(data);
                                            var fieldWrapper = $("<div class=\"body\"><div class=\"table - responsive\"><table id=\"tableProfit\" class=\"display table table - hover table - checkable order - column m - t - 20 width - per - 100\"><thead><tr><th>שם</th><th>סכום</th> </tr ></thead ><tbody> "); 
                                            fieldWrapper.data("idx", id3);
                                            for (var i = 0; i < Chartatem.length; i++) {
                                                if (Chartatem[i].Name != null) {
                                                    var fName = $("<tr><td>" + Chartatem[i].Name + "</td><td>" + Chartatem[i].Sales + "</td></tr > ");
                                                    fieldWrapper.append(fName);
                                                }
                                            }
                                            var fn = $("   </tbody></table> </div> </div>");
                                            fieldWrapper.append(fn);
                                            $("#Profit").append(fieldWrapper);
                                            $('#singel-bar-chart').remove();
                                            $('#c').append('<canvas id="singel-bar-chart"><canvas>');
                                            try {
                                                var j = 0;
                                                for (var i = 0; i < Chartatem.length; i++) {
                                                    if (Chartatem[i].Months != 0) {
                                                        cy[j] = Chartatem[i].Years;
                                                        cm[j] = Chartatem[i].Months;
                                                        cs[j] = Chartatem[i].SumF;
                                                        j++;
                                                    }




                                                }



                                                // single bar chart
                                                var ctx = document.getElementById("singel-bar-chart");
                                                if (ctx) {
                                                    ctx.height = 150;
                                                    var myChart = new Chart(ctx, {
                                                        type: 'bar',
                                                        data: {
                                                            labels: cm,
                                                            datasets: [{
                                                                label: "עלות בתווך 6 חודשים ",
                                                                data: cs,
                                                                borderColor: "rgba(0, 123, 255, 0.9)",
                                                                borderWidth: "0",
                                                                backgroundColor: "rgba(0, 200, 255, 0.5)"
                                                            }]
                                                        },
                                                        options: {
                                                            legend: {
                                                                position: 'top',
                                                                labels: {
                                                                    fontFamily: 'Poppins'
                                                                }

                                                            },
                                                            scales: {
                                                                xAxes: [{
                                                                    ticks: {
                                                                        fontFamily: "Poppins",
                                                                        fontColor: "#9aa0ac", // Font Color

                                                                    }
                                                                }],
                                                                yAxes: [{
                                                                    ticks: {
                                                                        beginAtZero: true,
                                                                        fontFamily: "Poppins",
                                                                        fontColor: "#9aa0ac", // Font Color
                                                                    }
                                                                }]
                                                            }
                                                        }
                                                    });
                                                }

                                            } catch (error) {
                                                console.log(error);
                                            }

                                        },
                                        failure: function (errMsg) {
                                            alert(errMsg);
                                        }
                                    });
                                }
                            </script>
                            </section>
        </body>
      </ItemTemplate>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="foter2" runat="server">
                 <script src="/assets/js/app.min.js"></script>
    <script src="/assets/js/table.min.js"></script>
    <!-- Custom Js -->
    <script src="/assets/js/admin.js"></script>
    <script src="/assets/js/bundles/export-tables/dataTables.buttons.min.js"></script>
    <script src="/assets/js/bundles/export-tables/buttons.flash.min.js"></script>
    <script src="/assets/js/bundles/export-tables/jszip.min.js"></script>
    <script src="/assets/js/bundles/export-tables/pdfmake.min.js"></script>
    <script src="/assets/js/bundles/export-tables/vfs_fonts.js"></script>
    <script src="/assets/js/bundles/export-tables/buttons.html5.min.js"></script>
    <script src="/assets/js/bundles/export-tables/buttons.print.min.js"></script>
    <script src="/assets/js/pages/tables/jquery-datatable.js"></script>
        <script src="/assets/js/pages/forms/advanced-form-elements.js"></script>
        <script src="/assets/js/pages/forms/basic-form-elements.js"></script>
        <script src="/assets/js/form.min.js"></script>


    <script src="/assets/js/bundles//multiselect/js/jquery.multi-select.js"></script>
    <script src="/assets/js/bundles/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js"></script>
    <!-- Custom Js -->
    <script src="/assets/js/admin.js"></script>
    <script src="/assets/js/pages/forms/advanced-form-elements.js"></script>

    <script src="/assets/js/chart.min.js"></script>
    <!-- Echart Js -->
    <script src="/assets/js/bundles/echart/echarts.js"></script>

</asp:Content>
