﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="Fixel.login" %>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<title>Lorax - Bootstrap 4 Admin Dashboard Template</title>
	<!-- Favicon-->
	<link rel="icon" href="../../assets/images/favicon.ico" type="image/x-icon">
	<!-- Plugins Core Css -->
	<link href="../../assets/css/app.min.css" rel="stylesheet">
    <link href="../../assets/js/bundles/materialize-rtl/materialize-rtl.min.css" rel="stylesheet">
	<!-- Custom Css -->
	<link href="../../assets/css/style.css" rel="stylesheet" />
	<link href="../../assets/css/pages/extra_pages.css" rel="stylesheet" />
</head>
<body class="login-page rtl">
	<div class="limiter">
		<div class="container-login100 page-background">
			<div class="wrap-login100">
				<form class="login100-form validate-form" runat="server" >
					<span class="login100-form-logo">
						<img src="manage/logo.png" alt="" />
					</span>
					<span class="login100-form-title p-b-34 p-t-27">
						התחברות
					</span>
					<div class="wrap-input100 validate-input" data-validate="Enter username">
						<asp:TextBox ID="TextBox1" runat="server" class="input100" type="text" name="pass" placeholder="שם משתמש"></asp:TextBox>
						<i class="material-icons focus-input1001">person</i>
					</div>
					<div class="wrap-input100 validate-input" data-validate="Enter password">
						 <asp:TextBox ID="TextBox" runat="server" class="input100" type="password" name="pass" placeholder="סיסמא"></asp:TextBox>
						<i class="material-icons focus-input1001">lock</i>
					</div>
                     <div class="font-bold col-pink">
                        <asp:Literal ID="fals" runat="server"  ></asp:Literal>
                    </div>
					<div class="contact100-form-checkbox">
						<div class="form-check">
							<label class="form-check-label">
								<input class="form-check-input" type="checkbox" value=""> זכור אותי
								<span class="form-check-sign">
									<span class="check"></span>
								</span>
							</label>
						</div>
					</div>
					<div class="container-login100-form-btn">
                        <asp:Button  class="login100-form-btn" ID="shlch" OnClick="shlch_Click" runat="server"  Text="התחבר"   ></asp:Button>
					</div>
					<div class="text-center p-t-50">
						<a class="txt1" href="forgot-password.html">
							שכחת את הסיסמא?
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Plugins Js -->
	<script src="../../assets/js/app.min.js"></script>
	<!-- Extra page Js -->
	<script src="../../assets/js/pages/examples/pages.js"></script>
</body>
</html>
