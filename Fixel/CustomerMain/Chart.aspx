﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CustomerMain/Mana.Master" AutoEventWireup="true" CodeBehind="Chart.aspx.cs" Inherits="Fixel.CustomerMain.Chart" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="foter" runat="server">
    <div class="light rtl">
                <asp:Literal ID="ltluesr" runat="server" />
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="header">
                            <h2>דווח מכירות מול קניות עם רווח גולמי של השנה האחרונה</h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="#" onClick="return false;" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="#" onClick="return false;">Action</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">Another action</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">Something else here</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                        

                           

                                
                            <div id="echart_area_line" class="chartsh chart-shadow2"></div>
                        </div>
                    </div>

                   
                </div>
                  <div class="col-lg-6">
                    <div class="card">
                        <div class="header">
                            <h2>
                                רווח גולמי באחוזים</h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="#" onClick="return false;" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                        aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="#" onClick="return false;">Action</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">Another action</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">Something else here</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="recent-report__chart">
                                <canvas id="singel-bar-chart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="foter2" runat="server">
            <script src="/assets/js/app.min.js"></script>
    <script src="/assets/js/chart.min.js"></script>
    <!-- Echart Js -->
    <script src="/assets/js/bundles/echart/echarts.js"></script>
     <script type = "text / javascript" src = "/js/expenschart.js"></script>
    <!-- Custom Js -->
    <script src="/assets/js/admin.js"></script>
       <script src="/js/expenschart.js"></script>
</asp:Content>
