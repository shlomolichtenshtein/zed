﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CustomerMain/Mana.Master" AutoEventWireup="true" CodeBehind="Overview.aspx.cs" Inherits="Fixel.CustomerMain.Overview" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title>Lorax - Bootstrap 4 Admin Dashboard Template</title>
    <!-- Favicon-->
    <link rel="icon" href="/assets/images/favicon.ico" type="image/x-icon">
    <!-- Plugins Core Css -->
    <link href="/assets/css/app.min.css" rel="stylesheet">
    <link href="/assets/js/bundles/materialize-rtl/materialize-rtl.min.css" rel="stylesheet">
    <!-- Custom Css -->
    <link href="/assets/css/style.css" rel="stylesheet" />
    <!-- You can choose a theme from css/styles instead of get all themes -->
    <link href="/assets/css/styles/all-themes.css" rel="stylesheet" />
    <link href="../css/Overview.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="foter" runat="server">

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item">
                                <h4 class="page-title">סקירה כללית</h4>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-sm-6">
                    <div class="m">
                       
                        <h3 class="m-b-0">
                             <i class="material-icons">check</i>מצב התזרים לחודש הקרוב</h3>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6">
                    <div class="m">
                       
                        <h3 class="m-b-0">
                             <i class="material-icons">check</i>מצב התזרים לחודש הקרוב</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8">
                    <div class="card">
                        <div class="header">
<div class="row">
    <div class="col-md-2">
      <%--  <input class="border-bottom-color=#20c997" value="hhh"/>--%>
        
<div class="font-bold">פאגי 99999</div>
        <div class="font-bold col-teal"><h3>66666</h3></div>
    </div>
        <div class="col-md-2">
<div class="font-bold">פאגי 99999</div>
        <div class="font-bold col-teal"><h3 onclick="a">66666</h3></div>
    </div>

                        </div>
                        <div class="body">
                            <div class="recent-report__chart">
                                <canvas id="line-chart2"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                    </div>
                <div class="col-lg-4">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <strong>Recent</strong> Report</h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="#" onClick="return false;" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                        aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="#" onClick="return false;">Action</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">Another action</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">Something else here</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                             <img class="ng-star-inserted" src="https://bsecure.bizibox.biz/assets/images/alerts-all-caught.png" width="80" height="80" alt="admin"/>
                              
                        </div>
                    </div>
                </div>

                </div>
        <div class="row">
                <div class="col-lg-8">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <strong>Doughut</strong> Chart</h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="#" onClick="return false;" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                        aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="#" onClick="return false;">Action</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">Another action</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">Something else here</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="row">
                            <div class="col-sm-5">
                            <div class="recent-report__chart">
                                <canvas id="doughut-chart"></canvas>
                            </div>
                                </div>
                                <div class="col-sm-2">
                                      <div class="recent-report__chart">
                                <canvas id="bar-chart"></canvas>
                            </div>
                                </div>
                              <div class="col-sm-5">
                                    <div class="recent-report__chart">
                                <canvas id="doughut-charta"></canvas>
                            </div>
                                  </div>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                         <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <strong>Single</strong> Bar Chart</h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="#" onClick="return false;" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                        aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="#" onClick="return false;">Action</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">Another action</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">Something else here</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
<%--                            <div class="recent-report__chart">
                                <canvas id="singel-bar-chart"></canvas>
                            </div>--%>
                        </div>
                    </div>
                </div>
                        <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <strong>New </strong>Projects</h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="#" onClick="return false;" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                        aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="#" onClick="return false;">Action</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">Another action</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">Something else here</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div id="new-orders" class="media-list position-relative">
                                <div class="table-responsive">
                                    <table id="new-orders-table" class="table table-hover table-xl mb-0">
                                        <thead>
                                            <tr>
                                                <th class="border-top-0">Product</th>
                                                <th class="border-top-0">Employees</th>
                                                <th class="border-top-0">Cost</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="text-truncate">iPhone X</td>
                                                <td class="text-truncate">
                                                    <ul class="list-unstyled order-list">
                                                        <li class="avatar avatar-sm">
                                                            <img class="rounded-circle" src="/assets/images/user/user1.jpg"
                                                                alt="user">
                                                        </li>
                                                        <li class="avatar avatar-sm">
                                                            <img class="rounded-circle" src="/assets/images/user/user2.jpg"
                                                                alt="user">
                                                        </li>
                                                        <li class="avatar avatar-sm">
                                                            <img class="rounded-circle" src="/assets/images/user/user3.jpg"
                                                                alt="user">
                                                        </li>
                                                        <li class="avatar avatar-sm">
                                                            <span class="badge">+4</span>
                                                        </li>
                                                    </ul>
                                                </td>
                                                <td class="text-truncate">$8999</td>
                                            </tr>
                                            <tr>
                                                <td class="text-truncate">Pixel 2</td>
                                                <td class="text-truncate">
                                                    <ul class="list-unstyled order-list">
                                                        <li class="avatar avatar-sm">
                                                            <img class="rounded-circle" src="/assets/images/user/user1.jpg"
                                                                alt="user">
                                                        </li>
                                                        <li class="avatar avatar-sm">
                                                            <img class="rounded-circle" src="/assets/images/user/user2.jpg"
                                                                alt="user">
                                                        </li>
                                                        <li class="avatar avatar-sm">
                                                            <img class="rounded-circle" src="/assets/images/user/user3.jpg"
                                                                alt="user">
                                                        </li>
                                                        <li class="avatar avatar-sm">
                                                            <span class="badge">+4</span>
                                                        </li>
                                                    </ul>
                                                </td>
                                                <td class="text-truncate">$5550</td>
                                            </tr>
                                            <tr>
                                                <td class="text-truncate">OnePlus</td>
                                                <td class="text-truncate">
                                                    <ul class="list-unstyled order-list">
                                                        <li class="avatar avatar-sm">
                                                            <img class="rounded-circle" src="/assets/images/user/user1.jpg"
                                                                alt="user">
                                                        </li>
                                                        <li class="avatar avatar-sm">
                                                            <img class="rounded-circle" src="/assets/images/user/user2.jpg"
                                                                alt="user">
                                                        </li>
                                                        <li class="avatar avatar-sm">
                                                            <img class="rounded-circle" src="/assets/images/user/user3.jpg"
                                                                alt="user">
                                                        </li>
                                                        <li class="avatar avatar-sm">
                                                            <span class="badge">+4</span>
                                                        </li>
                                                    </ul>
                                                </td>
                                                <td class="text-truncate">$9000</td>
                                            </tr>
                                            <tr>
                                                <td class="text-truncate">Galaxy</td>
                                                <td class="text-truncate">
                                                    <ul class="list-unstyled order-list">
                                                        <li class="avatar avatar-sm">
                                                            <img class="rounded-circle" src="/assets/images/user/user1.jpg"
                                                                alt="user">
                                                        </li>
                                                        <li class="avatar avatar-sm">
                                                            <img class="rounded-circle" src="/assets/images/user/user2.jpg"
                                                                alt="user">
                                                        </li>
                                                        <li class="avatar avatar-sm">
                                                            <img class="rounded-circle" src="/assets/images/user/user3.jpg"
                                                                alt="user">
                                                        </li>
                                                        <li class="avatar avatar-sm">
                                                            <span class="badge">+4</span>
                                                        </li>
                                                    </ul>
                                                </td>
                                                <td class="text-truncate">$7500</td>
                                            </tr>
                                            <tr>
                                                <td class="text-truncate">Moto Z2</td>
                                                <td class="text-truncate">
                                                    <ul class="list-unstyled order-list">
                                                        <li class="avatar avatar-sm">
                                                            <img class="rounded-circle" src="/assets/images/user/user1.jpg"
                                                                alt="user">
                                                        </li>
                                                        <li class="avatar avatar-sm">
                                                            <img class="rounded-circle" src="/assets/images/user/user2.jpg"
                                                                alt="user">
                                                        </li>
                                                        <li class="avatar avatar-sm">
                                                            <img class="rounded-circle" src="/assets/images/user/user3.jpg"
                                                                alt="user">
                                                        </li>
                                                        <li class="avatar avatar-sm">
                                                            <span class="badge">+4</span>
                                                        </li>
                                                    </ul>
                                                </td>
                                                <td class="text-truncate">$8500</td>
                                            </tr>
                                            <tr>
                                                <td class="text-truncate">iPhone X</td>
                                                <td class="text-truncate">
                                                    <ul class="list-unstyled order-list">
                                                        <li class="avatar avatar-sm">
                                                            <img class="rounded-circle" src="/assets/images/user/user1.jpg"
                                                                alt="user">
                                                        </li>
                                                        <li class="avatar avatar-sm">
                                                            <img class="rounded-circle" src="/assets/images/user/user2.jpg"
                                                                alt="user">
                                                        </li>
                                                        <li class="avatar avatar-sm">
                                                            <img class="rounded-circle" src="/assets/images/user/user3.jpg"
                                                                alt="user">
                                                        </li>
                                                        <li class="avatar avatar-sm">
                                                            <span class="badge">+4</span>
                                                        </li>
                                                    </ul>
                                                </td>
                                                <td class="text-truncate">$8999</td>
                                            </tr>
                                            <tr>
                                                <td class="text-truncate">iPhone X</td>
                                                <td class="text-truncate">
                                                    <ul class="list-unstyled order-list">
                                                        <li class="avatar avatar-sm">
                                                            <img class="rounded-circle" src="/assets/images/user/user1.jpg"
                                                                alt="user">
                                                        </li>
                                                        <li class="avatar avatar-sm">
                                                            <img class="rounded-circle" src="/assets/images/user/user2.jpg"
                                                                alt="user">
                                                        </li>
                                                        <li class="avatar avatar-sm">
                                                            <img class="rounded-circle" src="/assets/images/user/user3.jpg"
                                                                alt="user">
                                                        </li>
                                                        <li class="avatar avatar-sm">
                                                            <span class="badge">+4</span>
                                                        </li>
                                                    </ul>
                                                </td>
                                                <td class="text-truncate">$8999</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
                <!-- #END# Task Info -->
                <!-- Browser Usage -->
            
                <!-- #END# Browser Usage -->
            </div>
          

    </section>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="foter2" runat="server">



    <!-- Knob Js -->
    <script src="/assets/js/pages/todo/todo.js"></script> 

<%--    <script src="../js/Overview.js"></script>--%>
    <!-- Echart Js -->
                <script src="/assets/js/app.min.js"></script>
    <script src="/assets/js/chart.min.js"></script>
    <!-- Echart Js -->
        <script src="/assets/js/pages/sparkline/sparkline-data.js"></script>
    <script src="../js/Overview.js"></script>
<%--        <script src="/assets/js/pages/charts/chartjs.js"></script>--%>
     <script type = "text / javascript" src = "/js/expenschart.js"></script>
    <!-- Custom Js -->
    <script src="/assets/js/admin.js"></script>

    \assets\js\pages\sparkline
    <!-- Chartjs Js -->

    <!-- Custom Js -->


</asp:Content>
