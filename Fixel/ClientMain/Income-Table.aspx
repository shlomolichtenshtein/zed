﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ClientMain/manager.Master" AutoEventWireup="true" CodeBehind="Income-Table.aspx.cs" Inherits="Fixel.ClientMain.Income_Table" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
                <meta charset="UTF-8"/>
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title>Lorax - Bootstrap 4 Admin Dashboard Template</title>
    <!-- Favicon-->
    <link rel="icon" href="/assets/images/favicon.ico" type="image/x-icon"/>
    <!-- Plugins Core Css -->
    <link href="/assets/css/app.min.css" rel="stylesheet"/>
    <link href="/assets/js/bundles/materialize-rtl/materialize-rtl.min.css" rel="stylesheet"/>
    <!-- Custom Css -->
    <link href="/assets/css/style.css" rel="stylesheet"/>
    <!-- You can choose a theme from css/styles instead of get all themes -->
    <link href="/assets/css/styles/all-themes.css" rel="stylesheet" />
    <link href="/assets/css/form.min.css" rel="stylesheet"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="foter" runat="server">
            <body class="light rtl">
            <asp:Literal ID="ltluesr" runat="server" />
            <section class="content">
        <div class="container-fluid">
                        <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <strong>הכנסות</strong> </h2>
                            <div class="row">
                                 <div class="col-md-4">
                                                        <button class="dt-button buttons-print" tabindex="0" aria-controls="tableExport" data-toggle="modal" data-target="#gridModal" type="button"><span>חדש</span></button>
                                     </div>
                                 <div class="col-md-4">
                                  <asp:DropDownList ID="DropDownList1" AutoPostBack="true" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" runat="server">
                                    <asp:ListItem Value="30"> 30 יום אחרונים</asp:ListItem>
                                    <asp:ListItem Value="3">3 ימי עסקים אחרונים</asp:ListItem>
                                   <asp:ListItem Value="92">3 חודשים </asp:ListItem>
                                    <asp:ListItem Value="184"> חצי שנה</asp:ListItem>
                                     <asp:ListItem Value="368" > שנה </asp:ListItem>
                                <asp:ListItem Value="0"> תאריך מדויק</asp:ListItem>
                            </asp:DropDownList>
                                </div>
                                 <div class="col-md-4">
                            <div id="adae1" runat="server" visible="false">
                                                      <div class="input-group">
                                                               <span class="input-group-addon">
                                                                <i class="material-icons">date_range</i>
                                                            </span>
                                                            <div class="form-line">
                                                                	<asp:TextBox ID="dateA" runat="server" class="datepicker form-control" Visible="true" type="text" name="pass" placeholder="מתאריך "></asp:TextBox>
                                                             </div>
                                                         </div>
                                </div>
                                                        <div  id="adae2" runat="server" visible="false">
                                                      <div class="input-group">
                                                               <span class="input-group-addon">
                                                                <i class="material-icons">date_range</i>
                                                            </span>
                                                            <div class="form-line">
                                                                	<asp:TextBox ID="dateB" runat="server" class="datepicker form-control" Visible="true" type="text" name="pass" placeholder="עד תאריך "></asp:TextBox>
                                                             </div>
                                                         </div>
                                </div>
                         
                                <asp:Button ID="performance" class="dt-button buttons-print" OnClick="period" runat="server" Text="הצג" />
                                </div>
                            </div>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="#" onClick="return false;" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                        aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="#" onClick="return false;">Action</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">Another action</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">Something else here</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table id="tableExport" class="display table table-hover table-checkable order-column m-t-20 width-per-100">
                                    <thead>
                                        <tr>
                                            <th>תאריך</th>
                                            <th>שם הכנסה</th>
                                            <th>סכום</th>
                                            <th>תאריך שיכנס</th>
                                            <th>סיווג</th>
                                            <th>תת סיווג</th>
                                            <th>עריכה </th>
                                            <th>מחיקה</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <asp:Repeater ID="RptProd" runat="server">
                                       <ItemTemplate>
                                        <tr>
                                            <td><%#Eval("Date") %></td>
                                            <td><%#Eval("IncomeName") %></td>
                                            <td><%#Eval("Amount") %></td>
                                            <td><%#Eval("DatePR") %></td>
                                            <td><%#Eval("ClassificationsName") %></td>
                                            <td><%#Eval("SubclassificationsName") %></td>
                                            <td>
                                                 <button type="button" id="m" class="btn tblActnBtn" data-toggle="modal" data-target="#gridModal" onclick="UpdateIncome(<%#Eval("IncomeID") %>, <%#Eval("CustomerID")%>)" >
                                                
                                                    <i class="material-icons">mode_edit</i>
                                                </button>
                                            </td>
                                            <td>
                                                <button type="button" id="de" class="btn tblActnBtn" data-type="confirm" onclick="deledIncome(<%#Eval("IncomeID") %>)" >
                                                    <i class="material-icons">delete</i>
                                                </button>
                                            </td>
                                        </tr>
                                                                                 </ItemTemplate>
                                     </asp:Repeater>
                                                                            </tbody>
                                    <tfoot>

                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
                                                <div class="modal fade" id="gridModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalGrid"
                                        aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalGrid">הכנסה</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="container-fluid">
                    <div class="row clearfix">
                        <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="card">
                                        <div class="header">
                                            <h2>
                                                <strong>חדש</strong> עדכון</h2>
                                            <ul class="header-dropdown m-r--5">
                                                <li class="dropdown">
                                                    <a href="#" onClick="return false;" class="dropdown-toggle" data-toggle="dropdown"
                                                        role="button" aria-haspopup="true" aria-expanded="false">
                                                        <i class="material-icons">more_vert</i>
                                                    </a>
                                                    <ul class="dropdown-menu pull-right">
                                                        <li>
                                                            <a href="#" onClick="return false;">Action</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" onClick="return false;">Another action</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" onClick="return false;">Something else here</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="body">
                                            <div class="demo-masked-input">
                                                <div class="row clearfix">
                                                   

                                                    

                                                   <input type="hidden" name="IncomeID" id="IncomeID" />
                                                    <div class="col-md-6">
                                                        <b>תאריך</b>
                                                        <div class="input-group">
                                                               <span class="input-group-addon">
                                                                <i class="material-icons">date_range</i>
                                                            </span>
                                                            <div class="form-line">
                                                            	<input id="date"  class="datepicker form-control" type="text" name="date" placeholder="תאריך הכנסה"/>

                                                            </div>
                                                        </div>
                                                    </div>
                                                        <div class="col-md-6">
                                                         <b>שם הכנסה</b>
                                                        <div class="input-group">
                                                          <span class="input-group-addo">
                                                              </span>
                                                            <div class="form-line">
                                                    		<input id="name"" class="form-control" type="text" name="name" placeholder="סיכום יומי"/>
                                                            
                                                           </div>
                                                       </div>
                                                  </div>
                                                        
                                                    
                                                      <div class="col-md-6">
                                                  <b>סיווג הכנסה</b>
                                                          <asp:DropDownList ID="classific" runat="server" DataTextField="ClassificationsName" DataValueField="ClassificationsID" CssClass="form-group default-select">
                                                              <asp:ListItem></asp:ListItem>
                                                          </asp:DropDownList>
                                                     

                                                         </div>
                                                   
                                                     <div class="col-md-6">
                                                  <b>תת סיווג הכנסה</b>
                                                          <asp:DropDownList ID="Subcla" runat="server" DataTextField="SubclassificationsName" DataValueField="SubclassificationsID" CssClass="form-group default-select">
                                                              <asp:ListItem></asp:ListItem>
                                                          </asp:DropDownList>
                                                  </div>          
                                                    <div class="col-md-4">
                                                        <b>מזומן Z</b>                                                    
                                                             <input id="amuntZ"  class="form-control select2" type="select" name="amuntZ" placeholder="0"/>
                                                            </div>
                                                        <div class="col-md-4">
                                                        <b>אמריקן אקספרס</b>
                                                             <input id="AmericanExpress"  class="form-control select2" type="select" name="AmericanExpress" placeholder="0"/>
                                                            </div>
                                                        <div class="col-md-4">
                                                        <b>לאומי קארד</b>
                                                             <input id="LeumiCard"  class="form-control select2" type="select" name="LeumiCard" placeholder="0"/>
                                                            </div>
                                                        <div class="col-md-4">
                                                        <b>ויזה</b>                                                        
                                                             <input id="visa"  class="form-control select2" type="select" name="visa" placeholder="0"/>
                                                            </div>
                                                    <div class="col-md-4">
                                                        <b>ישראכרט</b>
                                                             <input id="Isracard"  class="form-control select2" type="select" name="Isracard" placeholder="0" />                                                         
                                                            </div>
                                                    <div class="col-md-4">
                                                        <b>דיירנס</b>
                                                             <input id="Diners"  class="form-control select2" type="select" name="Diners" placeholder="0"/>
                                                            </div>
                                                    <div class="col-md-5">
                                                        <b>אשראי/כללי/אחר</b>                                                        
                                                             <input id="credit"  class="form-control select2" type="select" name="credit" placeholder="0"/>                                                          
                                                            </div>
                                                    <div class="col-md-3">
                                                        <b>יורופיי</b>                                                       
                                                             <input id="European"  class="form-control select2" type="select" name="European" placeholder="0"/>                                                          
                                                            </div>
                                                    <div class="col-md-4">
                                                        <b>אמינית</b>                                                        
                                                             <input id="Reliable"  class="form-control select2" type="select" name="Reliable" placeholder="0"/>                                                           
                                                            </div>
                                                    <div class="col-md-4">
                                                        <b>כאל</b>                                                        
                                                             <input id="Calc"  class="form-control select2" type="select" name="Calc" placeholder="0" />                                                          
                                                            </div>                                                       
                                                    <div class="col-md-4">
                                                        <b>שיק</b>                                                       
                                                             <input id="Check"  class="form-control select2" type="select" name="Check" placeholder="0" />                            
                                                        <div class="form-line">
                                                            <input id="DueDate" class="datepicker form-control" Visible="true" type="text" name="DueDate" placeholder="תאריך פרעון שיק"/>
                                                        </div>
                                                     </div>
                                                    <div class="col-md-4">
                                                        <b>העברה</b>                                                       
                                                             <input id="transference"  class="form-control select2" type="select" name="transference" placeholder="0"/>                                                          
                                                                <div id="dd" visible="false" >
                                                              <input id="BanksName" Visible="true"   Text="" type="text"  name="BanksName"  placeholder="שם בנק"  />
                                                                 </div>
                                                            </div>
                                                       <div class="col-md-4">
                                                        <b>פדיון יומי</b>                                                        
                                                             <input id="DailyRedemption" class="form-control select2" type="select" name="text" placeholder="0" />                                                          
                                                            </div>

                                                </div>
                                                        </div>
                                                  </div>
                                </div>
                                        </div>
                            <button type="button"  class="btn-hover color-8" id="shlch" onclick="AddIncome()" >עדכון הכנסה</button>                                         
                    </div>
                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                                    </div>
                        <script>
                            function UpdateIncome(id, id2) {
                                var Prod = 0;
                                var Classif;
                                var v;
                                //$("#gridModal").modal("show");

                                //$('#gridModal').on('shown.bs.modal', function () {
                                $.ajax({
                                    type: "GET",
                                    url: "/api/Income/" + id + "/" + id2,

                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    success: function (data) {

                                        Income = JSON.parse(data);
                                        $("#IncomeID").val(Income[0].IncomeID);
                                        $("#date").val(Income[0].Date);
                                        $("#name").val(Income[0].IncomeName);
                                        var objSelect = document.getElementById("foter_classific");
                                        setSelectedValue(objSelect, Income[0].ClassificationsName);
                                        if (Income[0].IncomeName == "מזומן זד יומי") {
                                            $("#amuntZ").val(Income[0].Amount);
                                        }
                                        else if (Income[0].IncomeName == "תשלום בהעברה") {
                                            $("#transference").val(Income[0].Amount);
                                            $("#BanksName").val(Income[0].BanksName);
                                        }
                                        else if (Income[0].IncomeName == "תשלום בשיק") {
                                            $("#Check").val(Income[0].Amount);
                                            $("#DueDate").val(Income[0].DatePR);
                                        }
                                        else if (Income[0].IncomeName == "תשלום באשראי") {
                                            $("#credit").val(Income[0].Amount);
                                        }
                                        else
                                        {
                                            $("#DailyRedemption").val(Income[0].Amount);
                                        }
                                    },
                                    failure: function (errMsg) {
                                        alert(errMsg);
                                    }
                                });
                            }
                            function deledIncome(id) {
                                var Answer = confirm("בטוח בכך שברצונך למחוק?");

                                if (Answer === false)
                                    return false;
                                $.ajax({
                                    type: "Delete",
                                    url: "/api/Income/" + id,

                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",

                                    failure: function (errMsg) {
                                        alert(errMsg);
                                    }

                                });
                                location.reload();
                            }
                            function AddIncome() {
                                var Iid = $("#IncomeID").val();



                                var AddI = {
                                    IncomeName: $("#IncomeName").val(), Date: $("#date").val(), amuntZ: parseInt($("#amuntZ").val()), AmericanExpress: parseInt($("#AmericanExpress").val()),
                                    LeumiCard: parseInt($("#LeumiCard").val()), visa: parseInt($("#visa").val()), Isracard: parseInt($("#Isracard").val()), Diners: parseInt($("#Diners").val()),
                                    credit: parseInt($("#credit").val()), European: parseInt($("#European").val()), Reliable: parseInt($("#Reliable").val()), Calc: parseInt($("#Calc").val()),
                                    Check: parseInt($("#Check").val()), DatePR: $("#DueDate").val(), transference: parseInt($("#transference").val()), BanksName: $("#BanksName").val(),
                                    DailyRedemption: parseInt($("#DailyRedemption").val()), CustomerID: us,
                                    ClassificationsID: parseInt($("#foter_classific option:selected").val()),
                                    SubclassificationsID: parseInt($("#foter_Subcla option:selected").val())
                                };
                                if ($("#IncomeID").val()) {
                                    $.ajax({
                                        type: "Put",
                                        url: "/api/Income/" + Iid,
                                        data: JSON.stringify(AddI),
                                        contentType: "application/json; charset=utf-8",
                                        dataType: "json",
                                        success: function (data) {
                                            Income = JSON.parse(data);
                                            location.reload();
                                        }
                                    })
                                }
                                else {
                                    $.ajax({
                                        type: "Post",
                                        url: "/api/Income",
                                        data: JSON.stringify(AddI),
                                        contentType: "application/json; charset=utf-8",
                                        dataType: "json",
                                        success: function (data) {
                                            Income = JSON.parse(data);
                                            location.reload();
                                        }
                                    })
                                }

                            }
                            function setSelectedValue(selectObj, valueToSet) {
                                for (var i = 0; i < selectObj.options.length; i++) {
                                    if (selectObj.options[i].text == valueToSet) {
                                        selectObj.options[i].selected = true;
                                        return;
                                    }
                                }
                            }
                            </script>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="foter2" runat="server">
              <script src="/assets/js/app.min.js"></script>
    <script src="/assets/js/table.min.js"></script>
    <!-- Custom Js -->
    <script src="/assets/js/admin.js"></script>
    <script src="/assets/js/bundles/export-tables/dataTables.buttons.min.js"></script>
    <script src="/assets/js/bundles/export-tables/buttons.flash.min.js"></script>
    <script src="/assets/js/bundles/export-tables/jszip.min.js"></script>
    <script src="/assets/js/bundles/export-tables/pdfmake.min.js"></script>
    <script src="/assets/js/bundles/export-tables/vfs_fonts.js"></script>
    <script src="/assets/js/bundles/export-tables/buttons.html5.min.js"></script>
    <script src="/assets/js/bundles/export-tables/buttons.print.min.js"></script>
    <script src="/assets/js/pages/tables/jquery-datatable.js"></script>
        <script src="/assets/js/form.min.js"></script>
    <script src="/assets/js/bundles//multiselect/js/jquery.multi-select.js"></script>
    <script src="/assets/js/bundles/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js"></script>
    <script src="/assets/js/pages/forms/advanced-form-elements.js"></script>
        <script src="/assets/js/pages/forms/basic-form-elements.js"></script>
</asp:Content>
