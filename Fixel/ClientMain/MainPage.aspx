﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MainPage.aspx.cs" Inherits="Fixel.ClientMain.MainPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" class="logo">
<head runat="server">
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<title>Lorax - Bootstrap 4 Admin Dashboard Template</title>
	<!-- Favicon-->
	<link rel="icon" href="../../assets/images/favicon.ico" type="image/x-icon">
	<!-- Plugins Core Css -->
	<link href="../../assets/css/app.min.css" rel="stylesheet">
    <link href="../../assets/js/bundles/materialize-rtl/materialize-rtl.min.css" rel="stylesheet">
	<!-- Custom Css -->
	<link href="../../assets/css/style.css" rel="stylesheet" />
	<link href="../../assets/css/pages/extra_pages.css" rel="stylesheet" />



    <!-- Custom Css -->
     <script   src="https://code.jquery.com/jquery-3.4.1.js"   integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="  crossorigin="anonymous"></script>
    <!-- You can choose a theme from css/styles instead of get all themes -->
    <link href="/assets/css/styles/all-themes.css" rel="stylesheet" />

    <link href="/assets/css/form.min.css" rel="stylesheet"/>
    <!-- Custom Css -->
    <!-- You can choose a theme from css/styles instead of get all themes -->
    <link href="/assets/css/styles/all-themes.css" rel="stylesheet" />
    <link href="../css/index.css" rel="stylesheet" />
      <link href="/assets/js/bundles/multiselect/css/multi-select.css" rel="stylesheet"/>

    <!-- Colorpicker Css -->
    <link href="../../assets/js/bundles/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="../../assets/css/style.css" rel="stylesheet">
    <!-- You can choose a theme from css/styles instead of get all themes -->
    <link href="../../assets/css/styles/all-themes.css" rel="stylesheet" />
</head>

<body class="logo rtl">
     
	<div class="limiter">
		<div>
            	<form runat="server" >
			  <section >
        <div class="container-fluid">
            <div class="block-header padding-125">
          <div class="row clearfix">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div class="header">
                        <h2>
                            <strong>דף</strong> ראשי</h2>
                    </div>
                    <div class="body" >
                      
                        <div class="demo">
                                <div class="row">
                                    <div class="col-md-3 col-sm-6">
                                        <div class="pricingTable">
                                            <div class="pricingTable-header">
                                                <i class="material-icons">account_box</i>
                                                <div class="price-value">לקוחות
                                                </div>
                                            </div>
                                            <h3 class="heading">חשבון לקוחות</h3>
                                            <div class="pricingTable-signup">

                                                 <a href="#"  id="m" data-toggle="modal" data-target="#gridModal"  >כניסה</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="pricingTable greenColor">
                                            <div class="pricingTable-header">
                                                <i class="material-icons">local_mall</i>
                                                <div class="price-value">לקוח חדש
                                                </div>
                                            </div>
                                            <h3 class="heading">יצירת חשבון</h3>

                                            <div class="pricingTable-signup">
                                                 <a href="AddC.aspx">צור</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="pricingTable blueColor">
                                            <div class="pricingTable-header">
                                                <i class="material-icons">spa</i>
                                                <div class="price-value"> סיווגים
                                                </div>
                                            </div>
                                            <h3 class="heading">צפיה עדכון הוספה</h3>
                                            <div class="pricingTable-signup">
                                               <a  href="Classification.aspx">היכנס</a>  
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="pricingTable redColor">
                                            <div class="pricingTable-header">
                                                <i class="material-icons">filter_vintage</i>
                                                <div class="price-value"> נקודת איזון
                                                </div>
                                            </div>
                                            <h3 class="heading">בדיקה ראשונית של העסק</h3>
                                            <div class="pricingTable-signup">
                                <a href="BalancePoint.aspx">בדוק</a>
                                            </div>
                                        </div>
                                    </div>                                   
                                     <div class="col-md-3 col-sm-6"></div>
                                     <div class="col-md-3 col-sm-6"></div>
                                     <div class="col-md-3 col-sm-6"></div>
                                </div>
                        </div>
                        </div>
                    </div>

            </div>
        </div>
                </div>
            
                                           
                  
          
         </section>
                      <div class="modal fade" id="gridModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalGrid"
                                        aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalGrid">התחברות לקוחות</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="container-fluid">
                    <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="card">

                                        <div class="body">
                                            <div class="demo-masked-input">
                                                <div class="row clearfix">
                                                   
                                                    <input type="hidden" name="ExpenseID" id="ExpenseID" />
                                                    <div class="col-md-6">
                                                                                                      <div class="container-fluid">
                                                        <div class="collapse navbar-collapse" id="navbar-collapse">
                                                            <ul class="nav navbar-nav navbar-right">
                                                                <li class="select-label">
                                                                    <asp:DropDownList ID="Custom" runat="server"   DataTextField="CustomerName" DataValueField="CustomerID" CssClass="form-group default-select" >
                                                                      <asp:ListItem></asp:ListItem>
                                                                  </asp:DropDownList>
                                                                </li>
                                                            </ul>
                                                         </div>
                                                        </div>
                                                  </div>
                                                    <div class="col-md-6">
                                                         <asp:Button ID="performance" class="dt-button buttons-print" OnClick="performance_Click" runat="server" Text="התחבר" />
                                                    </div>
                                                    </div>
                                                </div></div>
                                        </div>
                                    </div>
                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                                 </div>
                    </form>
            		</div>
	</div>

					<!-- Plugins Js -->
	<script src="../../assets/js/app.min.js"></script>
	<!-- Extra page Js -->
	<script src="../../assets/js/pages/examples/pages.js"></script>

    <script src="/assets/js/table.min.js"></script>
    <!-- Custom Js -->
    <script src="/assets/js/admin.js"></script>
    <script src="/assets/js/bundles/export-tables/buttons.flash.min.js"></script>
    <script src="/assets/js/bundles/export-tables/jszip.min.js"></script>
    <script src="/assets/js/bundles/export-tables/pdfmake.min.js"></script>
    <script src="/assets/js/bundles/export-tables/vfs_fonts.js"></script>
    <script src="/assets/js/bundles/export-tables/buttons.html5.min.js"></script>
    <script src="/assets/js/bundles/export-tables/buttons.print.min.js"></script>      
    <script src="/assets/js/form.min.js"></script>
    <script src="/assets/js/bundles//multiselect/js/jquery.multi-select.js"></script>
    <script src="/assets/js/bundles/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js"></script>
    <!-- Custom Js -->
        <script src="/assets/js/bundles//multiselect/js/jquery.multi-select.js"></script>
    <script src="/assets/js/pages/forms/advanced-form-elements.js"></script>
        <script src="/assets/js/pages/forms/basic-form-elements.js"></script>
    <script src="/assets/js/pages/forms/form-wizard.js"></script>


</body>
</html>
