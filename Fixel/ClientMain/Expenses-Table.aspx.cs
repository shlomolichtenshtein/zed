﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Fixel.BLL;

namespace Fixel.ClientMain
{
    public partial class Expenses_Table : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["User"] == null)
                {
                    Response.Redirect("login.aspx");
                }
  


                int us = (int)Session["CustomerID"];


                ltluesr.Text = "<script >var us=" + us + "; </script>";
                //By default, it incurs last 30 days
                List<Expense> ExpenseG = Expense.GetAl(us, 30);

                int i;

                string html = "";
                for (i = 0; i < ExpenseG.Count; i++)
                {
                    html += "<h1>" + ExpenseG[i].Date + "</h1>";
                }
                RptProdtab.DataSource = ExpenseG;
                RptProdtab.DataBind();
                //Create a selection list
                List<Classifications> cla = Classifications.GetAll();
                classific.DataSource = cla;
                classific.DataBind();
                classific.DataTextField = "ClassificationsName";
                classific.DataValueField = "ClassificationsID";
                classific.DataBind();
                classific.Items.Insert(0, new ListItem("סיווג הוצאה", " 0"));
                //Create a selection list
                List<Subclassifications> sub = Subclassifications.GetAll();
                Subcla.DataSource = sub;
                Subcla.DataBind();
                Subcla.DataTextField = "SubclassificationsName";
                Subcla.DataValueField = "SubclassificationsID";
                Subcla.DataBind();
                Subcla.SelectedIndex = 0;

            }
        }

        protected void shlch_Click(object sender, EventArgs e)
        {

        }

        protected void shlch_Click1(object sender, EventArgs e)
        {

        }
        protected void period(object sender, EventArgs e)//When the user selects a different period for spending
        {
            string g = DropDownList1.SelectedValue;
            int j = int.Parse(g);
            user u = (user)Session["User"];
            Customer cli = (Customer)Session["Customer"];
            int us = cli.UId;
            string cn = cli.UserName;
            if (DropDownList1.SelectedValue == "0")
            {
                string dat1 = Utils.GlobFuncs.dateadd(dateA.Text);
                string dat2 = Utils.GlobFuncs.dateadd(dateB.Text);
                List<Expense> ExpenseG = Expense.GetAlld(us, dat1, dat2);
                int i;
                string html = "";
                for (i = 0; i < ExpenseG.Count; i++)
                {
                    html += "<h1>" + ExpenseG[i].Date + "</h1>";
                }
                RptProdtab.DataSource = ExpenseG;
                RptProdtab.DataBind();

            }
            else
            {
                List<Expense> ExpenseG = Expense.GetAl(us, j);
                int i;
                string html = "";
                for (i = 0; i < ExpenseG.Count; i++)
                {
                    html += "<h1>" + ExpenseG[i].Date + "</h1>";
                }
                RptProdtab.DataSource = ExpenseG;
                RptProdtab.DataBind();
            }


        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            adae1.Visible = adae2.Visible = DropDownList1.SelectedValue == "0";
        }
    }
}