﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ClientMain/manager.Master" AutoEventWireup="true" CodeBehind="Settings.aspx.cs" Inherits="Fixel.ClientMain.Settings" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title>Lorax - Bootstrap 4 Admin Dashboard Template</title>
    <!-- Favicon-->
    <link rel="icon" href="../../assets/images/favicon.ico" type="image/x-icon">
    <!-- Plugins Core Css -->
    <link href="../../assets/css/app.min.css" rel="stylesheet">
    <link href="../../assets/js/bundles/materialize-rtl/materialize-rtl.min.css" rel="stylesheet">
    <!-- Custom Css -->
    <link href="../../assets/css/style.css" rel="stylesheet">
    <!-- You can choose a theme from css/styles instead of get all themes -->
    <link href="../../assets/css/styles/all-themes.css" rel="stylesheet" />
    <link href="../css/Settings.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="foter" runat="server">
   <body class="light rtl">
           <section class="content">
               <asp:Literal ID="ltluesr" runat="server" />
        <div class="container-fluid">
                        <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                                                <div class="body">
                            <div class="row clearfix">
                                                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                        <li role="presentation">
                                            <a href="#home_animation_2" data-toggle="tab" class="active show">פרטי העסק</a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#profile_animation_2" data-toggle="tab">חשבונות בנק</a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#messages_animation_2" data-toggle="tab">כרטיסי אשראי</a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#settings_animation_2" data-toggle="tab">SETTINGS</a>
                                        </li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane animated fadeInRight active show" id="home_animation_2">
                                            <table id="mainTable" class="table">
                                                <tbody>
                                                    <tr>
                                                        <td class="gray">שם</td>
                                                        <td><input id="name"   type="text" name="name" readonly="true"  class="border-0 margin-0" margin:0 border-bottom:0 value="שלמה"  /></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="gray">ת.ז./ח.פ</td>
                                                        <td>330</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="gray">דואר אלקטרוני</td>
                                                        <td>430</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="gray">פלפון</td>
                                                        <td>100</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div role="tabpanel" class="tab-pane animated fadeInRight" id="profile_animation_2">
                                            <b>חשבונות בנק</b>
                                            <p>
                                                Lorem ipsum dolor sit amet, ut duo atqui exerci dicunt, ius impedit
                                                mediocritatem an. Pri ut tation electram moderatius.
                                                Per te suavitate democritum. Duis nemore probatus ne quo, ad liber
                                                essent
                                                aliquid pro. Et eos nusquam accumsan, vide mentitum fabellas ne est, eu
                                                munere
                                                gubergren sadipscing mel.
                                            </p>
                                        </div>
                                        <div role="tabpanel" class="tab-pane animated fadeInRight" id="messages_animation_2">
                                            <b>כרטיסי אשראי</b>
                                            <p>
                                                Lorem ipsum dolor sit amet, ut duo atqui exerci dicunt, ius impedit
                                                mediocritatem an. Pri ut tation electram moderatius.
                                                Per te suavitate democritum. Duis nemore probatus ne quo, ad liber
                                                essent
                                                aliquid pro. Et eos nusquam accumsan, vide mentitum fabellas ne est, eu
                                                munere
                                                gubergren sadipscing mel.
                                            </p>
                                        </div>
                                        <div role="tabpanel" class="tab-pane animated fadeInRight" id="settings_animation_2">
                                            <b>Settings Content</b>
                                            <p>
                                                Lorem ipsum dolor sit amet, ut duo atqui exerci dicunt, ius impedit
                                                mediocritatem an. Pri ut tation electram moderatius.
                                                Per te suavitate democritum. Duis nemore probatus ne quo, ad liber
                                                essent
                                                aliquid pro. Et eos nusquam accumsan, vide mentitum fabellas ne est, eu
                                                munere
                                                gubergren sadipscing mel.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>

            </script>
        </div>
    </section>
</body>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="foter2" runat="server">
    <script src="../../assets/js/app.min.js"></script>
    <!-- Custom Js -->
    <script src="../../assets/js/admin.js"></script>
</asp:Content>
