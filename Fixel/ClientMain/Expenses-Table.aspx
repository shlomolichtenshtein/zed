﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ClientMain/manager.Master" AutoEventWireup="true" CodeBehind="Expenses-Table.aspx.cs" Inherits="Fixel.ClientMain.Expenses_Table" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
          <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title>Lorax - Bootstrap 4 Admin Dashboard Template</title>
    <!-- Favicon-->
    <link rel="icon" href="/assets/images/favicon.ico" type="image/x-icon"/>
    <!-- Plugins Core Css -->
    <link href="/assets/css/app.min.css" rel="stylesheet"/>
    <link href="/assets/js/bundles/materialize-rtl/materialize-rtl.min.css" rel="stylesheet"/>
    <!-- Custom Css -->
     <script   src="https://code.jquery.com/jquery-3.4.1.js"   integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="  crossorigin="anonymous"></script>

    <link href="/assets/css/style.css" rel="stylesheet"/>
    <!-- You can choose a theme from css/styles instead of get all themes -->
    <link href="/assets/css/styles/all-themes.css" rel="stylesheet" />


    <link href="/assets/js/bundles/materialize-rtl/materialize-rtl.min.css" rel="stylesheet"/>
    <link href="/assets/css/form.min.css" rel="stylesheet"/>
    <!-- Custom Css -->
    <link href="/assets/css/style.css" rel="stylesheet"/>
    <!-- You can choose a theme from css/styles instead of get all themes -->
    <link href="/assets/css/styles/all-themes.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="foter" runat="server">
       <body class="light rtl">
        <asp:Literal ID="ltluesr" runat="server" />
            <section class="content">
        <div class="container-fluid">
                        <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <strong>הוצאות</strong> </h2>
                            <div class="row">
                            <div class="col-md-4">
                            <button class="dt-button buttons-print" tabindex="0" aria-controls="tableExport" data-toggle="modal" data-target="#gridModal" type="button"><span>חדש</span></button>
                                </div>
                            <div class="col-md-4">
                            <asp:DropDownList ID="DropDownList1" AutoPostBack="true" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" runat="server">
                                <asp:ListItem Value="30"> 30 יום אחרונים</asp:ListItem>
                                <asp:ListItem Value="3">3 ימי עסקים אחרונים</asp:ListItem>
                                <asp:ListItem Value="92">3 חודשים </asp:ListItem>
                                <asp:ListItem Value="184"> חצי שנה</asp:ListItem>
                                <asp:ListItem Value="368" > שנה </asp:ListItem>
                                <asp:ListItem Value="0"> תאריך מדויק</asp:ListItem>
                            </asp:DropDownList>
                                </div>
                                <div class="col-md-4">
                            <div  id="adae1" runat="server" visible="false">
                                                      <div class="input-group">
                                                               <span class="input-group-addon">
                                                                <i class="material-icons">date_range</i>
                                                            </span>
                                                            <div class="form-line">
                                                                	<asp:TextBox ID="dateA" runat="server" class="datepicker form-control" Visible="true" type="text" name="pass" placeholder="מתאריך "></asp:TextBox>
                                                             </div>
                                                         </div>
                                </div>
                                                        <div id="adae2" runat="server" visible="false">
                                                      <div class="input-group">
                                                               <span class="input-group-addon">
                                                                <i class="material-icons">date_range</i>
                                                            </span>
                                                            <div class="form-line">
                                                                	<asp:TextBox ID="dateB" runat="server" class="datepicker form-control" Visible="true" type="text" name="pass" placeholder="עד תאריך "></asp:TextBox>
                                                             </div>
                                                         </div>
                                </div>
                                <asp:Button ID="performance" class="dt-button buttons-print" OnClick="period" runat="server" Text="הצג" />
                                </div>
                            
                                </div>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="#" onClick="return false;" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                        aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="#" onClick="return false;">Action</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">Another action</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">Something else here</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table id="tableExport" class="display table table-hover table-checkable order-column m-t-20 width-per-100">
                                    <thead>
                                        <tr>
                                            <th>תאריך</th>
                                            <th>שם ההוצאה</th>
                                            <th>סכום</th>
                                            <th>מקור ההוצאה</th>
                                            <th>סיווג</th>
                                            <th>תת סיווג</th>
                                            <th>קבוע/משתנה</th>
                                            <th>תקופה</th>
                                            <th>מספר תשלומים</th>
                                            <th>עריכה </th>
                                            <th>מחיקה</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <asp:Repeater ID="RptProdtab" runat="server">
                                       <ItemTemplate>
                                        <tr>
                                            <td><%#Eval("Date") %></td>
                                            <td><%#Eval("ExpenseName") %></td>
                                            <td><%#Eval("Amount") %></td>
                                            <td><%#Eval("BanksName") %></td>
                                            <td><%#Eval("ClassificationsName") %></td>
                                            <td><%#Eval("SubclassificationsName") %></td>
                                            <td><%#Eval("ConstantVariable") %></td>
                                            <td><%#Eval("Period") %></td>
                                            <td><%#Eval("Payments") %></td>
                                            <td>
                                                
                                                <button type="button" id="m" class="btn tblActnBtn" data-toggle="modal" data-target="#gridModal" onclick="UpdateExpnse(<%#Eval("ExpenseID") %>, <%#Eval("CustomerID")%>)" >
                                               
                                                    <i class="material-icons">mode_edit</i>
                                                </button>
                                           
                                            </td>
                                            <td>

                                                <button type="button" id="de" class="btn tblActnBtn" data-type="confirm" onclick="deledExpnse(<%#Eval("ExpenseID") %>)" >


                                                    <i class="material-icons">delete</i>
                                                </a>
                                            </td>
                                        </tr>
                                                                                 </ItemTemplate>
                                     </asp:Repeater>
                                                                            </tbody>
                                    <tfoot>

                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
                                    <div class="modal fade" id="gridModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalGrid"
                                        aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalGrid">Modal title</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="container-fluid">
                    <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="card">
                                        <div class="header">
                                            <h2>
                                                <strong>Masked</strong> Input</h2>
                                            <ul class="header-dropdown m-r--5">
                                                <li class="dropdown">
                                                    <a href="#" onClick="return false;" class="dropdown-toggle" data-toggle="dropdown"
                                                        role="button" aria-haspopup="true" aria-expanded="false">
                                                        <i class="material-icons">more_vert</i>
                                                    </a>
                                                    <ul class="dropdown-menu pull-right">
                                                        <li>
                                                            <a href="#" onClick="return false;">Action</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" onClick="return false;">Another action</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" onClick="return false;">Something else here</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="body">
                                            <div class="demo-masked-input">
                                                <div class="row clearfix">
                                                   
                                                    <input type="hidden" name="ExpenseID" id="ExpenseID" />
                                                    <div class="col-md-6">
                                                         <b>שם הוצאה</b>
                                                        <div class="input-group">
                                                          <span class="input-group-addo">
                                                              </span>
                                                            <div class="form-line">
                                                    		<input id="ExpenseName"  class="form-control" type="text" name="ExpenseName" placeholder="שם הוצאה" />
                                                            
                                                           </div>
                                                       </div>
                                                  </div>
                                                    <div class="col-md-6">
                                                        <b>תאריך</b>
                                                        <div class="input-group">
                                                               <span class="input-group-addon">
                                                                <i class="material-icons">date_range</i>
                                                            </span>
                                                            <div class="form-line">
                                                            	<input id="Date"  class="datepicker form-control" type="text" name="Date" placeholder="תאריך הוצאה" />

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <b>תאריך לדו"ח</b>
                                                        <div class="input-group">
                                                               <span class="input-group-addon">
                                                                <i class="material-icons">date_range</i>
                                                            </span>
                                                            <div class="form-line">
                                                                	<input id="ActualDate"  class="datepicker form-control" type="text" name="ActualDate" placeholder="תאריך לדוח"/>
                                 
                                                            </div>
                                                        </div>
                                                    </div>                                                   
                                                      <div class="col-md-6">                                                                                                                      
                                                          <asp:DropDownList ID="classific" runat="server" DataTextField="ClassificationsName" DataValueField="ClassificationsID"  />                                                                                                                                                                                                                            
                                                          </div>
                                                     <div class="col-md-6">                                                
                                                          <asp:DropDownList ID="Subcla" runat="server" DataTextField="SubclassificationsName" DataValueField="SubclassificationsID" CssClass="form-group default-select">
                                                              <asp:ListItem></asp:ListItem>
                                                          </asp:DropDownList>
                                                  </div>          
                                                    <div class="col-md-6">
                                                        <b>סכום הוצאה</b>                                                       
                                                             <input id="Amount"  class="form-control select2" type="select" name="Amount" placeholder="0" />
                                                           
                                                            </div>                                                                                                                                                                                                                  
                                                     <div class="col-md-6">                                                                                 
                                                            <label>
                                                                <input id="Banks" name="group2"   type="radio" />
                                                            <span>העברה</span>
                                                           </label>
                                                             <div id="dd" visible="false" >
                                                              <input id="BanksName"  type="text" name="BanksName"    placeholder="שם בנק"  />
                                                                </div>
                                                       </div>
                                                       <div class="col-md-6">                                                   
                                                            <label>
                                                            <input id="car" name="group2"   type="radio" />
                                                            <span>אשראי</span>
                                                           </label>
                                                                <input id="FourNumber"  type="text" name="FourNumber" placeholder="4 ספרות אחרונות "  />

                                                       </div>
                                                             <div class="col-md-6">
                                                             <label>
                                                            <input id="Checkbooks" name="group2"   type="radio" />
                                                            <span>שיק</span>
                                                           </label>                                                                                                                                  
                                                        <div class="input-group">
                                                               <span class="input-group-addon">
                                                                <i class="material-icons">date_range</i>
                                                            </span>
                                                            <div class="form-line">
                                                                	<input id="DueDate" class="datepicker form-control"  type="text" name="DueDate" placeholder="תאריך פרעון שיק" />
                                                             </div>
                                                         </div>
                                                               
                                                                     
                                                        <b>מס' שיק</b>
                                                        <div class="input-group">
                                                          <span class="input-group-addon">
                                                                <i class="material-icons">computer</i>
                                                            </span>
                                                            <div class="form-line">
                                                             <input id="CheckbooksNumber" class="form-control select2"  type="text" name="CheckbooksNumber" placeholder="מספר שיק" />
                                                           
                                                                 </div>
                                                        </div>
                                                 </div>
                                                          <div class="col-md-6">
                                                             <label>
                                                            <input id="Cash" name="group2"   type="radio" />
                                                            <span>מזומן</span>
                                                           </label>         
                                                  
                                            
                                                       
                                                    

                                                              
                                                            
                                                        </div>
                                                  
                                                    <div class="col-md-6">
                                            
                                                        <div class="form-group">
                                                          
                                                     <h2 class="card-inside-title">קבועה/משתנה</h2>
                                                    <div class="demo-switch">
                                                        <div class="switch">
                                                         <label>OFF
                                                             
                                                        <input id="ConstantVariable" type="checkbox" />
                                                          <span class="lever"></span>ON</label>
                                                     </div>
                                                          </div>
                                              </div>
                                            

                                </div>
                                <div class="col-md-6">
                                     <div class="row clearfix">
                                          <h2 class="card-inside-title">תקופה בחודשים</h2>
                                    <div class="input-group spinner" data-trigger="spinner">
                                        <div class="form-line">
                                            <input type="text" id="Period" class="form-control text-center" value="1" data-rule="quantity" />                                              
                                        </div>
                                        <span class="input-group-addon">
                                            <a href="javascript:;" class="spin-up" data-spin="up">
                                                <i class="material-icons">keyboard_arrow_up</i>
                                            </a>
                                            <a href="javascript:;" class="spin-down" data-spin="down">
                                                <i class="material-icons">keyboard_arrow_down</i>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                                                   </div>
                                 <div class="col-md-6">
                                     <div class="row clearfix">
                                          <h2 class="card-inside-title">מספר תשלומים</h2>
                                    <div class="input-group spinner" data-trigger="spinner">
                                        <div class="form-line">
                                           <input type="text" id="Payments" class="form-control text-center" value="1" data-rule="quantity" />                             
                                        </div>
                                        <span class="input-group-addon">
                                            <a href="javascript:;" class="spin-up" data-spin="up">
                                                <i class="material-icons">keyboard_arrow_up</i>
                                            </a>
                                            <a href="javascript:;" class="spin-down" data-spin="down">
                                                <i class="material-icons">keyboard_arrow_down</i>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                                                   </div>                   
                                                    <div class="col-md-6">
                                        <div class="form-group">
                                                          
                                                     <h2 class="card-inside-title">תשלום לספק</h2>
                                                    <div class="demo-switch">
                                                        <div class="switch">
                                                         <label>OFF
                                                             
                                                        <input id="VendorsID" type="checkbox" onchange="Vend()" />
                                                          <span class="lever"></span>ON</label>
                                                             <asp:Literal ID="fals" runat="server"  ></asp:Literal>
                                                     </div>
                                                          </div>
                                              </div>
                                            

                                </div>
                                                </div>
                                                        </div>
                                                  </div>
                          </div>
                                                </div>

                                         <button type="button"  class="btn-hover color-8" id="shlch" onclick="AddExpnse()" >עדכון הוצאה</button>


                                                                      


                                            </div>
                                        </div>
                                    </div>
    </div></div>
        </div>   
                                               
            <script>
                function UpdateExpnse(id, id2) {
                    var Prod = 0;
                    var Classif;
                    var v;
                        $.ajax({
                            type: "GET",
                            url: "/api/Expense/" + id + "/" + id2,

                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {

                                Expense = JSON.parse(data);
                                var a = Expense[0].ExpenseID;
                                $("#ExpenseID").val(Expense[0].ExpenseID);
                                $("#ExpenseName").val(Expense[0].ExpenseName);
                                $("#Date").val(Expense[0].Date);
                                $("#ActualDate").val(Expense[0].ActualDate);
                                $("#Amount").val(Expense[0].Amount);
                                $("#DueDate").val(Expense[0].DueDate);
                                $("#SubclassificationsName").val(Expense[0].SubclassificationsName);
                                $("#CheckbooksNumber").val(Expense[0].CheckbooksNumber);
                                $("#FourNumber").val(Expense[0].FourNumber);
                                $("#BanksName").val(Expense[0].BanksName);
                                $("#CustomerID").val(Expense[0].CustomerID);
                                $("#Period").val(Expense[0].Period);
                                $("#Payments").val(Expense[0].Payments);
                                if (Expense[0].ConstantVariable) {
                                    $("#ConstantVariable").prop("checked", true);
                                }

                                if (Expense[0].VendorsID) {
                                    $("#VendorsID").prop("checked", true);
                                }

                                if (Expense[0].Cash) {
                                    $("#Cash").prop("checked", true);
                                }

                                if (Expense[0].CheckbooksNumber != "") {
                                    $("#Checkbooks").prop("checked", true);
                                }

                                if (Expense[0].FourNumber != "") {
                                    $("#car").prop("checked", true);
                                }

                                if (Expense[0].BanksName != "") {
                                    $("#Banks").prop("checked", true);
                                }
                                v = Expense[0].ClassificationsID;
                                v.toString();                               
                                var objSelect = document.getElementById("foter_classific");
                                setSelectedValue(objSelect, Expense[0].ClassificationsName);     
                            },
                            failure: function (errMsg) {
                                alert(errMsg);
                            }
                        });               
                }

               

                function setSelectedValue(selectObj, valueToSet) {
                    for (var i = 0; i < selectObj.options.length; i++) {
                        if (selectObj.options[i].text == valueToSet) {
                            selectObj.options[i].selected = true;
                            return;
                        }
                    }
                }
                function deledExpnse(id) {
                    var Answer = confirm("בטוח בכך שברצונך למחוק?");

                    if (Answer === false)
                        return false;
                    $.ajax({
                        type: "Delete",
                        url: "/api/Expense/"+id ,

                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        
                         failure: function (errMsg) {
                            alert(errMsg);
                         }
                         
                    }); 
                    location.reload();
                }
                function AddExpnse() {
                   
                    var CoVa = ($("#ConstantVariable").prop("checked")) ? 1 : 0;
                    var Ven = ($("#VendorsID").prop("checked")) ? 1 : 0;
                    var Cash = ($("#Cash").prop("checked")) ? 1 : 0;
                    var Eid = $("#ExpenseID").val();
                    
                    
                  
                    var AddE = {
                        ExpenseName: $("#ExpenseName").val(), Date: $("#Date").val(), ActualDate: $("#ActualDate").val(),
                        Amount: parseInt($("#Amount").val()), DueDate: $("#DueDate").val(), CheckbooksNumber: $("#CheckbooksNumber").val(), FourNumber: $("#FourNumber").val(), 
                        BanksName: $("#BanksName").val(), CustomerID: us, Period: parseInt($("#Period").val()), Payments: parseInt($("#Payments").val()),
                        ConstantVariable: CoVa, VendorsID: Vid, Cash: Cash, ClassificationsID: parseInt($("#foter_classific option:selected").val()),
                        SubclassificationsID: parseInt($("#foter_Subcla option:selected").val())
                    };
                    if ($("#ExpenseID").val()) {
                        $.ajax({
                            type: "Put", 
                            url: "/api/Expense/"+Eid,
                            data: JSON.stringify(AddE),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                Expense = JSON.parse(data);
                                location.reload();
                            }
                        })
                    }
                    else {
                        $.ajax({
                            type: "Post",
                            url: "/api/Expense",
                            data: JSON.stringify(AddE),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                Expense = JSON.parse(data);
                                location.reload();
                            }
                        })
                    }

                }
                function Vend() {
                    if (($("#VendorsID").prop("checked")) != true) { return false;}
                    $.ajax({
                        type: "GET",
                        url: "/api/Vendors/" + us,

                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (dataV) {
                            Vendors = JSON.parse(dataV);
                            V(Vendors)
                        }

                    })

                } 
                var Vid;
                function Vendo() {
                    var AddV = { VendorsName: $("#ExpenseName").val(), CustomerID: us}
                    $.ajax({
                        type: "Post",
                        url: "/api/Vendors",
                        data: JSON.stringify(AddV),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (da) {
                            Vid = JSON.stringify(da);
                          
                        }
                    })
                }
                
                function V(Vendors) {
                    for (var i = 0; i < Vendors.length; i++) {
                        if (Vendors[i].VendorsName == $("#ExpenseName").val()) {
                            Vid = Vendors[i].VendorsID;
                            break;
                        }
                    }
                    if (i == Vendors.length) {
                        var Answer = confirm("אין ספק בשם" + " " + $("#ExpenseName").val() + " " + "האם להוסיף ספק זה?");

                        if (Answer === false) {
                            $("#VendorsID").prop("checked", false);
                            return false;
                        }
                        Vendo();
                        //$("#Vn").text = "אין ספק בשם" + " " + $("#ExpenseName").val();
                        //$("#myModal").modal()
                    }
                }
                $("#CheckbooksNumber").on('blur', function () {
                    var number = $("#CheckbooksNumber").val();
                    $.ajax({
                        type: "GET",
                        url: "/api/Checkbooks/" + number + "/" + us,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {

                            Checkbooks = JSON.stringify(data);
                            if (Checkbooks ==1){
                                var Answer = confirm("כבר השתמשת עם השיק הזה נא הזן מספר אחר");
                            }
                            else if(Checkbooks==2)
                                var Answer = confirm("אין מספר שיק כזה במערכת");
                            else
                                return false;
                        }
                    
                    })

                    

                });
                $("#FourNumber").on('blur', function () {
                    var number = $("#FourNumber").val();
                    $.ajax({
                        type: "GET",
                        url: "/api/PasswordsCR/" + number + "/" + us,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {

                            FourNumber = JSON.stringify(data);
                            if (FourNumber == "true") {
                                var Answer = confirm("מס' אשראי לא תקין?");
                            }
                            else
                                return false;
                        }

                    })



                });

               
            </script>
        </div>
    </section>

        </body>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="foter2" runat="server">
                <script src="/assets/js/app.min.js"></script>
    <script src="/assets/js/table.min.js"></script>
    <!-- Custom Js -->
    <script src="/assets/js/admin.js"></script>
    <script src="/assets/js/bundles/export-tables/dataTables.buttons.min.js"></script>
    <script src="/assets/js/bundles/export-tables/buttons.flash.min.js"></script>
    <script src="/assets/js/bundles/export-tables/jszip.min.js"></script>
    <script src="/assets/js/bundles/export-tables/pdfmake.min.js"></script>
    <script src="/assets/js/bundles/export-tables/vfs_fonts.js"></script>
    <script src="/assets/js/bundles/export-tables/buttons.html5.min.js"></script>
    <script src="/assets/js/bundles/export-tables/buttons.print.min.js"></script>
    <script src="/assets/js/pages/tables/jquery-datatable.js"></script>       
    <script src="/assets/js/form.min.js"></script>
    <script src="/assets/js/bundles//multiselect/js/jquery.multi-select.js"></script>
    <script src="/assets/js/bundles/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js"></script>
    <!-- Custom Js -->
    <script src="/assets/js/pages/forms/advanced-form-elements.js"></script>
        <script src="/assets/js/pages/forms/basic-form-elements.js"></script>
</asp:Content>
