﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ClientMain/manager.Master" AutoEventWireup="true" CodeBehind="VendorReportCus.aspx.cs" Inherits="Fixel.ClientMain.VendorReportCus" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="foter" runat="server">
      <body class="light rtl">
        <asp:Literal ID="ltluesr" runat="server" />
            <section class="content">
        <div class="container-fluid">
                        <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                         <div class="col-md-4">
                            <h2>
                                <strong>דווח ספקים</strong> </h2>
                         </div>
 
                            </div>
                        </div>

                    </div>
                              <div class="col-lg-6">
                    <div class="card">
                        <div class="header">

                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="#" onClick="return false;" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                        aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    
                                </li>
                            </ul>
                        </div>
                                                <div class="body">
                            <div class="recent-report__chart" id="c1">
                                <canvas id="VendRep"></canvas>
                            </div>
                        </div>
                        
                    </div>
                </div>
                            
                               <div class="col-lg-6" id="p">
                    <div class="card">
                        <div class="header">

                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="#" onClick="return false;" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                        aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    
                                </li>
                            </ul>
                        </div>
                                              
                        <div class="body">
                            <div class="recent-report__chart" id="c">
                                <canvas id="singel-bar-chart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                                        
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6" id="o">
                    <div class="card">
     
                        <div class="body">
                            <div class="col-md-4 text-center"> 

                                </div>
                            <div class="row clearfix">
                                <div class="col-md-5">
                                    <b>סך כל הקניות לשנה אחרונה</b>
                                </div>
                                <div class="col-md-4">
                                    <input id="SumI" name="SumI" readonly="true"  type="text" />
                                </div>


                                 <div class="col-md-5">
                                    <b>סך רווח במידה והיה הנחה של 5%</b>
                                </div>
                                <div class="col-md-4">
                                    <input id="ProductCost" readonly="true" name="ProductCost"    type="text" />
                                </div>


                                 <div class="col-md-5">
                                    <b>סך רווח במידה והיה הנחה של 7%</b>
                                </div>
                                <div class="col-md-4">
                                    <input id="MarketingCost" readonly="true"  type="text" />
                                </div>

                                 <div class="col-md-5">
                                    <b>סך רווח במידה והיה הנחה של 10%</b>
                                </div>
                                <div class="col-md-4">
                                    <input id="WearRepetition" readonly="true"  type="text" />
                                </div>


                                </div>
                        </div>
                    </div>
                </div>
                                </div>
          
                         

              
           

            </div>
                </section>
                 </body>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="foter2" runat="server">
                    <script src="/assets/js/app.min.js"></script>
    <script src="/assets/js/table.min.js"></script>
    <!-- Custom Js -->
    <script src="/assets/js/admin.js"></script>
    <script src="/assets/js/bundles/export-tables/dataTables.buttons.min.js"></script>
    <script src="/assets/js/bundles/export-tables/buttons.flash.min.js"></script>
    <script src="/assets/js/bundles/export-tables/jszip.min.js"></script>
    <script src="/assets/js/bundles/export-tables/pdfmake.min.js"></script>
    <script src="/assets/js/bundles/export-tables/vfs_fonts.js"></script>
    <script src="/assets/js/bundles/export-tables/buttons.html5.min.js"></script>
    <script src="/assets/js/bundles/export-tables/buttons.print.min.js"></script>
    <script src="/assets/js/pages/tables/jquery-datatable.js"></script>       
    <script src="/assets/js/form.min.js"></script>
    <script src="/assets/js/bundles//multiselect/js/jquery.multi-select.js"></script>
    <script src="/assets/js/bundles/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js"></script>
    <!-- Custom Js -->    <script src="/assets/js/chart.min.js"></script>
        <script src="/assets/js/bundles/echart/echarts.js"></script>
    <script src="/assets/js/pages/forms/advanced-form-elements.js"></script>
        <script src="/assets/js/pages/forms/basic-form-elements.js"></script>
         <script type = "text / javascript" src = "/js/expenschart.js"></script>
        <script src="/js/VendorReportJS.js"></script>
        <script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>  
</asp:Content>
