﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ClientMain/manager.Master" AutoEventWireup="true" CodeBehind="BalancePoint.aspx.cs" Inherits="Fixel.ClientMain.BalancePoint" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
         <script   src="https://code.jquery.com/jquery-3.4.1.js"   integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="  crossorigin="anonymous"></script>
        <link rel="icon" href="/assets/images/favicon.ico" type="image/x-icon">
    <!-- Plugins Core Css -->
    <link href="/assets/css/app.min.css" rel="stylesheet">
    <link href="/assets/js/bundles/materialize-rtl/materialize-rtl.min.css" rel="stylesheet">
    <!-- Custom Css -->
    <link href="/assets/css/style.css" rel="stylesheet" />
    <!-- You can choose a theme from css/styles instead of get all themes -->
    <link href="/assets/css/styles/all-themes.css" rel="stylesheet" />
    <link href="../css/Balance.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="foter" runat="server">
        <div class="light rtl">
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-xl-6 col-lg-6 col-md-8 col-sm-8">
                    <div class="card">
                        <div class="Bp">
                            <h2>סימולציה לנקודת איזון</h2>

                        </div>
                        <div class="body">
                            <div class="col-md-4 text-center"> 
                            <input id="Balance" class="btn bg-indigo waves-effect" type="button" value="נקודת איזון" onclick="BalancePoint()" />
                                </div>
                            <div class="row clearfix">
                                <div class="col-md-5">
                                    <b>סך כל המכירות</b>
                                </div>
                                <div class="col-md-4">
                                    <input id="SumI" name="SumI" placeholder="הזן את סך המכירות" type="text" />
                                </div>
                                <div class="col-md-3">
                                    <b>
                                        <input id="SumIA" value="100%"  readonly="true" type="text" />
                                    </b>
                                </div>

                                 <div class="col-md-5">
                                    <b>סך הכל עלות המוצרים עם העובדים</b>
                                </div>
                                <div class="col-md-4">
                                    <input id="ProductCost"  name="ProductCost"  placeholder="הזן סכום"   type="text" />
                                </div>

                                <div class="col-md-3">
                                    <b>
                                         <input id="ProductCostIn"  placeholder="הזן סכום ב%"  type="text" />
                                    </b>
                                 </div>
                                 <div class="col-md-5">
                                    <b>עלות שיווק</b>
                                </div>
                                <div class="col-md-4">
                                    <input id="MarketingCost" placeholder="הזן סכום"  type="text" />
                                </div>
                                <div class="col-md-3">
                                    <b>
                                         <input id="MarketingCostAIn"  placeholder=" הזן סכום ב%"  type="text" />
                                    </b>
                                 </div>
                                 <div class="col-md-5">
                                    <b>בלאי וחזרות</b>
                                </div>
                                <div class="col-md-4">
                                    <input id="WearRepetition"  placeholder="הזן סכום"  type="text" />
                                </div>
                                <div class="col-md-3">
                                    <b>
                                         <input id="WearRepetitionAIn"  placeholder="הזן סכום ב%" type="text" />
                                    </b>
                                 </div>
                                 <div class="col-md-5">
                                    <b>רווח גולמי</b>
                                </div>
                                <div class="col-md-4">
                                    <input id="GrossProfit"  readonly="true" type="text" />
                                </div>
                                <div class="col-md-3">
                                    <b>
                                        <input id="GrossProfitA"  readonly="true" type="text" />
                                    </b>
                                </div>
                                <div class="message my-message" >
                                            <p>על כל שקל הוצאה צריך למכור ב</p>
                                    <p id="ex" ></p>
                                            <div class="row">
                                            </div>
                                        </div>
                                <div class="Bal">
                                <div class="col-md-12" id="de">
                                <fieldset id="buildyourform">

</fieldset>
                                    <div class="row">
                                <div class="col-md-5 " id="field1">
                                    <input type="text" class="fieldname"  placeholder="הזן שם הוצאה" id="inpu1"/>
                                </div>
                             <div class="col-md-4">
                                    <input id="in0" class="form-control" value="0"  placeholder="הזן סכום"  type="text" />
                                </div>
                                <div class="col-md-3">
                                    <b>
                                         <input id="inp0" class="form" value="0" placeholder="הזן סכום ב%" type="text" />
                                    </b>
                                 </div>
                                        </div>
                                       </div>                         
                                
                                   
                                 <div class="col-md-12">
                                     <button type="button" id="add" class="btn bg-brown btn-circle waves-effect waves-circle waves-float">
                                    <i class="material-icons">add</i>
                                </button>
                                     </div>
                                      </div>
                                 <div class="row Ba">
                                                                     <div class="col-md-5">
                                    <b>הוצאות תפעול</b>
                                </div>
                             <div class="col-md-4">
                                    <input id="su"  readonly="true"    type="text" />
                                </div>
                                <div class="col-md-3">
                                    <b>
                                         <input id="sumta"  readonly="true"    type="text" />
                                    </b>
                                 </div>
                                        </div>
                                <div class="row">
                                                                     <div class="col-md-5">
                                    <b>רווח תפעולי</b>
                                </div>

                             <div class="col-md-4">
                                    <input id="sua"  readonly="true"    type="text" />
                                </div>
                                <div class="col-md-3">
                                    <b>
                                         <input id="sumtaa"  readonly="true"    type="text" />
                                    </b>
                                 </div>
                                        </div>
                                  <div class="Bal">
                                                                <div class="col-md-12" id="ma">
                                <fieldset id="managementform">

</fieldset>
                                    <div class="row">
                                <div class="col-md-5 " id="mana0">
                                    <input type="text" class="fieldname"  placeholder="הזן שם הוצאה" id="man0"/>
                                </div>
                             <div class="col-md-4">
                                    <input id="ma0" class="form-line" value="0"  placeholder="הזן סכום"  type="text" />
                                </div>
                                <div class="col-md-3">
                                    <b>
                                         <input id="m0" class="form-group" value="0" placeholder="הזן סכום ב%" type="text" />
                                    </b>
                                 </div>
                                        </div>
                                       </div>                         
                                
                                   
                                 <div class="col-md-12">
                                <button type="button" id="add2" class="btn bg-brown btn-circle waves-effect waves-circle waves-float">
                                    <i class="material-icons">add</i>
                                </button>
                                     </div>
                                      </div>
                                 <div class="row">
                                                                                                          <div class="col-md-5 Ba"  id="management">
                                    <b>הוצאות הנהלה וכלליות</b>
                                </div>

                             <div class="col-md-4">
                                    <input id="su2"  readonly="true"    type="text" />
                                </div>
                                <div class="col-md-3">
                                    <b>
                                         <input id="sumta2"  readonly="true"    type="text" />
                                    </b>
                                 </div>
                                        </div>
                                <div class="row BaPoi">
                                                                                                         <div class="col-md-5"  id="manage">
                                    <b>רווח נקי</b>
                                </div>

                             <div class="col-md-4">
                                    <input id="sua2"  readonly="true"    type="text" />
                                </div>
                                <div class="col-md-3">
                                    <b>
                                         <input id="sumtaa2"  readonly="true"    type="text" />
                                    </b>
                                 </div>
                                        </div>
                                <div class="row Bal">
                                                                 <div class="col-md-5">
                                    <b>משכורת מנכל</b>
                                </div>
                                <div class="col-md-4">
                                    <input id="CeoSalary"  placeholder="הזן סכום"  type="text" />
                                </div>
                                <div class="col-md-3">
                                    <b>
                                         <input id="CeoSalaryAIn"  placeholder="הזן סכום ב%" type="text" />
                                    </b>
                                 </div>
                                    </div>
                                 <div class="col-md-5">
                                    <b>הון שנשאר בעסק</b>
                                </div>
                                <div class="col-md-4">
                                    <input id="CapitalBusiness"  readonly="true" type="text" />
                                </div>
                                <div class="col-md-3">
                                    <b>
                                        <input id="CapitalBusinessA"  readonly="true" type="text" />
                                    </b>
                                </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                var s = 0;
                var sum1 = 0;
                var sum2 = 0;
                var sum3 = 0;
                var sum5 = 0;
                //Calling a function after entering a sum in a control
                $("#ProductCostIn").on('blur', function () {
                    BaPo();
                });
                $("#MarketingCostAIn").on('blur', function () {
                    BaPo();
                });
                $("#WearRepetitionAIn").on('blur', function () {
                    BaPo();
                });
                $("#SumI").on('blur', function () {
                    BaPo();
                });
                $("#CeoSalaryAIn").on('blur', function () {
                    BaPo();
                });
                var a4; 
                //Calculation function when entering percentages
                function BaPo() {
                    var a = SumI.value * ProductCostIn.value / 100;//עלות מוצרים
                    var a2 = SumI.value * MarketingCostAIn.value / 100;//עלות שיווק
                    var a3 = SumI.value * WearRepetitionAIn.value / 100;//בלאי וחזרות
                     a4 = SumI.value * CeoSalaryAIn.value / 100;
                     s = SumI.value - a - a2 - a3;
                    var sA = s * 100 / SumI.value;
                    $("#ProductCost").val(a.toFixed(0));//עלות מוצרים
                    $("#MarketingCost").val(a2.toFixed(0));//עלות שיווק
                    $("#WearRepetition").val(a3.toFixed(0));//בלאי וחזרות
                    $("#CeoSalary").val(a4.toFixed(0));
                    $("#GrossProfit").val(s.toFixed(0));
                    $("#GrossProfitA").val(sA.toFixed(0) + "%");
                    sus();
                   
                }
                //Calling a function after entering a sum in a control
                $("#ProductCost").on('blur', function () {
                    BP();
                });
                $("#MarketingCost").on('blur', function () {
                    BP();
                });
                $("#WearRepetition").on('blur', function () {
                    BP();
                });
                $("#CeoSalary").on('blur', function () {
                    BP();
                });
                //Calculation function when entering amount
                function BP() {
                    var a = ProductCost.value * 100 / SumI.value;
                    var a2 = MarketingCost.value * 100 / SumI.value;
                    var a3 = WearRepetition.value * 100 / SumI.value;
                    var a6 = CeoSalary.value * 100 / SumI.value;
                    s = SumI.value - ProductCost.value - MarketingCost.value - WearRepetition.value;
                    var sA = s * 100 / SumI.value;
                    $("#ProductCostIn").val(a.toFixed(0));
                    $("#MarketingCostAIn").val(a2.toFixed(0));
                    $("#WearRepetitionAIn").val(a3.toFixed(0));
                    $("#CeoSalaryAIn").val(a6.toFixed(0));
                    $("#GrossProfit").val(s.toFixed(0));
                    $("#GrossProfitA").val(sA.toFixed(0) + "%");
                    sus();
                   
                }
                var intId = 0;
                //Dynamically add rows with access to each new row by giving unique id
                $("#add").click(function () {  
                    intId++;
                    var fieldWrapper = $("<div class=\"fieldwrapper row\" id=\"field" + intId + "\"/>");
                    fieldWrapper.data("idx", intId);
                    var fName = $("<div class=\"col-md-5\"><input type=\"text\"  id=\"inpu" + intId + " " + "\"placeholder=\"הזן שם הוצאה\"/> </div>");
                    var fName2 = $(" <div class=\"col-md-4\"> <input value=\"0\" class=\"form-control\" id=\"in" + intId + " " + "\"placeholder=\"הזן סכום\" type=\"text\"/></div>");
                    var fName3 = $("<div class=\"col-md-3\"> <b> <input value=\"0\" class=\"form\" id=\"inp" + intId + " " + "\"placeholder=\"הזן סכום ב%\" type=\"text\" /></b></div></div>");
                    fieldWrapper.append(fName);
                    fieldWrapper.append(fName2);
                    fieldWrapper.append(fName3);
                    $("#buildyourform").append(fieldWrapper);

                });
              //Turn to dynamically generated lines
                $("#de").on("blur", ".form-control", function () {

                        var sumt=0;
                        var inputs = $(".form-control"); 
                        var inputs2 = $(".form");
                        for (var i = 0; i < inputs.length; i++) {
                            for (var j = 0; j < inputs2.length; j++) {
                                $(inputs2[i]).val(($(inputs[i]).val()) * 100 / SumI.value);

                            }
                            sumt += parseInt( ($(inputs[i]).val()));
                            let st = sumt * 100 / SumI.value;
                            $("#su").val(sumt.toFixed(0));
                            $("#sumta").val(st.toFixed(0));
                        }
                    sum1 = sumt;
                    sus();
                });
                //Turn to dynamically generated lines
                $("#de").on("blur", ".form", function () {
                    var sumt = 0;
                    var inputs = $(".form-control");
                    var inputs2 = $(".form");
                    for (var i = 0; i < inputs.length; i++) {
                        for (var j = 0; j < inputs2.length; j++) {
                            $(inputs[i]).val(SumI.value * ($(inputs2[i]).val()) / 100 );

                        }
                        sumt += parseInt(($(inputs[i]).val()));
                        let st = sumt * 100 / SumI.value;
                        $("#su").val(sumt.toFixed(0));
                        $("#sumta").val(st.toFixed(0));
                    }
                    sum1 = sumt;
                    sus();
                });
                //Incremental function
                function sus() {
                    sum2 = s - sum1;
                    let st = sum2 * 100 / SumI.value;
                    $("#sua").val(sum2.toFixed(0));
                    $("#sumtaa").val(st.toFixed(0));
                    sum4 = sum2 - sum3;
                    sum5 = sum4 -  CeoSalary.value; 
                    var sB = sum5 * 100 / SumI.value;
                    let st2 = sum4 * 100 / SumI.value;
                    $("#sua2").val(sum4.toFixed(0));
                    $("#sumtaa2").val(st2.toFixed(0));
                    $("#CapitalBusinessA").val(sB.toFixed(0) + "%");
                    $("#CapitalBusiness").val(sum5.toFixed(0));
                    ex.valuetext = 1 / sumtaa.value;
                }


                var intId2 = 0;
                 //Dynamically add rows with access to each new row by giving unique id
                $("#add2").click(function () {
                    intId2++;
                    var fieldWrapper = $("<div class=\"fieldwrapper row\" id=\"field" + intId2 + "\"/>");
                    fieldWrapper.data("idx", intId2);
                    var fName = $("<div class=\"col-md-5\"><input type=\"text\"  id=\"inpu" + intId2 + " " + "\"placeholder=\"הזן שם הוצאה\"/> </div>");
                    var fName2 = $(" <div class=\"col-md-4\"> <input value=\"0\" class=\"form-line\" id=\"in" + intId2 + " " + "\"placeholder=\"הזן סכום\" type=\"text\"/></div>");
                    var fName3 = $("<div class=\"col-md-3\"> <b> <input value=\"0\" class=\"form-group\" id=\"inp" + intId2 + " " + "\"placeholder=\"הזן סכום ב%\" type=\"text\" /></b></div></div>");
                    fieldWrapper.append(fName);
                    fieldWrapper.append(fName2);
                    fieldWrapper.append(fName3);
                    $("#managementform").append(fieldWrapper);

                });

                $("#ma").on("blur", ".form-line", function () {

                    var sumt2 = 0;
                    var inputs2 = $(".form-line");
                    var inputs22 = $(".form-group");
                    for (var i = 0; i < inputs2.length; i++) {
                        for (var j = 0; j < inputs22.length; j++) {
                            $(inputs22[i]).val(($(inputs2[i]).val()) * 100 / SumI.value);

                        }
                        sumt2 += parseInt(($(inputs2[i]).val()));
                        let st2 = sumt2 * 100 / SumI.value;
                        $("#su2").val(sumt2.toFixed(0));
                        $("#sumta2").val(st2.toFixed(0));
                    }
                    sum3 = sumt2;
                    sus();
                });
                $("#ma").on("blur", ".form-group", function () {
                    var sumt2 = 0;
                    var inputs2 = $(".form-line");
                    var inputs22 = $(".form-group");
                    for (var i = 0; i < inputs2.length; i++) {
                        for (var j = 0; j < inputs22.length; j++) {
                            $(inputs2[i]).val(SumI.value * ($(inputs22[i]).val()) / 100);

                        }
                        sumt2 += parseInt(($(inputs2[i]).val()));
                        let st2 = sumt2 * 100 / SumI.value;
                        $("#su2").val(sumt2.toFixed(0));
                        $("#sumta2").val(st2.toFixed(0));
                    }
                    sum3 = sumt2;
                    sus();
                });
                //A function that gives a balance point means that after entering total sales and costs when created there is a minus in remaining
                //capital in the business we check how many will have to sell in order to not have a minus in remaining capital in the business
                function BalancePoint() {
                    if (CapitalBusiness.value < 0) {
                        var r = 0;
                        var m = 0;
                        var sy = 0;
                        var s2 = 0;
                        var sm = 0;
                        var sumy = 0;
                        var sa = 0;
                        var sumf = 0;
                        var mysum = 0;
                        var my = 0;
                        r = parseInt(GrossProfitA.value);//רוח גולמי באחוזים
                        m = 100 - r;//עלות מוצר באחוזים
                        sa=100/r
                        sy = parseInt(su.value);
                        s2 = parseInt(su2.value);
                        sm = parseInt(CeoSalary.value);
                        sumy = sy + s2 + sm;
                        my = m / 100;
                        sumf = sumy * my * sa;
                        mysum = sumf + sumy;
                        SumI.value = mysum.toFixed(0);
                        var a = SumI.value * ProductCostIn.value / 100;//עלות מוצרים
                        var a2 = SumI.value * MarketingCostAIn.value / 100;//עלות שיווק
                        var a3 = SumI.value * WearRepetitionAIn.value / 100;//בלאי וחזרות
                         s = SumI.value - a - a2 - a3;//רווח גולמי
                        var sA = s * 100 / SumI.value;//באחוזים רווח גולמי
                        $("#ProductCost").val(a.toFixed(0));//עלות מוצרים
                        $("#MarketingCost").val(a2.toFixed(0));//עלות שיווק
                        $("#WearRepetition").val(a3.toFixed(0));//בלאי וחזרות
                        $("#GrossProfit").val(s.toFixed(0));//רווח גולמי
                        $("#GrossProfitA").val(sA.toFixed(0) + "%");//רווח גולמי באחוזים
                        var sumt = 0;
                        var inputs = $(".form-control");
                        var inputs2 = $(".form");
                        for (var i = 0; i < inputs.length; i++) {
                            for (var j = 0; j < inputs2.length; j++) {
                                $(inputs2[i]).val((($(inputs[i]).val()) * 100 / SumI.value).toFixed(0));

                            }
                            sumt += parseInt(($(inputs[i]).val()));
                            let st = sumt * 100 / SumI.value;
                            $("#su").val(sumt.toFixed(0));
                            $("#sumta").val(st.toFixed(0));
                        }
                        sum1 = sumt;
                        var sumt2 = 0;
                        var inputs2 = $(".form-line");
                        var inputs22 = $(".form-group");
                        for (var i = 0; i < inputs2.length; i++) {
                            for (var j = 0; j < inputs22.length; j++) {
                                $(inputs22[i]).val((($(inputs2[i]).val()) * 100 / SumI.value).toFixed(0));

                            }
                            sumt2 += parseInt(($(inputs2[i]).val()));
                            let st2 = sumt2 * 100 / SumI.value;
                            $("#su2").val(sumt2.toFixed(0));
                            $("#sumta2").val(st2.toFixed(0));
                        }
                        sum3 = sumt2;
                        sus();
                        var scs = CeoSalary.value * 100 / SumI.value;
                        $("#CeoSalaryAIn").val(scs.toFixed(0));

                    }
                }
            </script>
        </div>
    </section>
</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="foter2" runat="server">
     <script src="/assets/js/pages/tables/jquery-datatable.js"></script>
        <script src="/assets/js/bundles//multiselect/js/jquery.multi-select.js"></script>
        <script src="/assets/js/pages/apps/chat.js"></script>
        <script src="/assets/js/app.min.js"></script>
    <!-- Custom Js -->
    <script src="/assets/js/admin.js"></script>
</asp:Content>
