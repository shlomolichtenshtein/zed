﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Fixel.BLL;

namespace Fixel.ClientMain
{
    public partial class AddC : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<Banks> cla = Banks.GetAll();
            classific.DataSource = cla;
            classific.DataBind();
            classific.DataTextField = "BanksName";
            classific.DataValueField = "BanksID";
            classific.DataBind();
            classific.Items.Insert(0, new ListItem("בנק", " 0"));

            List<Credit> cr = Credit.GetAll();
            cre.DataSource = cr;
            cre.DataBind();
            cre.DataTextField = "CreditName";
            cre.DataValueField = "CreditID";
            cre.DataBind();
            cre.Items.Insert(0, new ListItem("אשראי", " 0"));
        }
    }
}
