﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OleDb;
using System.Data.SqlClient;
using Fixel.BLL;

namespace Fixel.ClientMain
{
    public partial class ExcelData : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["User"] == null)
                {
                    Response.Redirect("login.aspx");
                }



                int us = (int)Session["CustomerID"];
            }

         }
        protected void BtnUpload_Click(object sender, EventArgs e)
        {
            Customer cli = (Customer)Session["Customer"];
            int us = cli.UId;
            string saveFolder = Server.MapPath("/uploads/xls");

            string filePath = Path.Combine(saveFolder, UploadExcel.FileName);

            UploadExcel.SaveAs(filePath);

            Expense.ExtractDataFromExcel(filePath,us);


        }
    }
}