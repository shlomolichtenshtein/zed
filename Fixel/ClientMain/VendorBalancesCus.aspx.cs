﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Fixel.BLL;

namespace Fixel.ClientMain
{
    public partial class VendorBalancesCus : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["User"] == null)
                {
                    Response.Redirect("login.aspx");
                }
                int us = (int)Session["CustomerID"];
                ltluesr.Text = "<script >var us=" + us + "; </script>";
                List<Vendors> ExpenseG = Vendors.GetAllBalance(us);

                int i;

                string html = "";
                for (i = 0; i < ExpenseG.Count; i++)
                {
                    html += "<h1>" + ExpenseG[i].Date + "</h1>";
                }
                RptProd.DataSource = ExpenseG;
                RptProd.DataBind();
            }
        }
    }
}