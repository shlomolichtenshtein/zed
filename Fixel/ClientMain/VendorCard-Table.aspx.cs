﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Fixel.BLL;

namespace Fixel.ClientMain
{
    public partial class VendorCard_Table : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["User"] == null)
                {
                    Response.Redirect("login.aspx");
                }
                int us = (int)Session["CustomerID"];
                ltluesr.Text = "<script >var us=" + us + "; </script>";

                List<Vendors> Ven = Vendors.GetAll(us);

                Vendor.DataSource = Ven;
                Vendor.DataBind();
                Vendor.DataTextField = "VendorsName";
                Vendor.DataValueField = "VendorsID";
                Vendor.DataBind();
                Vendor.Items.Insert(0, new ListItem("שם ספק", " 0"));
                Vendor.SelectedIndex = 0;
            }
        }
        protected void period(object sender, EventArgs e)
        {
            string g = Vendor.SelectedItem.Text;
            user u = (user)Session["User"];
            Customer cli = (Customer)Session["Customer"];
            int us = cli.UId;
            string cn = cli.UserName;
            List<Vendors> ExpenseG = Vendors.GetAllBiV(us, g);
            Vendors Tamp;
            Tamp = new Vendors();
            ExpenseG[0].Subtotal = ExpenseG[0].Amount - ExpenseG[0].payment;
            Tamp.CreditSummary = ExpenseG[0].payment;
            Tamp.RequiredSummary = ExpenseG[0].Amount;
            for (int j = 1; j < ExpenseG.Count; j++)
            {
                ExpenseG[j].Subtotal = ExpenseG[j - 1].Subtotal;
                ExpenseG[j].Subtotal += ExpenseG[j].Amount - ExpenseG[j].payment;
                Tamp.CreditSummary += ExpenseG[j].payment;
                Tamp.RequiredSummary += ExpenseG[j].Amount;
            }

            Tamp.id = 0;
            Tamp.VendorsName = ExpenseG[0].VendorsName + " " + "יתרה";
            Tamp.Date = "";
            Tamp.payment = Tamp.CreditSummary;
            Tamp.Amount = Tamp.RequiredSummary;
            Tamp.InvoicingNamber = 0;
            Tamp.source = "";
            Tamp.DueDate = "";
            Tamp.Subtotal = Tamp.RequiredSummary - Tamp.CreditSummary;

            ExpenseG.Add(Tamp);
            RptProd.DataSource = ExpenseG;
            RptProd.DataBind();
        }
    }
}