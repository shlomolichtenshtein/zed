﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Fixel.BLL;

namespace Fixel.ClientMain
{
    public partial class Classification : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["User"] == null)
                {
                    Response.Redirect("login.aspx");
                }



                

                List<Classifications> cla = Classifications.GetAll();


                int i;

                string html = "";
                for (i = 0; i < cla.Count; i++)
                {
                    html += "<h1>" + cla[i].ClassificationsID + "</h1>";
                }
                RptProd.DataSource = cla;
                RptProd.DataBind();
            }
        }
    }
}