﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Fixel.BLL;

namespace Fixel.ClientMain
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        if (!IsPostBack)
            {
                if (Session["User"] == null)
                {
                    Response.Redirect("login.aspx");
                }
                List<Customer> CustomerG = Customer.GetAll();
                Custom.DataSource = CustomerG;
                Custom.DataBind();
                Custom.DataTextField = "CustomerName";
                Custom.DataValueField = "CustomerID";
                Custom.DataBind();
                Custom.Items.Insert(0, new ListItem("שם לקוח", " 0"));
                Custom.SelectedIndex = 0;
            }

        }
        protected void performance_Click(object sender, EventArgs e)
        {
            Customer Tmp = new Customer(int.Parse(Custom.SelectedValue), Custom.SelectedItem.ToString(), "", "", "", "");
            Session["Customer"] = Tmp;
            Session["CustomerID"] = Tmp.UId;
            Session["CustomerName"] = Tmp.UserName;
            Response.Redirect("Expenses_Table.aspx");
        }
    }
}