﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddC.aspx.cs" Inherits="Fixel.ClientMain.AddC" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title>Lorax - Bootstrap 4 Admin Dashboard Template</title>
    <!-- Favicon-->
       <script   src="https://code.jquery.com/jquery-3.4.1.js"   integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="  crossorigin="anonymous"></script>
    <link rel="icon" href="../../assets/images/favicon.ico" type="image/x-icon">
    <!-- Plugins Core Css -->
    <link href="../../assets/css/app.min.css" rel="stylesheet">
    <link href="../../assets/js/bundles/materialize-rtl/materialize-rtl.min.css" rel="stylesheet">
    <!-- Custom Css -->
    <link href="../../assets/css/style.css" rel="stylesheet">
    <!-- You can choose a theme from css/styles instead of get all themes -->
    <link href="../../assets/css/styles/all-themes.css" rel="stylesheet" />
     <link href="/assets/js/bundles/multiselect/css/multi-select.css" rel="stylesheet">

        <link href="/assets/css/form.min.css" rel="stylesheet"/>


    <link href="/assets/js/bundles/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css" rel="stylesheet" />
</head>
<body class="light rtl">
    <section class="content">
        <div class="container-fluid">
  <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>לקוח חדש</h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="#" onClick="return false;" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                        aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="#" onClick="return false;">Action</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">Another action</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="return false;">Something else here</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <form id="wizard_with_validation" runat="server" method="POST">
                                <h3>פרטי העסק</h3>
                                <fieldset>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" id="CustomerName" class="form-control" name="username" placeholder="שם" required>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="password"  class="form-control" name="password" id="password" placeholder="סיסמא"
                                                required>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="password" class="form-control" name="confirm" placeholder="אימות סיסמא" required>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="email" id="Email" name="email" class="form-control" placeholder="דואר אלקטרוני" required>
                                        </div>
                                    </div>
                                                                        <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="number" id="Phone" name="number" class="form-control" placeholder="פלאפון" required>
                                        </div>
                                    </div>
                                                                        <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="number" id="IdentityCard" name="text" class="form-control" placeholder="ת.ז./ח.פ" required>
                                        </div>
                                    </div>
                                </fieldset>
                                <h3>חשבונות בנק</h3>
                                <fieldset>
                                      <div id="buildyourform">

</div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                                         <asp:DropDownList CssClass="test" ID="classific" placeholder="בנק" runat="server" DataTextField="BanksName" DataValueField="BanksID"  />    
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" id="un" name="surname" class="form-control" placeholder="שם משתמש" required/>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                             <input type="password" id="pa" name="surnam" class="form-control" placeholder="סיסמא" required/>
                                        </div>
                                    </div>  
                                     <button type="button" id="add" class="btn bg-cyan waves-effect">
                                    הוספת הבנק לחשבון
                                </button>            

                                </fieldset>
                                                                <h3>כרטיסי אשראי</h3>
                                <fieldset>
                                      <div id="buildyourform2">

</div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                                         <asp:DropDownList CssClass="test" ID="cre" placeholder="אשראי" runat="server" DataTextField="CreditName" DataValueField="CreditID"  />    
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" id="un2" name="surname" class="form-control" placeholder="שם משתמש" required/>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                             <input type="password" id="pa2" name="surnam" class="form-control" placeholder="סיסמא" required/>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                             <input type="number" id="num" name="surnam" class="form-control" placeholder="שש ספרות אחרונות" required/>
                                    </div>  
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                             <input type="number" id="day" name="surnam" class="form-control" placeholder="יום בחודש שיורד" required/>
                                        </div>
                                    </div>  
                                     <button type="button" id="add2" class="btn bg-cyan waves-effect">
                                    הוספת הכרטיס לחשבון
                                </button>            

                                </fieldset>
                                <h3>Terms &amp; Conditions - Finish</h3>
                                <fieldset>
                                 <div class="form-check m-l-10">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" id="acceptTerms-2" name="acceptTerms" required/> I agree with the Terms and Conditions.
                                        <span class="form-check-sign">
                                            <span class="check"></span>
                                        </span>
                                    </label>
                                </div>
                                </fieldset>
                                <script>
                                    var intId = 0;
                                    $("#add").click(function () {
                                        intId++;
                                        var fieldWrapper = $("<div class=\"fieldwrapper row\" id=\"field" + intId + "\"/>");
                                        fieldWrapper.data("idx", intId);
                                        var fName = $("<div class=\"col-md-5\"><input class=\"form-contro\"  type=\"text\" name=" + $("#classific option:selected").val() + " " + "  id=\"inpu" + intId + " " + "\"value=" + $("#classific option:selected").text() + " " + "\"/> </div>");
                                        var fName2 = $(" <div class=\"col-md-4\"> <input value=" + $("#un").val() + " " + "\" class=\"form-contr\" id=\"in" + intId + " " + "\" type=\"text\"/></div>");
                                        var fName3 = $("<div class=\"col-md-3\"> <b> <input value=" + $("#pa").val() + " " + "\" class=\"form-cont\" id=\"inp" + intId + " " + "\" type=\"password\" /></b></div></div>");
                                        fieldWrapper.append(fName);
                                        fieldWrapper.append(fName2);
                                        fieldWrapper.append(fName3);
                                        $("#buildyourform").append(fieldWrapper);

                                    });
                                    var intId2 = 0;
                                    $("#add2").click(function () {
                                        intId2++;
                                        var fieldWrapper = $("<div class=\"fieldwrapper row\" id=\"field" + intId2 + "\"/>");
                                        fieldWrapper.data("idx", intId2);
                                        var fName = $("<div class=\"col-md-3\"><input type=\"text\" class=\"form-con\" name=" + $("#cre option:selected").val() + " " + "  id=\"inpu2" + intId2 + " " + "\"value=" + $("#cre option:selected").text() + " " + "\"/> </div>");
                                        var fName2 = $(" <div class=\"col-md-3\"> <input value=" + $("#un2").val() + " " + "\" class=\"form-co\" id=\"in2" + intId2 + " " + "\" type=\"text\"/></div>");
                                        var fName3 = $("<div class=\"col-md-2\"> <b> <input value=" + $("#pa2").val() + " " + "\" class=\"form-c\" id=\"inp2" + intId2 + " " + "\" type=\"password\" /></b></div></div>");
                                        var fName4 = $(" <div class=\"col-md-2\"> <input value=" + $("#num").val() + " " + "\" class=\"form-\" id=\"nu" + intId2 + " " + "\" type=\"text\"/></div>");
                                        var fName5 = $(" <div class=\"col-md-2\"> <input value=" + $("#day").val() + " " + "\" class=\"form-col\" id=\"da" + intId2 + " " + "\" type=\"text\"/></div>");
                                        fieldWrapper.append(fName);
                                        fieldWrapper.append(fName2);
                                        fieldWrapper.append(fName3);
                                        fieldWrapper.append(fName4);
                                        fieldWrapper.append(fName5);
                                        $("#buildyourform2").append(fieldWrapper);
                                    });
                                </script>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
                    
        </div>

    </section>
    <!-- Plugins Js -->
    <script src="../../assets/js/app.min.js"></script>
    <script src="../../assets/js/form.min.js"></script>
    <!-- Custom Js -->
    
    <script src="../../assets/js/admin.js"></script>
    <script src="../js/AddC.js"></script>

    <script src="/assets/js/table.min.js"></script>
    <!-- Custom Js -->

    <script src="/assets/js/bundles/export-tables/dataTables.buttons.min.js"></script>
    <script src="/assets/js/bundles/export-tables/buttons.flash.min.js"></script>
    <script src="/assets/js/bundles/export-tables/jszip.min.js"></script>
    <script src="/assets/js/bundles/export-tables/pdfmake.min.js"></script>
    <script src="/assets/js/bundles/export-tables/vfs_fonts.js"></script>
    <script src="/assets/js/bundles/export-tables/buttons.html5.min.js"></script>
    <script src="/assets/js/bundles/export-tables/buttons.print.min.js"></script>
    <script src="/assets/js/pages/tables/jquery-datatable.js"></script>
        <script src="/assets/js/pages/forms/advanced-form-elements.js"></script>
        <script src="/assets/js/pages/forms/basic-form-elements.js"></script>



    <script src="/assets/js/bundles//multiselect/js/jquery.multi-select.js"></script>
    <script src="/assets/js/bundles/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js"></script>
    <!-- Custom Js -->
    <script src="/assets/js/pages/forms/advanced-form-elements.js"></script>

         <script src="/assets/js/pages/tables/jquery-datatable.js"></script>
        <script src="/assets/js/bundles//multiselect/js/jquery.multi-select.js"></script>
        <script src="/assets/js/pages/apps/chat.js"></script>
</body>
</html>

