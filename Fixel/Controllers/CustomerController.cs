﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.UI.WebControls;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using Fixel.BLL;
using System.Runtime.Remoting.Contexts;
using System.Web;
using System.Web.SessionState;


namespace Fixel
{
    public class CustomerController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public int Post([FromBody]Customer temp)
        { 
            int id = temp.AddCus();
            HttpContext context = HttpContext.Current;
            context.Session["CustomerID"] = id;
            context.Session["Customer"] = temp;
            context.Session["CustomerName"] = temp.CustomerName;
            return id;
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}