﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using Fixel.BLL;

namespace Fixel
{
    public class IncomeController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            List<Income> Exp = Income.GetAll(id);
            return JsonConvert.SerializeObject(Exp);
        }
        public string Get(int id, int id2)
        {
            List<Income> Exp = Income.GetBId(id, id2);
            return JsonConvert.SerializeObject(Exp);
        }
        // POST api/<controller>
        public string Post([FromBody]Income temp)
        {
            temp.AddIncome();
            return JsonConvert.SerializeObject(temp);
        }

        // PUT api/<controller>/5
        public string Put(int id, [FromBody]Income temp)
        {
            temp.pot(id);
            return JsonConvert.SerializeObject(temp);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            Income.Delete(id);
        }
    }
}