﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using Fixel.BLL;
using System.Web;

namespace Fixel
{
    public class InvoicingController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public bool Get(int id,int id2,string id3)
        {
            return Invoicing.Used(id3, id2, id);
        }
        public string Get(int id, int id2)
        {
            List<Invoicing> Inv = Invoicing.GetBid(id, id2);
            return JsonConvert.SerializeObject(Inv);
        }
        // POST api/<controller>
        public void Post(int id)
        {
            string FileName = "";
            var httpRequest = HttpContext.Current.Request;//Index of a column on which it is clicked
            if (httpRequest.Files.Count > 0)
            {
                foreach (string file in httpRequest.Files)
                {
                    var postedFile = httpRequest.Files[file];
                    var filePath = HttpContext.Current.Server.MapPath("~/uploads/invoice/" + postedFile.FileName);
                    postedFile.SaveAs(filePath);
                    FileName = postedFile.FileName;
                }
               
            }

            Invoicing inv = new Invoicing() {
                Amount = int.Parse(httpRequest["Amount"].ToString()),
                CustomerID = int.Parse(httpRequest["CustomerID"].ToString()),
                date = httpRequest["date"].ToString(),
                VendorsName = httpRequest["VendorsName"].ToString(),
                InvoicingNamber = int.Parse(httpRequest["InvoicingNamber"].ToString()),
                InvoicingBill = FileName,
                InvoicingId = id
            };
            if(inv.InvoicingId==-1)
            {
                inv.AddInvoice();
            }
            else
            {
                inv.pot(inv.InvoicingId);
            }

        }

        // PUT api/<controller>/5
        public string Put(int id, [FromBody]Invoicing temp)
        {
            temp.pot(id);
            return JsonConvert.SerializeObject(temp);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            Invoicing.Delete(id);
        }
    }
}