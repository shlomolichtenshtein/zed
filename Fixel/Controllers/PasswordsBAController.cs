﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using Fixel.BLL;

namespace Fixel
{
    public class PasswordsBAController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            string Exp = PasswordsBA.name(id);
            return JsonConvert.SerializeObject(Exp);
        }
        public string Get(int id, int id2)
        {
            List<PasswordsBA> Exp = PasswordsBA.Getby(id);
            return JsonConvert.SerializeObject(Exp);
        }


        // POST api/<controller>
        public void Post(List<PasswordsBA> temp)
        {
            PasswordsBA d = new PasswordsBA();
            d.Postc(temp);
        }

        // PUT api/<controller>/5
        public void Put(string id, int id2,string id3)
        {
            PasswordsBA.pot(id, id2, id3);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}