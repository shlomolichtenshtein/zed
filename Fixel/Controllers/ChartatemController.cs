﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using Fixel.BLL;

namespace Fixel
{
    public class ChartatemController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5

        public string Get(int id)
        {
            List<chartatem> f = chartatem.Expensechart(id);
            return JsonConvert.SerializeObject(f);
        }
        public string Get(int id, string id2)
        {
            List<chartatem> f = chartatem.VendroRep(id,id2);
            return JsonConvert.SerializeObject(f);
        }
        public string Get(int id,int id2,int id3)
        {
            List<chartatem> f = chartatem.VendroRepAll(id,id2);
            return JsonConvert.SerializeObject(f);
        }
        public string Get(int id, int id3, int id2,int id4)
        {
            List<chartatem> f = chartatem.modalAll(id, id3, id2,id4);
            return JsonConvert.SerializeObject(f);

        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}