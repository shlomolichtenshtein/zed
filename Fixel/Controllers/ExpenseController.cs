﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using Fixel.BLL;

namespace Fixel
{
    public class ExpenseController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            List<Expense> Exp = Expense.GetAll(id);
            return JsonConvert.SerializeObject(Exp);
        }
        public string Get(int id, int id2)
        {
            List<Expense> Exp = Expense.GetBId(id, id2);
            return JsonConvert.SerializeObject(Exp);
        }
        // POST api/<controller>
        public string Post([FromBody]Expense temp)
        {
            temp.AddExpen();
            return JsonConvert.SerializeObject(temp);
        }

        // PUT api/<controller>/5
        public string Put(int id, [FromBody]Expense temp)
        {
            temp.pot(id);
            return JsonConvert.SerializeObject(temp);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            Expense.Delete(id);
        }
    }
}