﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using Fixel.BLL;

namespace Fixel
{
    public class CreditClearingController : ApiController
    {
        // GET api/<controller>
        public string Get(int id)
        {
            
            List<CreditClearing> Exp = CreditClearing.GetAll(id);
            return JsonConvert.SerializeObject(Exp);
        }

        // GET api/<controller>/5
        public string Get()
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}