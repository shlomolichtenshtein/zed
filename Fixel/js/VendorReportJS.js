﻿//Creating a vendor report chart showing how much supplier has bought in the past year and when you click on one vendor's column, it shows a vendor's chart how many each month he bought from
//that vendor, including a table of how much he would have earned if he had received a 5 % and a 7 % and a 10 % discount
$(function (e) {
    'use strict'
    var na = [];
    var se = [];
    var b = parseInt(1);
    var c = parseInt(1);
    var Chartatem;
    var nam;

    $("#o").hide();
        $("#p").hide();
    $.ajax({
        type: "GET",
        url: "/api/Chartatem/" + us + "/" + b + "/" + c,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            Chartatem = JSON.parse(data);
            vall();

        }
    });
    function vall() {
        try {

            for (var i = 0; i < Chartatem.length; i++) {
                na[i] = Chartatem[i].Name;
                se[i] = Chartatem[i].Sales;
            }
            var ctx = document.getElementById("VendRep");
            if (ctx) {
                ctx.height = 150;
                if (myChart) { myChart.destroy(); }
                var myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: na,
                        datasets: [{
                            label: " סך קניות לשנה ",
                            data: se,
                            borderColor: "rgba(0, 123, 255, 0.9)",
                            borderWidth: "0",
                            backgroundColor: "rgba(0, 200, 255, 0.5)"
                        }]
                    },
                    options: {
                        legend: {
                            position: 'top',
                            labels: {
                                fontFamily: 'Poppins'
                            }
                        },
                        //When you click on a column from one of the vendors, it shows a chart of the same vendor how many each month it bought from that vendor
                        onClick: function (e) {
                            var activePoints = myChart.getElementsAtEvent(e);
                            var inte = activePoints[0]._index;//Index of a column on which it is clicked
                                $("#o").show();
                            $("#p").show();
                            
                            for (var i = 0; i < na.length; i++) {
                                if (i == inte) {
                                    nam = na[i];
                                }
                            }
                            $.ajax({
                                type: "GET",
                                url: "/api/Chartatem/" + us + "/" + nam,
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (data) {
                                    Chartatem = JSON.parse(data);
                                    Gp();
                                }
                            });
                            function Gp() {
                                var csA = [];
                                var cm = [];
                                var su = 0;
                                try {
                                    for (var i = 0; i < Chartatem.length; i++) {
                                        cm[i] = Chartatem[i].Months;
                                        csA[i] = Chartatem[i].Sales;
                                        //var a = cs[i] * 100 / ci[i];
                                        //var b = a.toFixed(0);
                                        su += Chartatem[i].Sales;



                                    }
                                    var su1 = su * 0.05;
                                    var su2 = su * 0.07;
                                    var su3 = su * 0.10;
                                    $("#SumI").val(su.toFixed(0));
                                    $("#ProductCost").val(su1.toFixed(0));
                                    $("#MarketingCost").val(su2.toFixed(0));
                                    $("#WearRepetition").val(su3.toFixed(0));
                                    $('#singel-bar-chart').remove();
                                    $('#c').append('<canvas id="singel-bar-chart"><canvas>');
                                    var ctx2 = document.getElementById("singel-bar-chart");
                                    if (ctx2) {
                                        ctx2.height = 150;
                                        var myChart2 = new Chart(ctx2, {
                                            type: 'bar',
                                            data: {
                                                labels: cm,
                                                datasets: [{
                                                    label: " סך קניות לחודש מספק" + " " +   nam,
                                                    data: csA,
                                                    borderColor: "rgba(0, 123, 255, 0.9)",
                                                    borderWidth: "0",
                                                    backgroundColor: "rgba(0, 200, 255, 0.5)"
                                                }]
                                            },
                                            options: {
                                                legend: {
                                                    position: 'top',
                                                    labels: {
                                                        fontFamily: 'Poppins'
                                                    }
                                                },
                                                scales: {
                                                    xAxes: [{
                                                        ticks: {
                                                            fontFamily: "Poppins",
                                                            fontColor: "#9aa0ac", // Font Color
                                                        }
                                                    }],
                                                    yAxes: [{
                                                        ticks: {
                                                            beginAtZero: true,
                                                            fontFamily: "Poppins",
                                                            fontColor: "#9aa0ac", // Font Color
                                                        }
                                                    }]
                                                }
                                            }
                                        });
                                    }

                                } catch (error) {
                                    console.log(error);
                                }

                            }
                        },
                        scales: {
                            xAxes: [{
                                ticks: {
                                    fontFamily: "Poppins",
                                    fontColor: "#9aa0ac", // Font Color

                                }
                            }],
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    fontFamily: "Poppins",
                                    fontColor: "#9aa0ac", // Font Color
                                }
                            }]
                        }
                    }
                });
            }

        } catch (error) {
            console.log(error);
        }
    }

})




