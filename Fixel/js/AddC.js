﻿'use strict';
$(function () {
    //Horizontal form basic
    $('#wizard_horizontal').steps({
        headerTag: 'h2',
        bodyTag: 'section',
        transitionEffect: 'slideLeft',
        onInit: function (event, currentIndex) {
            setButtonWavesEffect(event);
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
            setButtonWavesEffect(event);
        }
    });

    //Vertical form basic
    $('#wizard_vertical').steps({
        headerTag: 'h2',
        bodyTag: 'section',
        transitionEffect: 'slideLeft',
        stepsOrientation: 'vertical',
        onInit: function (event, currentIndex) {
            setButtonWavesEffect(event);
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
            setButtonWavesEffect(event);
        }
    });

    //Advanced form with validation
    var form = $('#wizard_with_validation').show();
    form.steps({
        headerTag: 'h3',
        bodyTag: 'fieldset',
        transitionEffect: 'slideLeft',
        onInit: function (event, currentIndex) {

            //Set tab width
            var $tab = $(event.currentTarget).find('ul[role="tablist"] li');
            var tabCount = $tab.length;
            $tab.css('width', (100 / tabCount) + '%');

            //set button waves effect
            setButtonWavesEffect(event);
        },
        onStepChanging: function (event, currentIndex, newIndex) {
            if (currentIndex > newIndex) { return true; }

            if (currentIndex < newIndex) {
                form.find('.body:eq(' + newIndex + ') label.error').remove();
                form.find('.body:eq(' + newIndex + ') .error').removeClass('error');
            }

            form.validate().settings.ignore = ':disabled,:hidden';
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
            setButtonWavesEffect(event);
        },
        onFinishing: function (event, currentIndex) {
            form.validate().settings.ignore = ':disabled';
            return form.valid();
        },
        onFinished: function (event, currentIndex) {
            var addc = {
                CustomerName: $("#CustomerName").val(), Email: $("#Email").val(), Phone: $("#Phone").val(),
                Password: $("#password").val(), IdentityCard: $("#IdentityCard").val()
            };
            var idcu;
            $.ajax({
                            type: "Post",
                            url: "/api/Customer",
                            data: JSON.stringify(addc),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                idcu = JSON.stringify(data);
                                if (idcu != null) {
                                    Addb(idcu);
                                    Addc(idcu);
                                }
                                
                            }
                        })

            swal({
                title: "הוספת לקוח חדש",
                text: "מזל טוב",
                type: "success",
                showConfirmButton: false

            });
            setTimeout(function () { location.replace('Expenses-Table.aspx'); }, 5000);
        }
    });
    function Addb(idcu) {
        if (intId > 0) {

            var arr = new Array;
            var inputs = $(".form-contro");
            var inputs1 = $(".form-contr");
            var inputs2 = $(".form-cont");
            for (var i = 0; i < inputs.length; i++)
            {
                var BanksID = $(inputs[i]);
                var bi = BanksID[0].name;
                arr.push({
                    BanksID: parseInt(bi), CustomerID: parseInt(idcu), Identify: $(inputs1[i]).val(), Password: $(inputs2[i]).val(),
                    AccountNumber: '0', Balance: 0, Frame: 0, Attraction: 0
                });
            } 
            var PasswordsBA = JSON.stringify(arr);
            $.ajax({
                type: "Post",
                url: "/api/PasswordsBA",
                data: PasswordsBA,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    PasswordsBA = JSON.stringify(data);
                }
            })
            
        }

    }
    function Addc(idcu) {
        if (intId > 0) {

            var arr = new Array;
            var inputs = $(".form-con");
            var inputs1 = $(".form-co");
            var inputs2 = $(".form-c");
            var inputs3 = $(".form-");
            var inputs4 = $(".form-col");
            for (var i = 0; i < inputs.length; i++) {
                var CreditID = $(inputs[i]);
                var ci = CreditID[0].name;
                var tn = $(inputs3[i]).val() ? $(inputs3[i]).val() : 0;
                arr.push({
                    CreditID: parseInt(ci), CustomerID: parseInt(idcu), Identify: $(inputs1[i]).val(), Password: $(inputs2[i]).val(),
                    TicketNumber: tn, Day: parseInt($(inputs4[i]).val())
                });
            }
            var PasswordsCredit = JSON.stringify(arr);
            $.ajax({
                type: "Post",
                url: "/api/PasswordsCredit",
                data: PasswordsCredit,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    PasswordsCredit = JSON.stringify(data);
                }
            })

        }

    }
    form.validate({
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        },
        rules: {
            'confirm': {
                equalTo: '#password'
            }
        }
    });
});

function setButtonWavesEffect(event) {
    $(event.currentTarget).find('[role="menu"] li a').removeClass('waves-effect');
    $(event.currentTarget).find('[role="menu"] li:not(.disabled) a').addClass('waves-effect');
}