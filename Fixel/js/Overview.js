﻿$(function (e) {
	var Ac = [];
	var Ba = [];
	var At = [];
	var Fr = [];
	var Bank = [];


	var PasswordsBA;
	$.ajax({
		type: "GET",
		url: "/api/PasswordsBA/" + 101 + "/" + 1,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (data) {

			PasswordsBA = JSON.parse(data);
			if (PasswordsBA.length > 1) {

			}
			drawchart();
			

		}






	});

	function drawchart() {
		try {

			// single bar chart
			var ctx = document.getElementById("singel-bar-chart");
			if (ctx) {
				ctx.height = 150;
				var myChart = new Chart(ctx, {
					type: 'bar',
					data: {
						labels: ["Sun", "Mon", "Tu", "Wed", "Th", "Fri", "Sat"],
						datasets: [{
							label: "My First dataset",
							data: [40, 55, 75, 81, 56, 55, 40],
							borderColor: "rgba(0, 123, 255, 0.9)",
							borderWidth: "0",
							backgroundColor: "rgba(0, 123, 255, 0.5)"
						}]
					},
					options: {
						legend: {
							position: 'top',
							labels: {
								fontFamily: 'Poppins'
							}

						},
						scales: {
							xAxes: [{
								ticks: {
									fontFamily: "Poppins",
									fontColor: "#9aa0ac", // Font Color

								}
							}],
							yAxes: [{
								ticks: {
									beginAtZero: true,
									fontFamily: "Poppins",
									fontColor: "#9aa0ac", // Font Color
								}
							}]
						}
					}
				});
			}

		} catch (error) {
			console.log(error);
		}

		try {
			//Sales chart
			var ctx = document.getElementById("line-chart2");
			if (ctx) {
				ctx.height = 150;
				var myChart = new Chart(ctx, {
					type: 'line',
					data: {
						labels: ["2010", "2011", "2012", "2013", "2014", "2015", "2016"],
						type: 'line',
						defaultFontFamily: 'Poppins',
						datasets: [{
							label: "Foods",
							data: [0, 30, 10, 120, 50, 63, 10],
							backgroundColor: 'transparent',
							borderColor: '#222222',
							borderWidth: 2,
							pointStyle: 'circle',
							pointRadius: 3,
							pointBorderColor: 'transparent',
							pointBackgroundColor: '#222222',
						}, {
							label: "Electronics",
							data: [0, 50, 40, 80, 40, 79, 120],
							backgroundColor: 'transparent',
							borderColor: '#f96332',
							borderWidth: 2,
							pointStyle: 'circle',
							pointRadius: 3,
							pointBorderColor: 'transparent',
							pointBackgroundColor: '#f96332',
						}]
					},
					options: {
						responsive: true,
						tooltips: {
							mode: 'index',
							titleFontSize: 12,
							titleFontColor: '#000',
							bodyFontColor: '#000',
							backgroundColor: '#fff',
							titleFontFamily: 'Poppins',
							bodyFontFamily: 'Poppins',
							cornerRadius: 3,
							intersect: false,
						},
						legend: {
							display: false,
							labels: {
								usePointStyle: true,
								fontFamily: 'Poppins',
							},
						},
						scales: {
							xAxes: [{
								display: true,
								gridLines: {
									display: false,
									drawBorder: false
								},
								scaleLabel: {
									display: false,
									labelString: 'Month'
								},
								ticks: {
									fontFamily: "Poppins",
									fontColor: "#9aa0ac", // Font Color
								}
							}],
							yAxes: [{
								display: true,
								gridLines: {
									display: false,
									drawBorder: false
								},
								scaleLabel: {
									display: true,
									labelString: 'Value',
									fontFamily: "Poppins"

								},
								ticks: {
									fontFamily: "Poppins",
									fontColor: "#9aa0ac", // Font Color
								}
							}]
						},
						title: {
							display: false,
							text: 'Normal Legend'
						}
					}
				});
			}


		} catch (error) {
			console.log(error);
		}



		try {

			//doughut chart
			var ctx = document.getElementById("doughut-chart");
			if (ctx) {
				ctx.height = 150;
				var myChart = new Chart(ctx, {
					type: 'doughnut',
					data: {
						datasets: [{
							data: [45, 25, 20, 10],
							backgroundColor: [
								"rgba(0, 123, 255,0.9)",
								"rgba(0, 123, 255,0.7)",
								"rgba(0, 123, 255,0.5)",
								"rgba(0,0,0,0.07)"
							],
						}],
					},
					formatter: "aaaa",

					options: {
						legend: {
							position: 'top',
							labels: {
								fontFamily: 'Poppins'
							}

						},
						responsive: true
					}
				});
			}


		} catch (error) {
			console.log(error);
		}
		try {

			//doughut chart
			var ctx = document.getElementById("doughut-charta");
			if (ctx) {
				ctx.height = 150;
				var myChart = new Chart(ctx, {
					type: 'doughnut',
					data: {
						datasets: [{
							data: [45, 25, 20, 10],
							backgroundColor: [
								"rgba(0, 123, 255,0.9)",
								"rgba(0, 123, 255,0.7)",
								"rgba(0, 123, 255,0.5)",
								"rgba(0,0,0,0.07)"
							],
						}],
					},
					formatter: "aaaa",

					options: {
						legend: {
							position: 'top',
							labels: {
								fontFamily: 'Poppins'
							}

						},
						responsive: true
					}
				});
			}


		} catch (error) {
			console.log(error);
		}

		try {
			//bar chart
			var ctx = document.getElementById("bar-chart");
			if (ctx) {
				ctx.height = 600;
				var myChart = new Chart(ctx, {
					type: 'bar',
					defaultFontFamily: 'Poppins',
					data: {
						labels: ["January"],
						datasets: [{
							label: "My First dataset",
							data: [65],
							borderColor: "rgba(0, 123, 255, 0.9)",
							borderWidth: "0",
							backgroundColor: "rgba(0, 123, 255, 0.5)",
							fontFamily: "Poppins"
						},
						{
							label: "My Second dataset",
							data: [28],
							borderColor: "rgba(0,0,0,0.09)",
							borderWidth: "0",
							backgroundColor: "rgba(0,0,0,0.07)",
							fontFamily: "Poppins"
						}
						]
					},
					options: {
						legend: {
							position: 'top',
							labels: {
								fontFamily: 'Poppins'
							}

						},
						scales: {
							xAxes: [{
								ticks: {
									fontFamily: "Poppins",
									fontColor: "#9aa0ac", // Font Color

								}
							}],
							yAxes: [{
								ticks: {
									beginAtZero: true,
									fontFamily: "Poppins",
									fontColor: "#9aa0ac", // Font Color
								}
							}]
						}
					}
				});
			}


		} catch (error) {
			console.log(error);
		}
	}
	
});
function a() { alert = "ff"; }