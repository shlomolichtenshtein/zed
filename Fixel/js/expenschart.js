﻿//Create a Sales and Purchase and Gross Profit Chart as well as a Gross Profit Percentage Chart for last year
    $(function (e) {
        'use strict'
        var ci = [];
        var ce = [];
        var cm = [];
        var cs = [];
        var csA = [];


        var Chartatem;
        $.ajax({
            type: "GET",
            url: "/api/Chartatem/"+us,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {

                Chartatem = JSON.parse(data);
                drawchart();

            }






        });
        function drawchart() {
            for (var i = 0; i < Chartatem.length; i++) {
                ce[i] = Chartatem[i].Expend;
                ci[i] = Chartatem[i].Income;
                cm[i] = Chartatem[i].Months;
                cs[i] = Chartatem[i].SumF;
            }
            /* line chart */
            var chart = document.getElementById('echart_area_line');
            var lineChart = echarts.init(chart);

            lineChart.setOption({
                title: {
                    text: "מכירות/קניות/רווח גולמי",
                    subtext: ""
                },
                tooltip: {
                    trigger: "axis"
                },
                legend: {
                    x: 220,
                    y: 20,
                    data: ["רווח גולמי","קניות", "מכירות"]
                },
                toolbox: {
                    show: !0,
                    feature: {
                        magicType: {
                            show: !0,
                            title: {
                                line: "קו",
                                bar: "Bar",
                                stack: "Stack",
                                tiled: "Tiled"
                            },
                            type: ["line", "bar", "stack", "tiled"]
                        },
                        restore: {
                            show: !0,
                            title: "שחזר"
                        },
                        saveAsImage: {
                            show: !0,
                            title: "שמור כתמונה"
                        }
                    }
                },
                calculable: !0,
                xAxis: [{
                    type: "category",
                    boundaryGap: !1,
                    data: cm,
                    axisLabel: {
                        fontSize: 10,
                        color: '#9aa0ac'
                    }
                }],
                yAxis: [{
                    type: "value",
                    axisLabel: {
                        fontSize: 10,
                        color: '#9aa0ac'
                    }
                }],
                series: [{
                    name: "מכירות",
                    type: "line",
                    smooth: !0,
                    itemStyle: {
                        normal: {
                            areaStyle: {
                                type: "default"
                            }
                        }
                    },

                    data: ci
                }, {
                    name: "קניות",
                    type: "line",
                    smooth: !0,
                    itemStyle: {
                        normal: {
                            areaStyle: {
                                type: "default"
                            }
                        }
                    },
                    data: ce
                    }, {
                        name: "רווח גולמי",
                        type: "line",
                        smooth: !0,
                        itemStyle: {
                            normal: {
                                areaStyle: {
                                    type: "default"
                                }
                            }
                        },
                        data: cs
                    }
                ],
                color: ['#007bff', '#dc3545','#28a745',]
            });
            Gp();
        }

        function Gp() {


            try {

                for (var i = 0; i < Chartatem.length; i++) {
                    var a = cs[i] * 100 / ci[i];
                    var b= a.toFixed(0)
                    csA[i] =b;
 
                  

                }



                // single bar chart
                var ctx = document.getElementById("singel-bar-chart");
                if (ctx) {
                    ctx.height = 150;
                    var myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: cm,
                            datasets: [{
                                label: "רווח גולמי באחוזים ",
                                data: csA,
                                borderColor: "rgba(0, 123, 255, 0.9)",
                                borderWidth: "0",
                                backgroundColor: "rgba(0, 200, 255, 0.5)"
                            }]
                        },
                        options: {
                            legend: {
                                position: 'top',
                                labels: {
                                    fontFamily: 'Poppins'
                                }

                            },
                            scales: {
                                xAxes: [{
                                    ticks: {
                                        fontFamily: "Poppins",
                                        fontColor: "#9aa0ac", // Font Color

                                    }
                                }],
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true,
                                        fontFamily: "Poppins",
                                        fontColor: "#9aa0ac", // Font Color
                                    }
                                }]
                            }
                        }
                    });
                }

            } catch (error) {
                console.log(error);
            }
        }
    });
