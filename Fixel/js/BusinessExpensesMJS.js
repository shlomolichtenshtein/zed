﻿$(function (e) {
    var chart = document.getElementById('echart_pie');
    var barChart = echarts.init(chart);

    barChart.setOption({
        tooltip: {
            trigger: "item",
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        legend: {
            x: "center",
            y: "bottom",
            data: ["Direct Access", "E-mail Marketing", "Union Ad", "Video Ads", "Search Engine"]
        },

        calculable: !0,
        series: [{
            name: "Chart Data",
            type: "pie",
            radius: "55%",
            center: ["50%", "48%"],
            data: [{
                value: 335,
                name: "Direct Access"
            }, {
                value: 310,
                name: "E-mail Marketing"
            }, {
                value: 234,
                name: "Union Ad"
            }, {
                value: 135,
                name: "Video Ads"
            }, {
                value: 548,
                name: "Search Engine"
            }]
        }],
        color: ['#575B7A', '#DE725C', '#DFC126', '#72BE81', '#50A5D8']
    });
});