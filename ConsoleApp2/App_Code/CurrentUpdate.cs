﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp2.BLL;

namespace ConsoleApp2
{
    namespace Utils
    {
        class CurrentUpdate
        {
            public string Date { get; set; }
            public string Description { get; set; }
            public int Reference { get; set; }
            public double Promotional { get; set; }
            public double obligation { get; set; }


            public CurrentUpdate()
            {

            }

            public static void GatCurrent(List<CurrentUpdate> Upd, double balance, double attraction, double frame, int custumerid, int bankid, int PasswordsBAid)
            {
                List<Expense> ExpenseG = Expense.GetAl(custumerid, 30);
               for(int i=0;i<Upd.Count;i++)//מעבר על כל הפעולות שחזרו מהבנק
                {
                    if ((Upd[i].Description.IndexOf("משיכת שיק")!=-1) || (Upd[i].Description=="שיק" ))//בירור האם זה משיכת שיק 
                    {
                        List<Checkbooks> ChGatAll = Checkbooks.GetAll(custumerid, PasswordsBAid);
                        int chid = 0;
                        for (int j=0;j<ChGatAll.Count;j++)//מעבר על פנקסי השיקים במערכת
                        {   
                            if(ChGatAll[j].CheckbooksNumber==Upd[i].Reference)
                            {
                                if(ChGatAll[j].active==1)
                                {
                                    Checkbooks ch = new Checkbooks();  
                                    ch.active = 2;
                                    ch.CheckbooksID = ChGatAll[j].CheckbooksID;
                                    chid = -1;
                                    Checkbooks.potAc(ch);//ליצור פונקציה שתעדכן לשנים
                                    break;
                                }
                                else if(ChGatAll[j].active ==0)
                                {
                                    chid = ChGatAll[j].CheckbooksID;//ליצור פונקצי השמעדכנן את ההוצאה לפי מספר השיק
                                }
                            }
                        }
                        if (chid == 0)                       
                        {
                            Checkbooks.pot(Upd[i].Reference, PasswordsBAid);
                            chid = Checkbooks.Check(Upd[i].Reference, custumerid);
                        }
                        if (chid != -1)
                        {
                            Expense Exp;
                            Exp = new Expense();
                            Exp.Date = Upd[i].Date;
                            Exp.Amount = (int)Upd[i].obligation;
                            Exp.ActualDate = Upd[i].Date;
                            Exp.ExpenseName = "שיק לטיפול";
                            Exp.CheckbooksID = chid;
                            Exp.ConstantVariable = 0;
                            Exp.Period = 1;
                            Exp.CustomerID = custumerid;
                            Exp.Payments = 1;
                            Exp.ClassificationsID = 0;
                            Exp.SubclassificationsID = 0;
                            Exp.VendorsID = 0;
                            Exp.PasswordsBAID = 0;
                            Exp.PasswordsCRID = 0;
                            Exp.Cash = 0;
                            Exp.DueDate = "";
                            Expense.AddExpense(Exp);
                        }
                    }
                    //האם העברה או זיכוי לבדוק מה עושים
                    else
                    {
                        List<Types> Tye = Types.GatAll();
                    }
                }
            }

        }
    }
}
