﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConsoleApp2.BLL;
using System.Data;

namespace ConsoleApp2
{
    namespace DAL
    {
        public class VendorsDal
        {
            public static int Vend(string data2,int Cid)
            {
                DadaBase dbc = new DadaBase();
                string sqle = "select VendorsID as Vend from Vendors where VendorsName='" + data2 + "' and CustomerID='"+ Cid + "'";
                DataTable da = dbc.execute(sqle);
                if(da.Rows.Count == 0)
                {
                    return -1;
                }
                int Code = 0;
                return Code = (int)da.Rows[0]["Vend"];
            }
            public static DataTable GetAll(int us)
            {
                DadaBase Db = new DadaBase();
                string sql = "select * From Vendors where CustomerID=" + us;

                return Db.execute(sql);
            }
            public static DataTable GetAllBiV(int us,string name )
            {
                DadaBase Db = new DadaBase();
                string sql = "select DISTINCT T1.ExpenseID AS id, ExpenseName as VendorsName,[Date],t1.Amount as payment, 0 as  Amount,0 as InvoicingNamber, source, DueDate   from Expense " +
                    "t1 LEFT join(select CAST(CheckbooksNumber as varchar(20)) as source, t2.ExpenseID from Checkbooks t1 join Expense t2 on t1.CheckbooksID = t2.CheckbooksID" +
                    " union select CreditName as source, t2.ExpenseID from Credit t1 join PasswordsCR t3 on t1.CreditID = t3.CreditID  join Expense t2 on t3.PasswordsCRID = t2.PasswordsCRID" +
                    " union select BanksName as source,t2.ExpenseID from Banks t1  join PasswordsBA t5 on t1.BanksID = t5.BanksID join Expense t2 on t5.PasswordsBAID = t2.PasswordsBAID " +
                    "union select 'מזומן' as source, ExpenseID from Expense where Cash <> 0) T_source on t1.ExpenseID = T_source.ExpenseID where VendorsID is not null and ExpenseName = '" + name +"'  " +
                    "and t1.CustomerID =" + us+ "union select t1.InvoicingId AS id,  VendorsName,[Date],0 as payment, Amount,InvoicingNamber,'', '' from Invoicing t1 join Vendors t2 on t1.VendorsID=t2.VendorsID where VendorsName= '" + name + "' " +
                    "and t1.CustomerID= " + us + " order by [Date] asc";

                return Db.execute(sql);
            }
            public static DataTable GetAllBalance(int us)
            {
                DadaBase Db = new DadaBase();
                string sql = "select p.VendorsName as VendorsName,sum(p.se) as RequiredSummary,sum(p.si1)as CreditSummary, sum(P.sd)as InvoiciMM, " +
                    " sum(p.si1-p.se) as balance from (select ExpenseName as VendorsName, sum(Amount) as se, 0 as si1, 0 as sd from Expense t1 " +
                    "join Vendors t2 on t1.VendorsID = t2.VendorsID where t1.CustomerID = " + us + " and t1.VendorsID <> 0 GROUP BY ExpenseName union " +
                    "select VendorsName, 0 as se1, sum(Amount) as si, 0 as ss from Invoicing t1 join Vendors t2 on t1.VendorsID = t2.VendorsID " +
                    "where t1.CustomerID = " + us + " and(DATEPART(MM, Date) <> MONTH(GETDATE()) OR DATEPART(YY, Date) <> Year(GETDATE())) GROUP BY VendorsName " +
                    "union select VendorsName AS V, 0 as se2, 0 as s, sum(Amount) as si2 from Invoicing t1 join Vendors t2 on t1.VendorsID = t2.VendorsID " +
                    "where t1.CustomerID = " + us + " and(DATEPART(MM, Date) = MONTH(GETDATE()) and DATEPART(YY, Date) = Year(GETDATE())) GROUP BY VendorsName)p " +
                    "GROUP BY p.VendorsName";

                return Db.execute(sql);
            }
            public static int  AddVendors(Vendors th)
            {
                DadaBase dbc = new DadaBase();
                string sqle = "INSERT into Vendors (VendorsName, CustomerID) values('" + th.VendorsName + "'," + th.CustomerID + ")";
                DataTable da = dbc.execute(sqle);
                return Vend(th.VendorsName,th.CustomerID);
            }
        }
    }

}
