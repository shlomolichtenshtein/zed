﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConsoleApp2.BLL;
using System.Data;
using ConsoleApp2.Utils;

namespace ConsoleApp2
{
    namespace DAL
    {
        public class ExpenseDal
        {
           
            public static bool  AddExpen(Expense th)

            {

                DadaBase dbc = new DadaBase();
                string sqle = "INSERT into Expense (ClassificationsID, SubclassificationsID,[Date], Amount, ActualDate, ExpenseName, VendorsID, CheckbooksID, PasswordsBAID, Cash, PasswordsCRID, ConstantVariable, Period, CustomerID, DueDate, Payments) values(" +th.ClassificationsID + "," + th.SubclassificationsID + ",'" + th.Date + "'," + th.Amount + ",'" + th.ActualDate + "','" + th.ExpenseName + "'," + th.VendorsID +"," + th.CheckbooksID + "," + th.PasswordsBAID +"," + th.Cash + "," + th.PasswordsCRID +","  + th.ConstantVariable + "," + th.Period + "," + th.CustomerID + ",'" + th.DueDate + "'," + th.Payments + ")";
                DataTable da = dbc.execute(sqle);
                return true;
            }

           
            public static DataTable GetAll(int CId)//Brings all expenses by customer
            {
                DadaBase Db = new DadaBase();
                string sql = "select ExpenseID, t1.CustomerID, [Date], ExpenseName, Amount,isnull(BanksName, 'מזומן') as BanksName,ClassificationsName," +
                    "SubclassificationsName, ConstantVariable,[Period],Payments from Expense t1 full join Checkbooks t2 on t1.CheckbooksID = t2.CheckbooksID " +
                    "full join PasswordsBA t5 on t1.PasswordsBAID = t5.PasswordsBAID full join PasswordsCR t6 on t1.PasswordsCRID = t6.PasswordsCRID full" +
                    " join PasswordsBA t3 on t2.PasswordsBAID = t3.PasswordsBAID  full join Classifications t7 on t1.ClassificationsID=t7.ClassificationsID" +
                    "  full join Subclassifications t8 on t1.SubclassificationsID = t8.SubclassificationsID full join Banks t4 on t3.BanksID = t4.BanksID or" +
                    " t5.BanksID = t4.BanksID or t6.BanksID = t4.BanksID where ExpenseName is not null and t1.CustomerID =" + CId;
                return Db.execute(sql);
            }

           
            public static DataTable GetBId(int CId, int EId)//Brings spend by id
            {
                DadaBase Db = new DadaBase();
                string sql = " select ExpenseID,[Date], t1.ClassificationsID, t1.SubclassificationsID, ClassificationsName, SubclassificationsName," +
                    " Amount,ActualDate,ExpenseName, VendorsID,CheckbooksNumber,BanksName,Cash,RIGHT(TicketNumber, 4) as FourNumber,t1.CustomerID," +
                    "ConstantVariable,Period,DueDate,Payments  from Expense t1 full join Classifications t2 on t1.ClassificationsID = t2.ClassificationsID" +
                    " full join Subclassifications t3 on t1.SubclassificationsID = t3.SubclassificationsID full join PasswordsBA t4 on t1.PasswordsBAID = " +
                    "t4.PasswordsBAID full join Banks t5 on t4.BanksID = t5.BanksID full join PasswordsCR t6 on t1.PasswordsCRID = t6.PasswordsCRID  full " +
                    "join Checkbooks t7 on t1.CheckbooksID = t7.CheckbooksID where ExpenseID =" + CId + "and t1.CustomerID =" + EId ;
                return Db.execute(sql);
            }

           
            public static DataTable GetA(int CId)//Brings the last 40 expenses
            {
                DadaBase Db = new DadaBase();
                string sql = "select top 40 ExpenseID,  [Date], ExpenseName, Amount,isnull(BanksName, 'מזומן') as BanksName,ClassificationsName," +
                    "SubclassificationsName, ConstantVariable,[Period],Payments    from Expense t1  full join Checkbooks t2 on t1.CheckbooksID = " +
                    "t2.CheckbooksID full join PasswordsBA t5 on t1.PasswordsBAID = t5.PasswordsBAID full join PasswordsCR t6 on t1.PasswordsCRID = " +
                    "t6.PasswordsCRID full join PasswordsBA t3 on t2.PasswordsBAID = t3.PasswordsBAID full join Classifications t7 on t1.ClassificationsID " +
                    "= t7.ClassificationsID  full join Subclassifications t8 on t1.SubclassificationsID = t8.SubclassificationsID full  join Banks t4 on" +
                    " t3.BanksID = t4.BanksID or t5.BanksID = t4.BanksID or t6.BanksID = t4.BanksID  where ExpenseName is not null " +
                    "and t1.CustomerID =" +  CId + "order by[Date] desc"  ;
                return Db.execute(sql);
            }

           
            public static DataTable GetAl(int CId, int da)//Brings your expenses by date 
            {
                DadaBase Db = new DadaBase();
                string sql = "select ExpenseID, t1.CustomerID,  [Date], ExpenseName, Amount,isnull(BanksName, 'מזומן') as BanksName,ClassificationsName," +
                    "SubclassificationsName, ConstantVariable,[Period],Payments   from Expense t1 full join Checkbooks t2 on t1.CheckbooksID = t2.CheckbooksID" +
                    " full  join PasswordsBA t5 on t1.PasswordsBAID = t5.PasswordsBAID full join PasswordsCR t6 on t1.PasswordsCRID = t6.PasswordsCRID full" +
                    " join PasswordsBA t3 on t2.PasswordsBAID = t3.PasswordsBAID full join Classifications t7 on t1.ClassificationsID = t7.ClassificationsID" +
                    " full join Subclassifications t8 on t1.SubclassificationsID = t8.SubclassificationsID full join Banks t4 on t3.BanksID = t4.BanksID or" +
                    " t5.BanksID = t4.BanksID or t6.BanksID = t4.BanksID where ExpenseName is not null and t1.CustomerID =" + CId + "and[Date] >" +
                    " GETDATE() -" + da + " order by[Date] asc";
                return Db.execute(sql);
            }

           
            public static DataTable GetAlld(int CId, string da1, string da2)//Brings your expenses by date range 
            {
                DadaBase Db = new DadaBase();
                string sql = "select ExpenseID, t1.CustomerID, [Date],  ExpenseName, Amount,isnull(BanksName, 'מזומן') as BanksName,ClassificationsName," +
                    "SubclassificationsName, ConstantVariable,[Period],Payments   from Expense t1 full join Checkbooks t2 on t1.CheckbooksID = t2.CheckbooksID" +
                    " full  join PasswordsBA t5 on t1.PasswordsBAID = t5.PasswordsBAID full join PasswordsCR t6 on t1.PasswordsCRID = t6.PasswordsCRID full " +
                    "join PasswordsBA t3 on t2.PasswordsBAID = t3.PasswordsBAID full join Classifications t7 on t1.ClassificationsID = t7.ClassificationsID" +
                    " full join Subclassifications t8 on t1.SubclassificationsID = t8.SubclassificationsID full join Banks t4 on t3.BanksID = t4.BanksID or " +
                    "t5.BanksID = t4.BanksID or t6.BanksID = t4.BanksID where ExpenseName is not null and t1.CustomerID =" + CId + "and " +
                    "[Date]>'" + da1 + "' and [Date]<'" + da2 + "' order by [Date] asc";
                return Db.execute(sql);
            }

           
            public static bool Delete(int id)
            {
                DadaBase Db = new DadaBase();
                string sql = " DELETE from Expense where ExpenseID ='" + id + "'";
                DataTable da = Db.execute(sql);
                return true;
            }

           
            public  static bool pot(Expense th)

            {

                DadaBase dbc = new DadaBase();
                string sqle = "update Expense set ClassificationsID = '" + th.ClassificationsID + "', SubclassificationsID = '" + th.SubclassificationsID + "'" +
                    ",[Date] = '" + th.Date +"', Amount = '" + th.Amount +"', ActualDate = '" + th.ActualDate + "', ExpenseName = '" + th.ExpenseName + "'" +
                    ", VendorsID = '" + th.VendorsID + "', CheckbooksID= '" + th.CheckbooksID + "', PasswordsBAID ='" + th.PasswordsBAID + "' , Cash = '"
                    + th.Cash + "', PasswordsCRID= '" + th.PasswordsCRID + "' , ConstantVariable = '" + th.ConstantVariable + "', Period = '" + th.Period + "'" +
                    " , CustomerID= '" + th.CustomerID + "' , DueDate= '" + th.DueDate + "' , Payments= '" + th.Payments + "'where ExpenseID='" + th.ExpenseID 
                    + "'";
                DataTable da = dbc.execute(sqle);
                return true;
            }

           
            public static bool ExtractDataFromExcel(string isql)

            {

                DadaBase dbc = new DadaBase();
                DataTable da = dbc.execute(isql);
                return true;
            }

        }
    }

}
