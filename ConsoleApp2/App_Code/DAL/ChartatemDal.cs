﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConsoleApp2.BLL;
using System.Data;


namespace ConsoleApp2
{
    namespace DAL
    {
        public class ChartatemDal
        {
            public static DataTable Expensechart(int id)
            {
                DadaBase dbe = new DadaBase();
                string sqle = "select p.Months,p.Years,sum(p.Inco) as Income, sum(p.Expe) as Expend,SUM(p.Inco-p.Expe) as SumF" +
                    " from (select DATEPART(MM, t1.Date) as Months,DATEPART(YYYY, t1.Date) as Years, sum(t1.Amount) as Inco,0 as Expe " +
                    "from Income t1 where t1.Date BETWEEN  DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()-365)-0,0)and DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()-0)-0,0) and t1.CustomerID =" + id +"and " +
                    "t1.ClassificationsID = 2 GROUP BY DATEPART(MM, t1.Date),DATEPART(YYYY, t1.Date) " +
                    "union select DATEPART(MM, t1.Date) as Months,DATEPART(YYYY, t1.Date) as Years,0 as I, " +
                    "sum(t1.Amount) as E from Expense t1  where t1.Date BETWEEN  DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()-365)-0,0)and DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()-0)-0,0) and t1.CustomerID =" + id  +
                    "and t1.ClassificationsID = 4 GROUP BY DATEPART(MM, t1.Date),DATEPART(YYYY, t1.Date)) p group by p.Months,p.Years";

                return dbe.execute(sqle);
            }
            public static DataTable VendroRep(int id, string VendroName)
            {
                DadaBase dbe = new DadaBase();
                string sqle = " select  DATEPART(MM,Date) as Months,DATEPART(YYYY, Date) as Years, sum(Amount) as Sales from Expense  t1 join Vendors t2 on t1.VendorsID = t2.VendorsID" +
                    " where  VendorsName='" + VendroName +"' and t1.CustomerID =" + id + "and Date BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE() - 365)-0,0) and" +
                    " DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE() - 0)-0,0)  GROUP BY DATEPART(YYYY, Date),DATEPART(MM, Date)";

                return dbe.execute(sqle);
            }
            public static DataTable VendroRepAll(int id)
            {
                DadaBase dbe = new DadaBase();
                string sqle = " select   ExpenseName as Name,sum(Amount) as Sales from Expense where VendorsID<>0 and CustomerID =" + id + "and" +
                    " Date BETWEEN DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE() - 365)-0,0) and DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE() - 0)-0,0)" +
                    " GROUP BY  ExpenseName";

                return dbe.execute(sqle);
            }
            public static DataTable ProfitLossAll(int id)
            {
                DadaBase dbe = new DadaBase();
                string sqle = "select p.m,p.y,SUM(p.Inco - p.Expe) as GrossProfit, sum(p.SalaryEmployees) as SalaryEmployees, sum(p.OtherBusinessExpenses) as OtherBusinessExpenses," +
                    " sum(p.Mortgage) as Mortgage , sum(p.interest) as interest , sum(p.CEOSalary) as CEOSalary from( select DATEPART(MM, t1.Date) as m, DATEPART(YYYY, t1.Date) as y," +
                    " sum(t1.Amount) as Inco, 0 as Expe, 0 as SalaryEmployees, 0 as OtherBusinessExpenses, 0 as Mortgage, 0 as interest, 0 as CEOSalary from Income t1 where" +
                    " t1.CustomerID =" + id + " and t1.ClassificationsID = 2 GROUP BY DATEPART(MM, t1.Date), DATEPART(YYYY, t1.Date) union select DATEPART(MM, t1.Date) as m, " +
                    "DATEPART(YYYY, t1.Date) as y, 0 as I, sum(t1.Amount) as E, 0 as SalaryEmployee, 0 as OtherBusinessExpense, 0 as Mortgag, 0 as interes, 0 as CEOSalar from Expense t1" +
                    " where t1.CustomerID =" + id + "and t1.ClassificationsID = 4 GROUP BY DATEPART(MM, t1.Date), DATEPART(YYYY, t1.Date) union  select DATEPART(MM, Date) as m," +
                    " DATEPART(YYYY, Date) as y, 0 as Ia, 0 as eI, sum(Amount) as SalaryEmployees, 0 as OtherBusinessExpens, 0 as Mortga, 0 as intere, 0 as CEOSala from Expense where" +
                    " CustomerID = " + id + "and ClassificationsID = 2 and SubclassificationsID = 2 group by DATEPART(MM, Date), DATEPART(YYYY, Date) union select DATEPART(MM, Date) as m," +
                    " DATEPART(YYYY, Date) as y, 0 as Iab, 0 as eIg, 0 as seIg, sum(Amount) as OtherBusinessExpen, 0 as Mortg, 0 as inter, 0 as CEOSal from Expense where CustomerID =" + id + "and" +
                    " ClassificationsID = 2 and SubclassificationsID <> 2 group by DATEPART(MM, Date), DATEPART(YYYY, Date) union  select  DATEPART(MM, Date) as m, DATEPART(YYYY, Date) as y," +
                    " 0 as Iabb, 0 as eIgb, 0 as seIgb, 0 as seIgb, sum(Amount) as Mort, 0 as inte, 0 as CEOSa from Expense where CustomerID =" + id + "and ClassificationsID = 3 and SubclassificationsID = 15" +
                    " group by DATEPART(MM, Date), DATEPART(YYYY, Date) union select  DATEPART(MM, Date) as m, DATEPART(YYYY, Date) as y, 0 as Iabba, 0 as eIgba, 0 as seIgba, 0 as seIgba," +
                    " 0 as seIgba, sum(Amount) as inta, 0 as CEOS from Expense t1 where CustomerID =" + id + "and ClassificationsID = 3 and SubclassificationsID = 10 group by DATEPART(MM, t1.Date)," +
                    " DATEPART(YYYY, t1.Date) union select  DATEPART(MM, Date) as m, DATEPART(YYYY, Date) as y, 0 as Iabbac, 0 as eIgbac, 0 as seIgbac, 0 as seIgbac, 0 as seIgbac, 0 as " +
                    "seIgbac, sum(Amount) as CEO from Expense where CustomerID =" + id + "and(ClassificationsID = 3 or ClassificationsID = 6) and SubclassificationsID <> 10 and" +
                    " SubclassificationsID <> 15 group by DATEPART(MM, Date), DATEPART(YYYY, Date)) p group by p.m,p.y";

                return dbe.execute(sqle);
            }
            public static DataTable BusinessM(int id)
            {
                DadaBase dbe = new DadaBase();
                string sqle = " select DATEPART(YYYY, Date) as y, DATEPART(MM,Date) as m, SubclassificationsName, sum(Amount) as SumF from Expense t1 join " +
                    "Subclassifications t2 on t1.SubclassificationsID = t2.SubclassificationsID where CustomerID =" + id + "and ClassificationsID = 2  " +
                    "group by DATEPART(YYYY, Date), DATEPART(MM, Date), SubclassificationsName";

                return dbe.execute(sqle);
            }
            public static DataTable modalAll(int id,int id3,int id2, int id4)
            {
                string a = "";
                if (id4 == 1)
                {
                    a = "ClassificationsID =2 and SubclassificationsID = 2";
                }
                else if(id4==2)
                {
                    a = "ClassificationsID =2 and SubclassificationsID <> 2";
                }
                else if (id4 == 3)
                {
                    a = "ClassificationsID=3 and SubclassificationsID=15";
                }
                else if (id4 == 4)
                {
                    a = "ClassificationsID=3 and SubclassificationsID=10";
                }
                else if (id4 == 5)
                {
                    a = "( ClassificationsID=3 or ClassificationsID=6 ) and SubclassificationsID<>10 and SubclassificationsID<>15";
                }
                DadaBase dbe = new DadaBase();
                string sqle = " select ExpenseName as Name ,Amount as Sales from Expense  where CustomerID = " + id + " and " + a + " and" +
                    " DATEPART(MM, Date)= " + id3 + "  and DATEPART(YYYY, Date)= " + id2 + "";

                return dbe.execute(sqle);
            }
            public static DataTable modalAl(int id, int id3, int id2, int id4)
            {
                DateTime currentTime = DateTime.Now;//,תאריך נוכחי
                id3 += 3;
                if (id3> 12)
                {
                    id3 -= 12;
                    id2++;
                }
                DateTime Timemy = new DateTime(id2,id3,1);//תאריך שביקש המשתמש + 3 חודשים
                while (currentTime < Timemy)
                {
                    Timemy=Timemy.AddMonths(-1);
                }
                int b = int.Parse(Timemy.Month.ToString());
                int by = int.Parse(Timemy.Year.ToString());
                b -= 6;
                if(b<1)
                {
                    b += 12;
                    by--;
                }
                DateTime Timem = new DateTime(by,b,Timemy.Day);
                string Timedd = Timemy.Day.ToString();
                string Timemm = Timemy.Month.ToString();
                string Timeyy = Timemy.Year.ToString();
                string TimeA = Timeyy +"/" + Timemm +"/" + Timedd;
                string TimeBdd = Timem.Day.ToString();
                string TimeBmm = Timem.Month.ToString();
                string TimeByy = Timem.Year.ToString();
                string TimeB = TimeByy + "/" + TimeBmm + "/" + TimeBdd;
                string a = "";
                if (id4 == 1)
                {
                    a = "ClassificationsName = 'עסק' and SubclassificationsName = 'משכורת'";
                }
                else if (id4 == 2)
                {
                    a = "ClassificationsName = 'עסק' and SubclassificationsName <> 'משכורת'";
                }
                else if (id4 == 3)
                {
                    a = "ClassificationsName='בית' and SubclassificationsName='הלוואות ומשכנתא'";
                }
                else if (id4 == 4)
                {
                    a = "ClassificationsName='בית' and SubclassificationsName='מימון'";
                }
                else if (id4 == 5)
                {
                    a = "( ClassificationsName='בית' or ClassificationsName='משכורת מנכל' ) and SubclassificationsName<>'מימון' and SubclassificationsName<>'הלוואות ומשכנתא'";
                }
                DadaBase dbe = new DadaBase();
                string sqle = "  select DATEPART(MM,Date) as m,DATEPART(YYYY, Date) as y, sum(Amount) as SumF from Expense t1 join Classifications t2 on" +
                    " t1.ClassificationsID = t2.ClassificationsID join Subclassifications t3 on t1.SubclassificationsID = t3.SubclassificationsID where CustomerID = " + id + "" +
                    " and " + a + "and t1.Date BETWEEN '" + TimeB + "' and '" + TimeA + "' group by DATEPART(MM, t1.Date)," +
                    "DATEPART(YYYY, t1.Date)";

                return dbe.execute(sqle);
            }

        }
    }

}
       