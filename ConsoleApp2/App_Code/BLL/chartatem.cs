﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConsoleApp2.BLL;
using System.Data;
using ConsoleApp2.DAL;

namespace ConsoleApp2
{
    namespace BLL
    {
        public class chartatem //Class of chart
        {

            public int Months { get; set; }
            public int Years { get; set; }
            public int Expend { get; set; }
            public int Income { get; set; }
            public int Sales { get; set; }
            public int SumF { get; set; }
            public string Name { get; set; }


            public int GrossProfit { get; set; }
            public int SalaryEmployees { get; set; }
            public int OtherBusinessExpenses { get; set; }
            public int Mortgage { get; set; }
            public int interest { get; set; }
            public int CEOSalary { get; set; }


            public chartatem(int Months, int Years, int Expend, int Income)
            {
                this.Months = Months;
                this.Years = Years;
                this.Expend = Expend;
                this.Income = Income;
          
            }
            public chartatem() { }
            public static List<chartatem> Expensechart( int id)
            {
                DataTable Dte = ChartatemDal.Expensechart(id);
                List<chartatem> Lst = new List<chartatem>();
                chartatem Tamp;
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new chartatem();
                    Tamp.Months = (int)Dte.Rows[i]["Months"];
                    Tamp.Years = (int)Dte.Rows[i]["Years"];
                    Tamp.Expend = (int)Dte.Rows[i]["Expend"];
                    Tamp.Income = (int)Dte.Rows[i]["Income"];
                    Tamp.SumF= (int)Dte.Rows[i]["SumF"];
                    Lst.Add(Tamp);
                }
                return Lst;
            }
            public static List<chartatem> VendroRep(int id, string VendroName)
            {
                DataTable Dte = ChartatemDal.VendroRep(id, VendroName);
                List<chartatem> Lst = new List<chartatem>();
                chartatem Tamp;
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new chartatem();
                    Tamp.Months = (int)Dte.Rows[i]["Months"];
                    Tamp.Years = (int)Dte.Rows[i]["Years"];
                    Tamp.Sales = (int)Dte.Rows[i]["Sales"];
                    Lst.Add(Tamp);
                }
                return Lst;
            }
            public static List<chartatem> VendroRepAll(int id, int id1)
            {
                DataTable Dte = ChartatemDal.VendroRepAll(id);
                List<chartatem> Lst = new List<chartatem>();
                chartatem Tamp;
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new chartatem();
                    Tamp.Name = (string)Dte.Rows[i]["Name"];
                    Tamp.Sales = (int)Dte.Rows[i]["Sales"];
                    Lst.Add(Tamp);
                }
                return Lst;
            }
            public static List<chartatem> ProfitLossAll(int id)
            {
                DataTable Dte = ChartatemDal.ProfitLossAll(id);
                List<chartatem> Lst = new List<chartatem>();
                chartatem Tamp;
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new chartatem();
                    Tamp.Months = (int)Dte.Rows[i]["m"];
                    Tamp.Years = (int)Dte.Rows[i]["y"];
                    Tamp.GrossProfit = (int)Dte.Rows[i]["GrossProfit"];
                    Tamp.SalaryEmployees = (int)Dte.Rows[i]["SalaryEmployees"];
                    Tamp.OtherBusinessExpenses = (int)Dte.Rows[i]["OtherBusinessExpenses"];
                    Tamp.Expend = Tamp.SalaryEmployees + Tamp.OtherBusinessExpenses;
                    Tamp.Income = Tamp.GrossProfit - Tamp.Expend;
                    Tamp.Mortgage = (int)Dte.Rows[i]["Mortgage"];
                    Tamp.interest = (int)Dte.Rows[i]["interest"];
                    Tamp.Sales = Tamp.interest + Tamp.Mortgage;
                    Tamp.CEOSalary = (int)Dte.Rows[i]["CEOSalary"];
                    Tamp.SumF = Tamp.Income - (Tamp.Sales + Tamp.CEOSalary);
                    Lst.Add(Tamp);
                }
                return Lst;
            }
            public static List<chartatem> BusinessM(int id)
            {
                DataTable Dte = ChartatemDal.BusinessM(id);
                List<chartatem> Lst = new List<chartatem>();
                List<Subclassifications> sub = Subclassifications.GetAll();
                chartatem Tamp;
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    for(int j=0;j<sub.Count;j++)
                    {
                        string s = (string)Dte.Rows[i]["SubclassificationsName"];
                        string s2 = sub[i].SubclassificationsName ;
                        if (s==s2)
                        {

                        }
                    }
                    Tamp = new chartatem();
                    Tamp.Months = (int)Dte.Rows[i]["m"];
                    Tamp.Years = (int)Dte.Rows[i]["y"];
                    Tamp.Name=(string)Dte.Rows[i]["SubclassificationsName"];
                    Tamp.SumF = (int)Dte.Rows[i]["SumF"]; ;
                    Lst.Add(Tamp);
                }
                return Lst;
            }
            public static List<chartatem> modalAll(int id, int id3,int id2, int id4)
            {
                DataTable Dte = ChartatemDal.modalAll(id,id3,id2,id4);
                List<chartatem> Lst = new List<chartatem>();
                chartatem Tamp;
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new chartatem();
                    Tamp.Name = (string)Dte.Rows[i]["Name"];
                    Tamp.Sales = (int)Dte.Rows[i]["Sales"];
                    Lst.Add(Tamp);
                }
                DataTable Dte2 = ChartatemDal.modalAl(id, id3, id2, id4);
                chartatem Tamp2;
                for (int i = 0; i < Dte2.Rows.Count; i++)
                {
                    Tamp2 = new chartatem();
                    Tamp2.Months = (int)Dte2.Rows[i]["m"];
                    Tamp2.Years = (int)Dte2.Rows[i]["y"];
                    Tamp2.SumF = (int)Dte2.Rows[i]["SumF"];
                    Lst.Add(Tamp2);
                }
                return Lst;
            }

        }
        

    }
}
