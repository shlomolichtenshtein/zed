﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using ConsoleApp2.DAL;

namespace ConsoleApp2
{
    namespace BLL
    {
        public class PasswordsBA
        {
            public int PasswordsBAID { get; set; }
            public int BanksID { get; set; }
            public int CustomerID { get; set; }
            public string Identify { get; set; }
            public string Password { get; set; }
            public string AccountNumber { get; set; }
            public PasswordsBA(int PasswordsBAID, int BanksID, int CustomerID, string Identify, string Password, string AccountNumber)
            {
                this.PasswordsBAID = PasswordsBAID;
                this.BanksID = BanksID;
                this.CustomerID = CustomerID;
                this.Identify = Identify;
                this.Password = Password;
                this.AccountNumber = AccountNumber;
            }
            public PasswordsBA(int passwordsBAID, int BanksID)
            {
                this.PasswordsBAID = PasswordsBAID;
                this.BanksID = BanksID;
            }
            public PasswordsBA()
            {

            }
            public static int Pas( string data2, int Cid)
            {


                return PasswordsBADal.Pas( data2, Cid);
            }
            public int PasswId(int Bid, int Cid)
            {
                return PasswordsBADal.PasswId(Bid, Cid);
            }
            public static string name(int paid)
            {
                return PasswordsBADal.name(paid);
            }
            public static List<PasswordsBA> PasswUser(int Bid, int Cid)//Brings your expenses by date
            {
                DataTable Dte = PasswordsBADal.PasswUser(Bid,Cid);
                List<PasswordsBA> Exp = new List<PasswordsBA>();
                PasswordsBA Tamp;
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new PasswordsBA();
                    Tamp.Identify = (string)Dte.Rows[i]["Identify"];
                    Tamp.Password = (string)Dte.Rows[i]["Password"];
                    Exp.Add(Tamp);
                }
                return Exp;
            }
            public static List<PasswordsBA> GatAll(int id)//Brings your expenses by date
            {
                DataTable Dte = PasswordsBADal.GatAll(id);
                List<PasswordsBA> Exp = new List<PasswordsBA>();
                PasswordsBA Tamp;
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new PasswordsBA();
                    Tamp.BanksID = (int)Dte.Rows[i]["BanksID"];
                    Tamp.CustomerID = (int)Dte.Rows[i]["CustomerID"];
                    Tamp.Identify = (string)Dte.Rows[i]["Identify"];
                    Tamp.Password = (string)Dte.Rows[i]["Password"];
                    Exp.Add(Tamp);
                }
                return Exp;
            }
            
        }
    }

}
