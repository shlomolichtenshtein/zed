﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using ConsoleApp2.DAL;

namespace ConsoleApp2
{
    namespace BLL
    {
        public class Checkbooks//Department of Checks
        {
            public int CheckbooksID { get; set; }
            public int CheckbooksNumber { get; set; }
            public int PasswordsBAID { get; set; }
            public int CustomerID { get; set; }
            public int active { get; set; }

            public int Amount { get; set; }
            public string ExpenseName { get; set; }
            public string DueDate { get; set; }
            public string BanksName { get; set; }
            public Checkbooks(int CheckbooksID, int CheckbooksNumber,  int CustomerID)
            {
                this.CheckbooksID = CheckbooksID;
                this.CheckbooksNumber = CheckbooksNumber;
                this.CustomerID = CustomerID;
            }
            public Checkbooks()
            {

            }
            public static int Check( int namber, int cid)
            {
                return CheckbooksDal.Check(namber, cid);

            }
            public static List<Checkbooks> GetAll(int CId, int PasswordsBAid)
            {
                DataTable Dte = CheckbooksDal.GetAll(CId, PasswordsBAid);
                List<Checkbooks> Exp = new List<Checkbooks>();
                Checkbooks Tamp;
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new Checkbooks();
                    Tamp.CheckbooksID = (int)Dte.Rows[i]["CheckbooksID"];
                    Tamp.CheckbooksNumber = (int)Dte.Rows[i]["CheckbooksNumber"];
                    Tamp.CustomerID = (int)Dte.Rows[i]["CustomerID"];
                    Tamp.active = (int)Dte.Rows[i]["active"];
                    Exp.Add(Tamp);
                }
                return Exp;
            }
            public static List<Checkbooks> Getz(int CId)
            {
                DataTable Dte = CheckbooksDal.Getz(CId);
                List<Checkbooks> Exp = new List<Checkbooks>();
                Checkbooks Tamp;
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new Checkbooks();
                    Tamp.Amount = (int)Dte.Rows[i]["Amount"];
                    Tamp.ExpenseName = (string)Dte.Rows[i]["ExpenseName"];
                    Tamp.DueDate = (string)Dte.Rows[i]["DueDate"].ToString();
                    Tamp.BanksName = (string)Dte.Rows[i]["BanksName"];
                    Exp.Add(Tamp);
                }
                return Exp;
            }
            //public static int Used(int CheckbooksNumber, int cid)//Control function
            //{
            //    int i;
            //    List<Checkbooks> examination = Checkbooks.GetAll(cid);
            //    for(i=0;i<examination.Count;i++)
            //    {
            //        if(examination[i].CheckbooksNumber==CheckbooksNumber)//Does a check number already appear in the expense
            //        {
            //            break;
            //        }
            //    }
            //    if (i==examination.Count)//Does not have a chic number in the system
            //    {
            //        return 2;
            //    }
            //    else
            //        return CheckbooksDal.Used(CheckbooksNumber, cid);
            //}
            public static int pot(int id2, int PasswordsBAid)
            {
                int num = id2 % 100;
                int mymun =num % 25;
                id2 -= mymun;
                    int c = id2 + 25;
                    string InsertSql = "";
                    while (id2 < c)
                    {
                        InsertSql += "Insert into Checkbooks([CheckbooksNumber],[PasswordsBAID]) values(";
                        InsertSql += "" + id2 + ",";
                        InsertSql += "" + PasswordsBAid + ");";
                        id2++;
                    }
                    CheckbooksDal.pot(InsertSql);
                    return 0;
            }
            public static bool potAc(Checkbooks Ch)
            {
                return CheckbooksDal.potAc(Ch);
            }
        }
    }

}
