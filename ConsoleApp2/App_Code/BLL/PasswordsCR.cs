﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using ConsoleApp2.DAL;

namespace ConsoleApp2
{
    namespace BLL
    {
        public class PasswordsCR
        {
            public int PasswordsCRID { get; set; }
            public int CreditID { get; set; }
            public int BanksID { get; set; }
            public int CustomerID { get; set; }
            public string TicketNumber { get; set; }
            public string CardValidity { get; set; }
            public int Cvv { get; set; }
            public string Identify { get; set; }
            public string Passwords { get; set; }

            public string CreditName { get; set; }
            public string BanksName { get; set; }
            public string numberCr { get; set; }

            public PasswordsCR(int PasswordsCRID, int CreditID,  int BanksID, int CustomerID, string TicketNumber, string CardValidity, int Cvv, string Identify, string Passwords)
            {
                this.PasswordsCRID = PasswordsCRID;
                this.CreditID = CreditID;
                this.BanksID = BanksID;
                this.CustomerID = CustomerID;
                this.TicketNumber = TicketNumber;
                this.CardValidity = CardValidity;
                this.Cvv = Cvv;
                this.Identify = Identify;
                this.Passwords = Passwords;
              

            }
            public PasswordsCR(int PasswordsCRID, string TicketNumber,  int CustomerID)
            {
                this.PasswordsCRID = PasswordsCRID;
                this.TicketNumber = TicketNumber;
                this.CustomerID = CustomerID;
            }
            public PasswordsCR()
            {

            }
            public static List<PasswordsCR> GetAll(int CId)
            {
                DataTable Dte = PasswordsCRDal.GetAll(CId);
                List<PasswordsCR> Exp = new List<PasswordsCR>();
                PasswordsCR Tamp;
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new PasswordsCR();
                    Tamp.PasswordsCRID = (int)Dte.Rows[i]["PasswordsCRID"];
                    Tamp.CreditName = (string)Dte.Rows[i]["CreditName"];
                    Tamp.BanksName = (string)Dte.Rows[i]["BanksName"];
                    Tamp.CustomerID = (int)Dte.Rows[i]["CustomerID"];
                    Tamp.numberCr = (string)Dte.Rows[i]["numberCr"];
                    Tamp.CardValidity = (string)Dte.Rows[i]["CardValidity"];
                    Tamp.Cvv = (int)Dte.Rows[i]["Cvv"];
                    Tamp.Identify = (string)Dte.Rows[i]["Identify"];
                    Tamp.Passwords = (string)Dte.Rows[i]["Passwords"];
                    Exp.Add(Tamp);
                }
                return Exp;
            }
            public int pcr(int data1, string data2, int Cid)
            {


                return PasswordsCRDal.pcr(data1, data2, Cid);
            }
            public static string number(int paid)
            {
                return PasswordsCRDal.number(paid);
            }
            public static bool Used(string numberCr, int cid)//Function for checking whether the credit card in the system is correct
            {
                int i;
                List<PasswordsCR> examination = PasswordsCR.GetAll(cid);
                for (i = 0; i < examination.Count; i++)
                {
                    if (examination[i].numberCr == numberCr)
                    {
                        string mo = "";
                         mo += examination[i].CardValidity[0];
                        string yu = "20";
                        yu+= examination[i].CardValidity[2] + examination[i].CardValidity[3];
                        int m = int.Parse(mo);
                        int y = int.Parse(yu);
                        DateTime currentTime = DateTime.Now;
                        string year = currentTime.Year.ToString();
                        int yea = int.Parse(year);
                        string Month = currentTime.Month.ToString();
                        int Mont = int.Parse(Month);
                        if (y>yea | (y==yea & m>=Mont))
                            break;
                    }
                }
                if (i == examination.Count)
                {
                    return true;
                }
                else
                    return false;
            }
        }
    }

}