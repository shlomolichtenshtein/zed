﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using ConsoleApp2.DAL;

namespace ConsoleApp2
{
    namespace BLL
    {
        class PasswordsCredit
        {
            public int PasswordsCreditID { get; set; }
            public int CreditID { get; set; }
            public int CustomerID { get; set; }
            public string TicketNumber { get; set; }
            public string Identify { get; set; }
            public string Password { get; set; }
            public int Day { get; set; }
            public PasswordsCredit()
            {

            }
            public static List<PasswordsCredit> PasswUser(int Bid, int Cid)//Brings your expenses by date
            {
                DataTable Dte = PasswordsCreditDAL.PasswUser(Bid, Cid);
                List<PasswordsCredit> Exp = new List<PasswordsCredit>();
                PasswordsCredit Tamp;
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new PasswordsCredit();
                    Tamp.Identify = (string)Dte.Rows[i]["Identify"];
                    Tamp.Password = (string)Dte.Rows[i]["Password"];
                    Tamp.TicketNumber = (string)(Dte.Rows[i]["TicketNumber"] + "");
                    Tamp.Day = (int)Dte.Rows[i]["Day"];

                    Exp.Add(Tamp);
                }
                return Exp;
            }
        }
    }

}
