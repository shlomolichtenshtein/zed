﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConsoleApp2.DAL;



namespace ConsoleApp2
{
    namespace BLL
    { 
        public class user
        {
            public int UId { get; set; }
            public string UserName { get; set; }
            public string UserEmail { get; set; }
            public string UserPassword { get; set; }
            public user(int UId,string UserName, string UserEmail, string UserPassword)
            {
                this.UId = UId;
                this.UserName = UserName;
                this.UserEmail = UserEmail;
                this.UserPassword = UserPassword;
            }
            public user(string UserName, string UserPassword)
            {
                this.UserName = UserName;
                this.UserPassword = UserPassword;

            }
            public user()
            {

            }
            public bool CheckLogin()
            {
                 UserDal.CheckLogin(this);

                return UId != -1;
            }
        }
    }
}