﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp2.DAL;
using System.Data;

namespace ConsoleApp2
{
    namespace BLL
    {
        class Types
        {
            public int TypeID { get; set; }
            public string TypeName { get; set; }
            public int ClassificationsID { get; set; }
            public int SubclassificationsID { get; set; }
            public Types()
            {

            }
            public static List<Types> GatAll()
            {
                DataTable Dte = TypeDal.GatAll();
                List<Types> Exp = new List<Types>();
                Types Tamp;
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new Types();
                    Tamp.TypeID = (int)Dte.Rows[i]["TypeID"];
                    Tamp.ClassificationsID = (int)Dte.Rows[i]["ClassificationsID"];
                    Tamp.TypeName = (string)Dte.Rows[i]["TypeName"];
                    Tamp.SubclassificationsID = (int)Dte.Rows[i]["SubclassificationsID"];
                    Exp.Add(Tamp);
                }
                return Exp;
            }
        }
    }

}
