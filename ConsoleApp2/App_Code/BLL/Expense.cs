﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConsoleApp2.DAL;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace ConsoleApp2
{
    namespace BLL
    {
        public class Expense//Department of Expenditure
        {
            public int ExpenseID { get; set; }
            public int EId { get; set; }
            public int ClassificationsID { get; set; }
            public int SubclassificationsID { get; set; }
            public string Date { get; set; }
            public int Amount { get; set; }
            public string ActualDate { get; set; }
            public string ExpenseName { get; set; }
            public int VendorsID { get; set; }
            public int CheckbooksID { get; set; }
            public int PasswordsBAID { get; set; }
            public int Cash { get; set; }



            public int PasswordsCRID { get; set; }
            public int ConstantVariable { get; set; }
            public int Period { get; set; }
            public int CustomerID { get; set; }
            public string DueDate { get; set; }
            public int Payments { get; set; }
           

            public string BanksName { get; set; }
            public string ClassificationsName { get; set; }
            public string SubclassificationsName { get; set; }

            public string FourNumber { get; set; }
            public string CheckbooksNumber { get; set; }

            public Expense(int EId, int ClassificationsID, int SubclassificationsID, string Date, int Amount, string ActualDate, string ExpenseName, int VendorsID, int CheckbooksID, int PasswordsBAID, int Cash, int PasswordsCRID, int ConstantVariable, int Period, int CustomerID, string DueDate, int Payments)
            {
                this.EId = EId;
                this.ClassificationsID = ClassificationsID;
                this.SubclassificationsID = SubclassificationsID;
                this.Date = Date;
                this.Amount = Amount;
                this.ActualDate = ActualDate;
                this.ExpenseName = ExpenseName;
                this.VendorsID = VendorsID;
                this.CheckbooksID = CheckbooksID;
                this.PasswordsBAID = PasswordsBAID;
                this.Cash = Cash;
                this.PasswordsCRID = PasswordsCRID;
                this.ConstantVariable = ConstantVariable;
                this.Period = Period;
                this.CustomerID = CustomerID;
                this.DueDate = DueDate;
                this.Payments = Payments;
            }
            public Expense(int EId, int ClassificationsID, int SubclassificationsID, string Date, int Amount, string ActualDate, string ExpenseName, int VendorsID,int CheckbooksID, int PasswordsCRID, int ConstantVariable, int Period, int CustomerID, string DueDate, int Payments)
            {
                this.EId = EId;
                this.ClassificationsID = ClassificationsID;
                this.SubclassificationsID = SubclassificationsID;
                this.Date = Date;
                this.Amount = Amount;
                this.ActualDate = ActualDate;
                this.ExpenseName = ExpenseName;
                this.VendorsID = VendorsID;
                this.CheckbooksID = CheckbooksID;
                this.PasswordsCRID = PasswordsCRID;
                this.ConstantVariable = ConstantVariable;
                this.Period = Period;
                this.CustomerID = CustomerID;
                this.DueDate = DueDate;
                this.Payments = Payments;
            }
            public Expense(int EId, int ClassificationsID, int SubclassificationsID, string Date, int Amount, string ActualDate, string ExpenseName, int VendorsID, int PasswordsBAID, int Cash, int ConstantVariable, int Period, int CustomerID)
            {
                this.EId = EId;
                this.ClassificationsID = ClassificationsID;
                this.SubclassificationsID = SubclassificationsID;
                this.Date = Date;
                this.Amount = Amount;
                this.ActualDate = ActualDate;
                this.ExpenseName = ExpenseName;
                this.VendorsID = VendorsID;
                this.PasswordsBAID = PasswordsBAID;
                this.Cash = Cash;
                this.ConstantVariable = ConstantVariable;
                this.Period = Period;
                this.CustomerID = CustomerID;
            }
            public Expense()
            {

            }

           
            public void AddExpen()//Add a new expense
            {
                string dat2 = Utils.GlobFuncs.dateadd(this.Date);
                string dat = Utils.GlobFuncs.dateadd(this.ActualDate);
                string dat1 = Utils.GlobFuncs.dateadd(this.DueDate);
                int p = 0;
                if (this.BanksName !="")//Will the payment be transferred
                {
                    p = PasswordsBA.Pas(this.BanksName, this.CustomerID);
                }
                int pc = 0;
                if (this.FourNumber != "")//Do credit card payment
                {
                    PasswordsCR pacr = new PasswordsCR();

                    pc = pacr.pcr(-1, this.FourNumber, this.CustomerID);
                }
                int Chec = 0;
                if (this.CheckbooksNumber !="")//Is the payment in check
                {
                    Chec= Checkbooks.Check(int.Parse(this.CheckbooksNumber), this.CustomerID);
                }
                Expense ht;
                ht = new Expense();
                ht.ExpenseID = (int)this.ExpenseID;
                ht.Date = (string)dat2.ToString();
                ht.ExpenseName = (string)this.ExpenseName;
                ht.Amount = (int)this.Amount;
                ht.PasswordsBAID = (int)p;
                ht.ClassificationsID = (int)this.ClassificationsID;
                ht.SubclassificationsID = (int)this.SubclassificationsID;
                ht.ConstantVariable = (int)this.ConstantVariable;
                ht.Period = (int)this.Period;
                ht.Payments = (int)this.Payments;
                ht.CustomerID = (int)this.CustomerID;
                ht.CheckbooksID = (int)Chec;
                ht.PasswordsCRID = (int)pc;
                ht.ActualDate = (string)dat.ToString();
                ht.DueDate = (string)dat1.ToString();
                ht.Cash = (int)this.Cash;
                ht.VendorsID = (int)this.VendorsID;
                ExpenseDal.AddExpen(ht);
            }
            public static List<Expense> GetAll(int CId)
            {
                DataTable Dte = ExpenseDal.GetAll(CId);
                List<Expense> Exp = new List<Expense>();
                Expense Tamp;
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new Expense();
                    Tamp.ExpenseID = (int)Dte.Rows[i]["ExpenseID"];
                    Tamp.Date = (string)Dte.Rows[i]["Date"].ToString();
                    Tamp.ExpenseName = (string)Dte.Rows[i]["ExpenseName"];
                    Tamp.Amount = (int)Dte.Rows[i]["Amount"];
                    Tamp.BanksName = (string)Dte.Rows[i]["BanksName"];
                    Tamp.ClassificationsName = (string)Dte.Rows[i]["ClassificationsName"];
                    Tamp.SubclassificationsName = (string)Dte.Rows[i]["SubclassificationsName"];
                    Tamp.ConstantVariable = (int)Dte.Rows[i]["ConstantVariable"];
                    Tamp.Period = (int)Dte.Rows[i]["Period"];
                    Tamp.Payments = (int)Dte.Rows[i]["Payments"];
                    Tamp.CustomerID = (int)Dte.Rows[i]["CustomerID"];
                    Exp.Add(Tamp);
                }
                return Exp;
            }

           
            public static List<Expense> GetAl(int CId, int da)//Brings your expenses by date
            {
                DataTable Dte = ExpenseDal.GetAl(CId,da);
                List<Expense> Exp = new List<Expense>();
                Expense Tamp;
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new Expense();
                    Tamp.ExpenseID = (int)Dte.Rows[i]["ExpenseID"];
                    Tamp.Date = (string)Dte.Rows[i]["Date"].ToString();
                    Tamp.ExpenseName = (string)Dte.Rows[i]["ExpenseName"];
                    Tamp.Amount = (int)Dte.Rows[i]["Amount"];
                    Tamp.BanksName = (string)Dte.Rows[i]["BanksName"] + "";
                    Tamp.ClassificationsName = (string)Dte.Rows[i]["ClassificationsName"].ToString() + "";
                    Tamp.SubclassificationsName = (string)Dte.Rows[i]["SubclassificationsName"].ToString() + "";
                    Tamp.ConstantVariable = (int)Dte.Rows[i]["ConstantVariable"];
                    Tamp.Period = (int)Dte.Rows[i]["Period"];
                    Tamp.Payments = (int)Dte.Rows[i]["Payments"];
                    Tamp.CustomerID = (int)Dte.Rows[i]["CustomerID"];
                    Exp.Add(Tamp);
                }
                return Exp;
            }
            public static List<Expense> GetAlld(int CId, string da1, string da2)
            {
                DataTable Dte = ExpenseDal.GetAlld(CId, da1,da2);
                List<Expense> Exp = new List<Expense>();
                Expense Tamp;
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new Expense();
                    Tamp.ExpenseID = (int)Dte.Rows[i]["ExpenseID"];
                    Tamp.Date = (string)Dte.Rows[i]["Date"].ToString();
                    Tamp.ExpenseName = (string)Dte.Rows[i]["ExpenseName"];
                    Tamp.Amount = (int)Dte.Rows[i]["Amount"];
                    Tamp.BanksName = (string)Dte.Rows[i]["BanksName"];
                    Tamp.ClassificationsName = (string)Dte.Rows[i]["ClassificationsName"]+ "";
                    Tamp.SubclassificationsName = (string)Dte.Rows[i]["SubclassificationsName"] +"";
                    Tamp.ConstantVariable = (int)Dte.Rows[i]["ConstantVariable"];
                    Tamp.Period = (int)Dte.Rows[i]["Period"];
                    Tamp.Payments = (int)Dte.Rows[i]["Payments"];
                    Tamp.CustomerID = (int)Dte.Rows[i]["CustomerID"];
                    Exp.Add(Tamp);
                }
                return Exp;
            }
            public static List<Expense> GetBId(int CId, int EId )
            {

                DataTable Dte = ExpenseDal.GetBId(CId , EId);
                List<Expense> Exp = new List<Expense>();
                Expense Tamp;
                string datag = Utils.GlobFuncs.DateGet(Dte.Rows[0]["Date"].ToString());
                string ActualDateg = Utils.GlobFuncs.DateGet(Dte.Rows[0]["ActualDate"].ToString());
                string DueDateg = Utils.GlobFuncs.DateGet(Dte.Rows[0]["DueDate"].ToString());
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new Expense();
                    Tamp.ExpenseID = (int)Dte.Rows[i]["ExpenseID"];
                    Tamp.Date = datag;
                    Tamp.ExpenseName = (string)Dte.Rows[i]["ExpenseName"];
                    Tamp.Amount = (int)Dte.Rows[i]["Amount"];
                    Tamp.ActualDate = ActualDateg;
                    Tamp.ClassificationsName = (string)Dte.Rows[i]["ClassificationsName"];
                    Tamp.SubclassificationsName = (string)Dte.Rows[i]["SubclassificationsName"];
                    Tamp.ConstantVariable = (int)Dte.Rows[i]["ConstantVariable"];
                    Tamp.Period = (int)Dte.Rows[i]["Period"];
                    Tamp.Payments = (int)Dte.Rows[i]["Payments"];
                    Tamp.CheckbooksNumber = (string)Dte.Rows[i]["CheckbooksNumber"].ToString();
                    Tamp.Cash = (int)Dte.Rows[i]["Cash"];
                    Tamp.FourNumber = (string)Dte.Rows[i]["FourNumber"].ToString();
                    Tamp.CustomerID = (int)Dte.Rows[i]["CustomerID"];
                    Tamp.VendorsID = (int)Dte.Rows[i]["VendorsID"];
                    Tamp.DueDate = DueDateg;
                    Tamp.BanksName = (string)Dte.Rows[i]["BanksName"].ToString();
                    Tamp.ClassificationsID = (int)Dte.Rows[i]["ClassificationsID"];
                    Tamp.SubclassificationsID = (int)Dte.Rows[i]["SubclassificationsID"];
                    Exp.Add(Tamp);
                }
                return Exp;
            }
            public static bool Delete(int id)
            {
                ExpenseDal.Delete(id);
                return true;
            }

            public void pot(int id)
            {
                string dat2 = Utils.GlobFuncs.dateadd(this.Date);
                string dat = Utils.GlobFuncs.dateadd(this.ActualDate);
                string dat1 = Utils.GlobFuncs.dateadd(this.DueDate);
                int p = 0;
                if (this.BanksName != "")//Will the payment be transferred
                {
                    p = PasswordsBA.Pas(this.BanksName, this.CustomerID);
                }
                int pc = 0;
                if (this.FourNumber != "")//Do credit card payment
                {
                    PasswordsCR pacr = new PasswordsCR();

                    pc = pacr.pcr(-1, this.FourNumber, this.CustomerID);
                }
                int Chec = 0;
                if (this.CheckbooksNumber != "")//Is the payment in check
                {
                    Chec = Checkbooks.Check(int.Parse(this.CheckbooksNumber), this.CustomerID);
                    Checkbooks ch = new Checkbooks();
                    ch.active = 1;
                    ch.CheckbooksID = Chec;
                    Checkbooks.potAc(ch);
                }
                Expense ht;
                ht = new Expense();
                ht.ExpenseID = id;
                ht.Date = (string)dat2.ToString();
                ht.ExpenseName = (string)this.ExpenseName;
                ht.Amount = (int)this.Amount;
                ht.PasswordsBAID = (int)p;
                ht.ClassificationsID = (int)this.ClassificationsID;
                ht.SubclassificationsID = (int)this.SubclassificationsID;
                ht.ConstantVariable = (int)this.ConstantVariable;
                ht.Period = (int)this.Period;
                ht.Payments = (int)this.Payments;
                ht.CustomerID = (int)this.CustomerID;
                ht.CheckbooksID = (int)Chec;
                ht.PasswordsCRID = (int)pc;
                ht.ActualDate = (string)dat.ToString();
                ht.DueDate = (string)dat1.ToString();
                ht.Cash = (int)this.Cash;
                ht.VendorsID = (int)this.VendorsID;
                ExpenseDal.pot(ht);
              
            }
            public static void ExtractDataFromExcel(string filePath, int id)
            {
                String excelConnString = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 8.0\"", filePath);
                string Sql = "SELECT * FROM  [Activities$]";
                string InsertSql = "";
                OleDbConnection excelConn = new OleDbConnection(excelConnString);
                excelConn.Open();
                OleDbCommand excelcmd = new OleDbCommand(Sql, excelConn);
                OleDbDataReader excelDr = excelcmd.ExecuteReader();
                List<Expense> Exp = Expense.GetAl(id,60);
                int b = 0;
                while (excelDr.Read())
                {
                    int a = 0;
                    int c = 0;
                    for(int i=0;i<Exp.Count;i++)
                    {
                        c = (int.TryParse(excelDr["ch"].ToString(), out c)) ? c : 0;
                        string d = excelDr["da"].ToString();
                        string na = excelDr["t"].ToString();
                        if (d==Exp[i].Date && na == Exp[i].ExpenseName && c==Exp[i].Amount && Exp[i].CustomerID==id)
                        {
                            a++;
                            break;
                        }
                    }
                    if(a==0)
                    {
                        string Day = "";
                        string year = "";
                        string month = "";
                        string mydate = (string)excelDr["da"].ToString();
                        Day += mydate[0] + "" + mydate[1];
                        month += mydate[3] + "" + mydate[4];
                        year += mydate[6] + "" + mydate[7] + "" + mydate[8] + "" + mydate[9];
                        string md = year + "/" + month + "/" + Day;

                        InsertSql += "Insert into Expense([Date],[ActualDate],[CustomerID],[ConstantVariable],[Period],[Payments],[Amount],[ExpenseName]) values(";
                        InsertSql += "'" + md + "',";
                        InsertSql += "'" + md + "',";
                        InsertSql += "" + id + ",";
                        InsertSql += "" + 0 + ",";
                        InsertSql += "" + 1 + ",";
                        InsertSql += "" + 1 + ",";
                        InsertSql += "" + c + ",";
                        InsertSql += "'" + (string)excelDr["t"] + "');";
                        b++;
                    }
                }
                if (b > 0)
                {
                    ExpenseDal.ExtractDataFromExcel(InsertSql);
                }
               
            }
            public static void AddExpense(Expense Ex)
            {
                ExpenseDal.AddExpen(Ex);
            }
        }

    }

}