﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConsoleApp2.BLL;
using System.Data;
using ConsoleApp2.DAL;

namespace ConsoleApp2
{
    namespace BLL
    {
        public class Customer : user//A client class inherits from the user
        {
            public int CustomerID { get; set; }
            public string CustomerName { get; set; }
            public string Email { get; set; }
            public string Phone { get; set; }
            public string Password { get; set; }
            public string IdentityCard { get; set; }
            public Customer(int CustomerID, string CustomerName, string Email, string Phone, string Password, string IdentityCard) : base(CustomerID, CustomerName, Email, Password)
            {
                this.CustomerID = CustomerID;
                this.CustomerName = CustomerName;
                this.Email = Email;
                this.Phone = Phone;
                this.Password = Password;
                this.IdentityCard = IdentityCard;
            }
            public Customer()
            {

            }
            public static List<Customer> GetAll()
            {
                DataTable Dte = CustomerDal.GetAll();
                List<Customer> Exp = new List<Customer>();
                Customer Tamp;
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new Customer();
                    Tamp.CustomerID = (int)Dte.Rows[i]["CustomerID"];
                    Tamp.Email = (string)Dte.Rows[i]["E-mail"];
                    Tamp.CustomerName = (string)Dte.Rows[i]["CustomerName"];
                    Tamp.Phone = (string)Dte.Rows[i]["Phone"];
                    Tamp.Password = (string)Dte.Rows[i]["Password"];
                    Tamp.IdentityCard = (string)Dte.Rows[i]["IdentityCard"];
                    Exp.Add(Tamp);
                }
                return Exp;
            }
            public static string GatIdC(int id)
            {
                return CustomerDal.GatIdC(id);
            }
        }

    } 

}

