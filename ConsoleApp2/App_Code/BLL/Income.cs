﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using ConsoleApp2.DAL;

namespace ConsoleApp2
{
    namespace BLL
    {
        public class Income//Department of income
        {
            public int IncomeID { get; set; }
            public int ClassificationsID { get; set; }
            public int SubclassificationsID { get; set; }
            public string IncomeName { get; set; }
            public int CustomerID { get; set; }
            public string Date { get; set; }
            public int Amount { get; set; }
            public string DatePR { get; set; }
            public int ProductsID { get; set; }
            public int PasswordsBAID { get; set; }
            public int CreditID { get; set; }
            public int AgentID { get; set; }

            public string BanksName { get; set; }
            public string ClassificationsName { get; set; }
            public string SubclassificationsName { get; set; }

            public int amuntZ { get; set; }
            public double AmericanExpress { get; set; }
            public double LeumiCard { get; set; }
            public double visa { get; set; }
            public double Isracard { get; set; }
            public double Diners { get; set; }
            public double credit { get; set; }
            public double European { get; set; }
            public double Reliable { get; set; }
            public double Calc { get; set; }
            public int Check { get; set; }
            public int transference { get; set; }
            public int DailyRedemption { get; set; }


            public Income(int IncomeID, int ClassificationsID, int SubclassificationsID, string IncomeName, int CustomerID, string Date, int Amount, string DatePR, int ProductsID, int PasswordsBAID, int CreditID, int AgentID)
            {
                this.IncomeID = IncomeID;
                this.ClassificationsID = ClassificationsID;
                this.SubclassificationsID = SubclassificationsID;
                this.Date = Date;
                this.Amount = Amount;
                this.IncomeName = IncomeName;
                this.DatePR = DatePR;
                this.PasswordsBAID = PasswordsBAID;
                this.CreditID = CreditID;
                this.CustomerID = CustomerID;
                this.AgentID = AgentID;
                this.ProductsID = ProductsID;

            }
            public Income()
            {

            }
            public static List<Income> GetAll(int CId)
            {
                DataTable Dte = IncomeDal.GetAll(CId);
                List<Income> Exp = new List<Income>();
                Income Tamp;
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new Income();
                    Tamp.CustomerID = (int)Dte.Rows[i]["customerID"];
                    Tamp.IncomeID = (int)Dte.Rows[i]["IncomeID"];
                    Tamp.Date = (string)Dte.Rows[i]["Date"].ToString();
                    Tamp.IncomeName = (string)Dte.Rows[i]["IncomeName"];
                    Tamp.Amount = (int)Dte.Rows[i]["Amount"];
                    Tamp.DatePR = (string)Dte.Rows[i]["DatePR"].ToString();
                    Tamp.ClassificationsName = (string)Dte.Rows[i]["ClassificationsName"];
                    Tamp.SubclassificationsName = (string)Dte.Rows[i]["SubclassificationsName"];
                    Exp.Add(Tamp);
                }
                return Exp;
            }
            public static List<Income> GetAl(int CId,int da)
            {
                DataTable Dte = IncomeDal.GetAl(CId,da);
                List<Income> Exp = new List<Income>();
                Income Tamp;
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new Income();
                    Tamp.CustomerID = (int)Dte.Rows[i]["customerID"];
                    Tamp.IncomeID = (int)Dte.Rows[i]["IncomeID"];
                    Tamp.Date = (string)Dte.Rows[i]["Date"].ToString();
                    Tamp.IncomeName = (string)Dte.Rows[i]["IncomeName"];
                    Tamp.Amount = (int)Dte.Rows[i]["Amount"];
                    Tamp.DatePR = (string)Dte.Rows[i]["DatePR"].ToString();
                    Tamp.ClassificationsName = (string)Dte.Rows[i]["ClassificationsName"];
                    Tamp.SubclassificationsName = (string)Dte.Rows[i]["SubclassificationsName"];
                    Exp.Add(Tamp);
                }
                return Exp;
            }
            public static List<Income> GetAlD(int CId, string da1,string da2)
            {
                DataTable Dte = IncomeDal.GetAlD(CId, da1,da2);
                List<Income> Exp = new List<Income>();
                Income Tamp;
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new Income();
                    Tamp.CustomerID = (int)Dte.Rows[i]["customerID"];
                    Tamp.IncomeID = (int)Dte.Rows[i]["IncomeID"];
                    Tamp.Date = (string)Dte.Rows[i]["Date"].ToString();
                    Tamp.IncomeName = (string)Dte.Rows[i]["IncomeName"];
                    Tamp.Amount = (int)Dte.Rows[i]["Amount"];
                    Tamp.DatePR = (string)Dte.Rows[i]["DatePR"].ToString();
                    Tamp.ClassificationsName = (string)Dte.Rows[i]["ClassificationsName"];
                    Tamp.SubclassificationsName = (string)Dte.Rows[i]["SubclassificationsName"];
                    Exp.Add(Tamp);
                }
                return Exp;
            }

            public void AddIncome()//New Income Function
            {

                string nameI;
                Income amz;
                amz = new Income();

                string st = Utils.GlobFuncs.dateadd(this.Date);               
                if (this.amuntZ != 0)
                {
                    nameI = "מזומן זד יומי";
                    amz.IncomeName = nameI;
                    amz.ClassificationsID = this.ClassificationsID;
                    amz.SubclassificationsID = this.SubclassificationsID;
                    amz.CustomerID = this.CustomerID;
                    amz.Amount = this.amuntZ;
                    amz.Date = st;
                    amz.DatePR = "";
                    amz.PasswordsBAID = 0;
                    IncomeDal.AddIncome(amz);
                }

                if (this.DailyRedemption != 0)
                {
                    nameI = "מזומן נוסף";
                    amz.IncomeName = nameI;
                    amz.ClassificationsID = this.ClassificationsID;
                    amz.SubclassificationsID = this.SubclassificationsID;
                    amz.CustomerID = this.CustomerID;
                    amz.Amount = this.DailyRedemption - this.amuntZ;
                    amz.Date = st;
                    amz.DatePR = "";
                    amz.PasswordsBAID = 0;
                    IncomeDal.AddIncome(amz);

                }

                if (this.transference != 0)
                {
                    nameI = "תשלום בהעברה";                   
                    int p = PasswordsBA.Pas(this.BanksName, this.CustomerID);
                    amz.IncomeName = nameI;
                    amz.ClassificationsID = this.ClassificationsID;
                    amz.SubclassificationsID = this.SubclassificationsID;
                    amz.CustomerID = this.CustomerID;
                    amz.Amount = this.transference;
                    amz.Date = st;
                    amz.DatePR = "";
                    amz.PasswordsBAID = p;
                    IncomeDal.AddIncome(amz);
                }
                if (this.Check != 0)
                {
                    nameI = "תשלום בשיק";
                    string st1 = Utils.GlobFuncs.dateadd(this.DatePR);
                    amz.IncomeName = nameI;
                    amz.ClassificationsID = this.ClassificationsID;
                    amz.SubclassificationsID = this.SubclassificationsID;
                    amz.CustomerID = this.CustomerID;
                    amz.Amount = this.Check;
                    amz.Date = st;
                    amz.DatePR = st1;
                    amz.PasswordsBAID = 0;
                    IncomeDal.AddIncome(amz);
                }
                //When the income is in a credit card I want to put it in one row in a database but when the money comes in on a different date or in another
                //bank I can't reduce to one line so I'll check and only when the money comes in on the same date and in the same bank

                string[] arr1 = new string[9];//An array of credit card types
                arr1[0] = "AmericanExpress";
                arr1[1] = "LeumiCard";
                arr1[2] = "visa";
                arr1[3] = "Isracard";
                arr1[4] = "Diners";
                arr1[5] = "credit";
                arr1[6] = "European";
                arr1[7] = "Reliable";
                arr1[8] = "Calc";


                double[] arr = new double[9];
                arr[0] = this.AmericanExpress;
                arr[1] = this.LeumiCard;
                arr[2] = this.visa;
                arr[3] = this.Isracard;
                arr[4] = this.Diners;
                arr[5] = this.credit;
                arr[6] = this.European;
                arr[7] = this.Reliable;
                arr[8] = this.Calc;

                int[] arr3 = new int[9];
                string[] arr2 = new string[9];
                List<CreditClearing> cre = CreditClearing.GetAll(this.CustomerID);
                for (int j = 0; j < arr.Length; j++)
                {
                    if (arr[j] != 0)
                    {
                        

                        for (int i = 0; i < cre.Count; i++)
                        {
                            if (cre[i].CreditClearingName == arr1[j])
                            {
                                arr3[j] = cre[i].BanksID;
                                arr[j] =arr[j]-cre[i].CommissionS;
                                DateTime dat = new DateTime();
                                dat = DateTime.Parse(st);
                                if (cre[i].Namber == 1)
                                {
                                    
                                    DateTime qw = new DateTime(dat.Year, dat.Month, 28);
                                    arr2[j] = qw.ToString("yyyy/MM/dd");
                                }
                                else if (cre[i].Namber == 2)
                                {
                                    if (arr1[j] != "Diners")
                                    {
                                        if (dat.Day < 15)
                                        {
                                            dat = dat.AddMonths(1);
                                            DateTime qw = new DateTime(dat.Year, dat.Month, 2);
                                            arr2[j] = qw.ToString("yyyy/MM/dd");
                                        }
                                        else
                                        {
                                            dat = dat.AddMonths(1);
                                            DateTime qw = new DateTime(dat.Year, dat.Month, 8);
                                            arr2[j] = qw.ToString("yyyy/MM/dd");
                                        }
                                    }
                                    else
                                    {
                                        if (dat.Day < 14)
                                        {
                                            dat = dat.AddMonths(1);
                                            DateTime qw = new DateTime(dat.Year, dat.Month, 2);
                                            arr2[j] = qw.ToString("yyyy/MM/dd");
                                        }
                                        else
                                        {
                                            dat = dat.AddMonths(1);
                                            DateTime qw = new DateTime(dat.Year, dat.Month, 15);
                                            arr2[j] = qw.ToString("yyyy/MM/dd");
                                        }
                                    }
                                }
                                else if (cre[i].Namber == 4)
                                {
                                    if (dat.Day < 8)
                                    {

                                        DateTime qw = new DateTime(dat.Year, dat.Month, 21);
                                        arr2[j] = qw.ToString("yyyy/MM/dd");
                                    }
                                    else if (dat.Day < 15)
                                    {
                                        dat = dat.AddMonths(1);
                                        DateTime qw = new DateTime(dat.Year, dat.Month, 1);
                                        arr2[j] = qw.ToString("yyyy/MM/dd");
                                    }
                                    else if (dat.Day < 20)
                                    {
                                        dat = dat.AddMonths(1);
                                        DateTime qw = new DateTime(dat.Year, dat.Month, 7);
                                        arr2[j] = qw.ToString("yyyy/MM/dd");
                                    }
                                    else
                                    {
                                        dat = dat.AddMonths(1);
                                        DateTime qw = new DateTime(dat.Year, dat.Month, 14);
                                        arr2[j] = qw.ToString("yyyy/MM/dd");
                                    }
                                }
                                else
                                {
                                    arr2[j] = st;
                                }
                            }
                        }

                    }
                }

                for (int i = 0; i < arr2.Length; i++)
                {
                    for (int j = i+1; j < arr2.Length - 1; j++)
                    {
                        if (arr2[i] == arr2[j] & arr[j] != 0 & arr3[i]==arr3[j])
                        {
                            arr[i] += arr[j];
                            arr[j] = 0;
                        }
                    }

                }
                int[] arr4 = new int[9];
                for(int i=0;i<9;i++)
                {
                    arr4[i] = (int)arr[i];
                }              
                for (int i = 0; i<arr.Length;i++)
                {
                    if(arr[i]!=0)
                    {
                        PasswordsBA pass = new PasswordsBA();
                        int p = pass.PasswId(arr3[i], this.CustomerID);
                        amz.IncomeName = "תשלום באשראי";
                        amz.ClassificationsID = this.ClassificationsID;
                        amz.SubclassificationsID = this.SubclassificationsID;
                        amz.CustomerID = this.CustomerID;
                        amz.Amount =arr4[i];
                        amz.Date = st;
                        amz.DatePR = arr2[i];
                        amz.PasswordsBAID = p;
                        IncomeDal.AddIncome(amz);
                    }
                }

                
            }
            public void pot(int id)
            {
                string nameI;
                Income amz;
                amz = new Income();

                string st = Utils.GlobFuncs.dateadd(this.Date);
                if (this.amuntZ != 0)
                {
                    nameI = "מזומן זד יומי";
                    amz.IncomeID = id;
                    amz.IncomeName = nameI;
                    amz.ClassificationsID = this.ClassificationsID;
                    amz.SubclassificationsID = this.SubclassificationsID;
                    amz.CustomerID = this.CustomerID;
                    amz.Amount = this.amuntZ;
                    amz.Date = st;
                    amz.DatePR = "";
                    amz.PasswordsBAID = 0;
                    IncomeDal.pot(amz);
                }

                if (this.DailyRedemption != 0)
                {
                    nameI = "מזומן נוסף";
                    amz.IncomeID = id;
                    amz.IncomeName = nameI;
                    amz.ClassificationsID = this.ClassificationsID;
                    amz.SubclassificationsID = this.SubclassificationsID;
                    amz.CustomerID = this.CustomerID;
                    amz.Amount = this.DailyRedemption - this.amuntZ;
                    amz.Date = st;
                    amz.DatePR = "";
                    amz.PasswordsBAID = 0;
                    IncomeDal.pot(amz);

                }

                if (this.transference != 0)
                {
                    nameI = "תשלום בהעברה";
                    amz.IncomeID = id;
                    int p = PasswordsBA.Pas(this.BanksName, this.CustomerID);
                    amz.IncomeName = nameI;
                    amz.ClassificationsID = this.ClassificationsID;
                    amz.SubclassificationsID = this.SubclassificationsID;
                    amz.CustomerID = this.CustomerID;
                    amz.Amount = this.transference;
                    amz.Date = st;
                    amz.DatePR = "";
                    amz.PasswordsBAID = p;
                    IncomeDal.pot(amz);
                }
                if (this.Check != 0)
                {
                    nameI = "תשלום בשיק";
                    amz.IncomeID = id;
                    string st1 = Utils.GlobFuncs.dateadd(this.DatePR);
                    amz.IncomeName = nameI;
                    amz.ClassificationsID = this.ClassificationsID;
                    amz.SubclassificationsID = this.SubclassificationsID;
                    amz.CustomerID = this.CustomerID;
                    amz.Amount = this.Check;
                    amz.Date = st;
                    amz.DatePR = st1;
                    amz.PasswordsBAID = 0;
                    IncomeDal.pot(amz);
                }
                string[] arr1 = new string[9];
                arr1[0] = "AmericanExpress";
                arr1[1] = "LeumiCard";
                arr1[2] = "visa";
                arr1[3] = "Isracard";
                arr1[4] = "Diners";
                arr1[5] = "credit";
                arr1[6] = "European";
                arr1[7] = "Reliable";
                arr1[8] = "Calc";


                double[] arr = new double[9];
                arr[0] = this.AmericanExpress;
                arr[1] = this.LeumiCard;
                arr[2] = this.visa;
                arr[3] = this.Isracard;
                arr[4] = this.Diners;
                arr[5] = this.credit;
                arr[6] = this.European;
                arr[7] = this.Reliable;
                arr[8] = this.Calc;

                int[] arr3 = new int[9];
                string[] arr2 = new string[9];
                List<CreditClearing> cre = CreditClearing.GetAll(this.CustomerID);
                for (int j = 0; j < arr.Length; j++)
                {
                    if (arr[j] != 0)
                    {


                        for (int i = 0; i < cre.Count; i++)
                        {
                            if (cre[i].CreditClearingName == arr1[j])
                            {
                                arr3[j] = cre[i].BanksID;
                                arr[j] = arr[j] - cre[i].CommissionS;
                                DateTime dat = new DateTime();
                                dat = DateTime.Parse(st);
                                if (cre[i].Namber == 1)
                                {
                                    dat = dat.AddMonths(1);
                                    DateTime qw = new DateTime(dat.Year, dat.Month, 2);
                                    arr2[j] = qw.ToString("yyyy/MM/dd");
                                }
                                else if (cre[i].Namber == 2)
                                {
                                    if (arr1[j] != "Diners")
                                    {
                                        if (dat.Day < 15)
                                        {
                                            dat = dat.AddMonths(1);
                                            DateTime qw = new DateTime(dat.Year, dat.Month, 2);
                                            arr2[j] = qw.ToString("yyyy/MM/dd");
                                        }
                                        else
                                        {
                                            dat = dat.AddMonths(1);
                                            DateTime qw = new DateTime(dat.Year, dat.Month, 8);
                                            arr2[j] = qw.ToString("yyyy/MM/dd");
                                        }
                                    }
                                    else
                                    {
                                        if (dat.Day < 14)
                                        {
                                            dat = dat.AddMonths(1);
                                            DateTime qw = new DateTime(dat.Year, dat.Month, 2);
                                            arr2[j] = qw.ToString("yyyy/MM/dd");
                                        }
                                        else
                                        {
                                            dat = dat.AddMonths(1);
                                            DateTime qw = new DateTime(dat.Year, dat.Month, 15);
                                            arr2[j] = qw.ToString("yyyy/MM/dd");
                                        }
                                    }
                                }
                                else if (cre[i].Namber == 4)
                                {
                                    if (dat.Day < 8)
                                    {

                                        DateTime qw = new DateTime(dat.Year, dat.Month, 21);
                                        arr2[j] = qw.ToString("yyyy/MM/dd");
                                    }
                                    else if (dat.Day < 15)
                                    {
                                        dat = dat.AddMonths(1);
                                        DateTime qw = new DateTime(dat.Year, dat.Month, 1);
                                        arr2[j] = qw.ToString("yyyy/MM/dd");
                                    }
                                    else if (dat.Day < 20)
                                    {
                                        dat = dat.AddMonths(1);
                                        DateTime qw = new DateTime(dat.Year, dat.Month, 7);
                                        arr2[j] = qw.ToString("yyyy/MM/dd");
                                    }
                                    else
                                    {
                                        dat = dat.AddMonths(1);
                                        DateTime qw = new DateTime(dat.Year, dat.Month, 14);
                                        arr2[j] = qw.ToString("yyyy/MM/dd");
                                    }
                                }
                                else
                                {
                                    arr2[j] = st;
                                }
                            }
                        }

                    }
                }

                for (int i = 0; i < arr2.Length; i++)
                {
                    for (int j = i+1; j < arr2.Length - 1; j++)
                    {
                        if (arr2[i] == arr2[j] & arr[j] != 0 & arr3[i] == arr3[j])
                        {
                            arr[i] += arr[j];
                            arr[j] = 0;
                        }
                    }

                }
                int[] arr4 = new int[9];
                for (int i = 0; i < 9; i++)
                {
                    arr4[i] = (int)arr[i];
                }
                for (int i = 0; i < arr.Length; i++)
                {
                    if (arr[i] != 0)
                    {
                        PasswordsBA pass = new PasswordsBA();
                        int p = pass.PasswId(arr3[i], this.CustomerID);
                        amz.IncomeName = "תשלום באשראי";
                        amz.IncomeID = id;
                        amz.ClassificationsID = this.ClassificationsID;
                        amz.SubclassificationsID = this.SubclassificationsID;
                        amz.CustomerID = this.CustomerID;
                        amz.Amount = arr4[i];
                        amz.Date = st;
                        amz.DatePR = arr2[i];
                        amz.PasswordsBAID = p;
                        IncomeDal.pot(amz);
                    }
                }

            }
            public static List<Income> GetBId(int CId, int EId)
            {
                DataTable Dte = IncomeDal.GetBId(CId, EId);
                List<Income> Exp = new List<Income>();
                Income Tamp;
                string datag = Utils.GlobFuncs.DateGet(Dte.Rows[0]["Date"].ToString());
                string DatePRg = Utils.GlobFuncs.DateGet(Dte.Rows[0]["DatePR"].ToString());
                for (int i = 0; i < Dte.Rows.Count; i++)
                {
                    Tamp = new Income();
                    Tamp.IncomeID = (int)Dte.Rows[i]["IncomeID"];
                    Tamp.Date = datag;
                    Tamp.IncomeName = (string)Dte.Rows[i]["IncomeName"];
                    Tamp.Amount = (int)Dte.Rows[i]["Amount"];
                    Tamp.DatePR = DatePRg;
                    Tamp.ClassificationsName = (string)Dte.Rows[i]["ClassificationsName"];
                    Tamp.SubclassificationsName = (string)Dte.Rows[i]["SubclassificationsName"];
                    Tamp.BanksName = (string)(Dte.Rows[i]["BanksName"] + "");
                    Exp.Add(Tamp);
                }
                return Exp;
            }
            public static bool Delete(int id)
            {
                IncomeDal.Delete(id);
                return true;
            }


        }
    }
 
}