﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConsoleApp2.DAL;

namespace ConsoleApp2
{
    namespace BLL
    {
        public class Banks //Department of Banks
        {
            public int BanksID { get; set; }
            public string BanksName { get; set; }
            public Banks(int BanksID, string BanksName)
            {
                this.BanksID = BanksID;
                this.BanksName = BanksName;
            }
            public int Bank(int data1, string data2)
            {


            return BanksDal.Bank(data1, data2);
            }
        }
    }

}